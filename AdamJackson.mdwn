
X hacker, for Red Hat and recreationally.  [[See elsewhere.|http://freedesktop.org/wiki/AdamJackson]] 


# Old school 3D hardware

I have something of a rare hardware fetish.  The following list of cards I'd like to have at some point is as much for my own benefit as anyone else's.  If you have one of these and are willing to part with it, please let me know!  Contact info can be found through the link at the top. 

Even better, if you have programming information for this hardware, or contact information for the defunct companies, let me know.  Even if it's just a definitive "sorry, this never taped out", that's better than nothing. 


## 3dfx

* Voodoo 5 6000.  Sure, why not. 
* Also, just about any sufficiently interesting Quantum3D card is fair game. 

## 3dlabs

* Anything newer than what's supported by the gamma(4) driver.  I've got a Wildcat VP760.  I'm sure there's more than one generation of Wildcat though. 

## Ark Logic

* 8100 (Tiger3D) and 8300 (Cougar3D).  If they exist. 

## ArtX

* Anything!  They look like they were in business just long enough to sell some of their own kit before being bought by ATI (ATI paying to have ArtX take over really).  I already have a GameCube, thanks though.

## ATI

* Rage Fury Maxx.  Dual rage128.  I have one but I don't think it POSTs anymore. 

## Barco

* Did some medical imaging cards, among other things.  Some were Number Nine chips with extra output cleverness, but there seem to have been some custom ASICs too. 
* Update, 2008-05-01: I have two now.  One's a pair of Cirrus chips behind a PCI bridge.  The other is a custom ASIC.  The PCI ID is for "Metheus Corporation", who Barco definitely bought.  Despite having a ROM, it's not VBE-compliant.  And, terrifyingly, the string "Tseng" appears in it.  The ASIC name appears to be "Aura", according to the ROM. 

## Chromatic Research

* mPact.  Actually I have one, don't remember which one though (there's two, according to pci.ids).  These were early shader-like hardware, so I would dearly love to find programming information or a competent driver disassembly. 

## Diamond

* Various FireGLs.  This is a bit misleading since Diamond never did silicon, but wikipedia [[lists|http://en.wikipedia.org/wiki/FireGL#List_of_Cards]] basically four varieties: FireGL 1 - 4 with IBM chips, 1000 through 3000 just boring old Permedias, 4000 is some wacky Mitsubishi chip and 5000 is something totally alien.  I'd love to lay hands on a 4000 or 5000, I have examples of the other two varieties. 

## Dome

* Same story as Barco, really.  Later sold to Planar but still sold under the Dome marque.  Not sure any of these were non-#9 chips, to be honest. 

## Evans and Sutherland

* Pretty much any REALimage chip.  I have a Lightning 1200 and a Tornado 3000.  The E&S chips look like they also made their way into AccelGraphics' AccelGalaxy and other boards.  See also RealVision below.

## Intel

* Intel 752 or 754 cards.  These are rumored to exist but I've never seen one in the wild.  Should be more or less i810 graphics on a card? 

## Intergraph

* Don't really know the genealogy here.  I have a few cards, need to document them better.  These are probably predecessors of the post-glint 3dlabs kit. 

## iXMicro

* Anything.  I had a Twin Turbo ages ago but gave it away to daniels.   

## Matrox

* QID.  Pretty sure this is just a Parhelia with two extra output pipes.  Almost certainly, pci.ids lists QIDs as variants of other P-series chips.  I have a Parhelia and a P650, but it's a little unclear where the register banks for the other two CRTCs on a QID live. 

## Neomagic

* MagicMedia 256XL+.  The only one with a 3D engine.  Almost certainly only to be found in laptops.  The OQO model 01 possibly?

## Number Nine

* Imagine 2 (PCI ID 105D:2339) 
* Ticket 2 Ride (PCI ID 105D:493D) 
* Anything done by [[Silicon Spectrum|http://siliconspectrum.com/]]. 

## NVIDIA

* NV2.  Assuming any ever made it outside NVIDIA campus. 

## Oak Technology

* Warp 5.  If it exists.  Wikipedia has a picture of a prototype board. 

## RealVision

* Anything.  Corporate takeover history leads me to believe they inherited the gfx group from E&S, so these might be related kit.  I don't speak Japanese though. 

## S3

* DeltaChrome and GammaChrome.  I have a Chrome S27.

## Tech Source

* Raptor 2100T.  Yet more ATC hardware. 

## Tritech

* Pyramid3D.  Eval hardware definitely produced, definitely never shipped to retail. 

## Tseng Labs

* ET6300.  Again, not sure this ever reached market. 

## XGI

* Volari V8 Duo.  If it shipped. 
* XP10. 