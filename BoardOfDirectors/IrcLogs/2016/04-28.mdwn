[[!format txt """
[22:45:28] [connected at Thu Apr 28 22:45:28 2016]
[22:45:38] [I have joined #xf-bod]
[22:51:11] <keithp> yup, I'm here for a short while
[22:52:18] <agd5f> I'll be here
[23:00:09] <danvet> hi all!
[23:00:32] <danvet> Agenda: election, offices, spi, x.org, gsoc
[23:00:57] <danvet> anything else?
[23:01:07] <robclark> o/
[23:01:11] <danvet> \o
[23:01:13] <keithp> isn't that enough?
[23:01:27] <bryce> keithp, never ;-)
[23:02:05] <robclark> are we roughly the same distribution of timezones now?  Otherwise that is a topic..
[23:02:57] <bryce> robclark, pacific tz here
[23:02:58] <whot> we still have .au, .eu and both us coasts
[23:03:36] <robclark> ok, so I guess the timeslot stands
[23:03:41] <danvet> yeah, mupuf still screwed mostly ;-)
[23:04:04] <danvet> did egbert say he can't make it?
[23:04:15] <danvet> tried to ping him already earlier today
[23:04:19] <mupuf_> iäm here!
[23:04:28] <danvet> mupuf_, hi!
[23:04:28] <mupuf_> bryce: welcome!
[23:04:35] <bryce> mupuf_, thanks :-)
[23:04:40] <agd5f> hi
[23:04:48] <keithp> I need to disappear shortly if possible
[23:05:24] <danvet> keithp, only thing would have been x.org domain, but if egbert isn't around we can discuss that next time around I'd say
[23:05:52] <keithp> sounds good. I'm thinking that once SPI "owns" the x.org assets, we can ask Leon to transfer it to them
[23:05:53] <egbert> hi gents!
[23:06:05] <danvet> oh cool, egbert's around!
[23:06:19] <danvet> we have everyone, let's get rolling
[23:06:37] <egbert> sorry for being late, had to connect to my screen session at home first
[23:06:38] <danvet> congrats to new/reelected board members agd5f, egbert, keithp &bryce
[23:06:45] <keithp> tnx! and welcome Bryce!
[23:06:46] <egbert> thx!
[23:06:56] <danvet> and thanks a lot for outgoing board member marcoz who decided to step down
[23:07:07] <egbert> right! welcome our new Board Member!
[23:07:17] <bryce> thanks all, looking forward to it
[23:07:27] <keithp> two in one city
[23:07:39] <egbert> and same, thanks for all the work to marcoz. hope to see you on xdc :)
[23:07:41] <keithp> we can do pancakes
[23:08:01] <marcoz> most welcome.   and yes I'll be there!  :)
[23:08:11] <danvet> marcoz, cool
[23:08:46] <bryce> keithp, heh yep
[23:08:50] <danvet> next up I guess is offices ...
[23:09:06] <danvet> I'm stupid enough to do secretary for another year I think
[23:09:15] <egbert> about the domain, i cannot say anything as i haven't had the chance to do anything. but since keithp is here, keithp, can you send me the status how far you had got there?
[23:09:40] <keithp> egbert: I was thinking that once we've transfered our legal entity to SPI, we can ask Leon to transfer the domain to them
[23:09:49] <danvet> egbert, you definitely scored a big mandate with your election platform though ;-)
[23:09:49] <keithp> seems like a good opportunity to me
[23:10:12] <egbert> danvet: yup
[23:10:24] <egbert> keithp, true
[23:10:39] <danvet> keithp, so would need to restart discussion with leon and sflc and everyone anyway I guess?
[23:10:42] <keithp> perhaps approach him now and see if that's a good idea
[23:10:54] <danvet> yeah I think that would be good
[23:11:01] <egbert> keithp: i'll contact him.
[23:11:04] <keithp> tnx
[23:11:18] <danvet> asking leon how he thinks of transferring to spi and whether he has concerns/conditions/yadayada
[23:11:26] <keithp> I'm gonna get the cat settled elsewhere for his nap and take off then. Have fun kids!
[23:11:31] <danvet> egbert, cool, that means we've covered x.org too
[23:11:35] <danvet> thx for doing this
[23:11:46] <egbert> sure!
[23:12:16] <danvet> ok, back to secretary
[23:12:26] <danvet> anyone else or are we going to do a 7 yay 1 abstain once more?
[23:12:55] * danvet needs to amortize the script more
[23:14:42] * mupuf_ thinks danvet is doing an amazing job!
[23:14:45] mupuf_ is now known as mupuf
[23:14:54] <egbert> yup!
[23:16:17] <danvet> folks? or everyone happy to not be volunteered?
[23:16:37] <whot> pretty much :)
[23:16:45] <robclark> was that a vote?  if so +1 for secretary danvet
[23:16:47] <egbert> danvet: i think everybody feels you've done a superb job!
[23:16:57] <whot> you have to hold out until XDC and then pass it to someone else there ;)
[23:16:59] <danvet> the glowing praise, it's awesome ;-)
[23:17:35] <danvet> robclark, seem no opposition anyway
[23:17:41] <danvet> next up is treasurer
[23:17:56] <danvet> stukreit, you're ok with doing it for a little longer until it's all handed of to spi?
[23:18:13] <danvet> but I think we should look for someone already to take over already ...
[23:18:19] <danvet> bryce, robclark ?
[23:18:26] <danvet> always aim for the new ones ;-)
[23:18:36] <bryce> danvet, I'd be open.  I do treasuring for Inkscape among other things
[23:18:44] <danvet> with spi handling the paperwork it shouldn't be much work at all
[23:18:55] <egbert> it should be someone in the us i guess as it would make things with the bank easier if you have to show up there
[23:18:57] <robclark> then +1 for bryce :-)
[23:19:01] <danvet> oh, someone with experience, practically elected!
[23:19:06] <bryce> heh
[23:19:09] <robclark> right :-)
[23:19:17] <danvet> egbert, spi will handle bank for us now
[23:19:24] <danvet> but tz overlap is still good I'd say
[23:19:26] <egbert> until then?
[23:19:28] <agd5f> +1 for danvet as sec.  and +1 for bryce as treasurer
[23:19:49] <danvet> egbert, stukreit I hope, he agreed to run the show until we hand over to spi
[23:19:55] <egbert> ok
[23:19:55] <mupuf> +1 for danvet and +1 for bryce
[23:19:55] <danvet> assuming it'll happen in 2016
[23:20:01] <danvet> which we managed to pull off
[23:20:06] <bryce> and I'll +1 danvet as secretary
[23:20:08] <egbert> same here +1/+1
[23:20:18] <danvet> bryce, didn't actually want to elect you right away tbh ...
[23:20:29] <danvet> but you're ok with that?
[23:21:14] <bryce> danvet, I'm fine with it.  I'll need to get up to speed but am assuming there'll be time before the transition?
[23:21:20] <danvet> cool, awesome
[23:21:28] * danvet +1 for bryce
[23:21:35] <whot> at the speed we're usually going, yes, there will be time :)
[23:21:42] <danvet> whot, keithp?
[23:21:43] <whot> +1 for bryce, +1 for danvet
[23:22:10] <danvet> bryce, an yeah the idea is that you'd get up to speed together with stuart while we transfer stuff to spi, so you'd know the spi treasure and process and all that
[23:22:11] <bryce> I'll apprentice to stukreit for now if he's willing to mentor
[23:22:16] <danvet> and would take over once spi holds our funds
[23:22:19] <danvet> yeah
[23:22:34] <bryce> ok sounds good
[23:23:03] <danvet> so next up is spi
[23:23:17] <danvet> thanks to massive PR we dragged enough folks into the voting booth, and we're joining!
[23:23:19] <danvet> \o/
[23:23:40] <danvet> I'd say I write a mail to Martin starting transfer stuff
[23:23:49] <danvet> with bryce/stuart on cc for money stuff
[23:23:55] <danvet> egbert for the x.org troubles
[23:23:59] <mupuf> yeah!
[23:24:03] <danvet> and sflc/mishi for legal stuff
[23:24:17] <danvet> since I'm not sure whether sflc wants to retain the legal body or not
[23:24:27] <danvet> anything else I should mention or someone I should include?
[23:24:31] <danvet> ofc cc both boards
[23:25:22] <danvet> egbert, keithp, whot  or any of the other oldtimers?
[23:25:52] <whot> cc the board list should be enough?
[23:26:04] <egbert> danvet: not right off hand. if we need to we can still relay things to others later.
[23:26:46] <danvet> whot, wanted to include individuals so the right mail addresses are known
[23:26:50] <danvet> in case
[23:27:08] <danvet> egbert, good point, I'll type that tomorrow then ...
[23:27:12] <whot> right
[23:27:23] <egbert> danvet: sure, maybe please also add the board list.
[23:27:33] <danvet> egbert, sure
[23:27:49] <danvet> anything else wrt elections/spi?
[23:27:56] <egbert> this is so that everybody who is not so familiar with the legacy (Bryce!) gets to know some of the ins and outs.
[23:28:13] <bryce> yeah I'll need to study up on this
[23:28:16] <danvet> it's amusing ...
[23:28:34] <egbert> unless you are involved in it yourself ...
[23:28:39] <bryce> heh
[23:29:27] <danvet> oh right
[23:29:33] <danvet> whot, you added bryce to board@ already?
[23:29:52] <bryce> I can sympathize; we had to do similar legal paperwork stuff for Inkscape with the Software Conservancy.
[23:29:53] <whot> no, but I'll chase that up, iirc I'm not a board admin but I have it somewhere
[23:30:25] <whot> agd5f: my notes say you are board@ list admin
[23:30:28] <danvet> bryce, oh and you're preferred mail for board stuff pls
[23:30:30] <egbert> bryce: difference is that you did not have a 20+ y legacy ;)
[23:30:32] <bryce> is there a list of tasks somewhere for the remaining work for the SPI merger?
[23:30:36] * danvet updating wiki right now
[23:30:42] <danvet> and I only have @canonical
[23:30:52] <danvet> bryce, we're not that structured ...
[23:31:31] <agd5f> I can add bryce once I get his email.
[23:31:35] <bryce> danvet, ah that's defunct.  Maybe use Bryce Harrington <bryce@osg.samsung.com>
[23:31:44] <agd5f> preferred email
[23:32:01] <bryce> danvet, secondary/permanent email is Bryce Harrington <bryce@bryceharrington.org>
[23:32:53] <danvet> bryce, pls double-check http://www.x.org/wiki/BoardOfDirectors/
[23:33:15] <danvet> ok, I think that's it, otherwise if there's more we can chat on board@
[23:33:20] <danvet> mupuf, agd5f gsoc update?
[23:33:36] <mupuf> we got 2 students!
[23:33:42] <mupuf> that's way lower than usual
[23:33:51] <danvet> :(
[23:33:56] <mupuf> but wayland has its own org
[23:34:01] <mupuf> and they got one sutdent
[23:34:06] <agd5f> bryce, is the samsung email preferred for board stuff?
[23:34:07] <mupuf> so, that would be 3
[23:34:09] <danvet> so 3 altogether ...
[23:34:14] <bryce> danvet, looks good
[23:34:14] <mupuf> which is lower than the 5 we usually gety
[23:34:17] <bryce> agd5f, yes
[23:34:26] <danvet> bryce, btw any legal disclaimer from samsung I need to take on record
[23:34:27] <danvet> ?
[23:34:37] <bryce> danvet, good question, I'll enquire
[23:34:39] <mupuf> so, we need to spend more time on advertising this, coming up with better ideas earlier too
[23:34:50] <danvet> some companies want/need something along the lines of "not representing $company on this board"
[23:35:01] <danvet> mupuf, hm yeah
[23:35:06] <bryce> Samsung is sponsoring my membership but I'm not formally representing their interests or anything
[23:35:14] <danvet> this time around I totally missed adding the kernel gsoc stuff to the page
[23:35:29] <danvet> although I did keep http://www.x.org/wiki/DRMJanitors/ up to date
[23:35:33] <mupuf> yeah, I did it for you
[23:35:36] <mupuf> I added the page
[23:35:38] <danvet> ah cool
[23:35:46] <bryce> danvet, yeah that verbage would probably be appropriate.  I'll check and follow up with you offline.
[23:36:02] <danvet> bryce, HR should have a readymade process/template if they care
[23:36:05] <danvet> otherwise meh
[23:36:24] <bryce> inkscape only got 2, which is down from the past.  I think google may be spreading the slots more broadly or something
[23:36:41] <agd5f> added
[23:36:42] <danvet> mupuf, wasn't it also a lack of good candidates on our side?
[23:36:44] <danvet> agd5f, thx
[23:36:50] <mupuf> danvet: yes, also
[23:36:54] <danvet> mupuf, or lack of mentors too?
[23:36:57] <mupuf> we had 7 proposals
[23:37:07] <mupuf> 5 were properly submited
[23:37:13] <mupuf> 3 were for the same project
[23:37:25] <bryce> danvet, same thing for inkscape.  If we had gotten more slots we probably would have given them up due to lack of good candidates.
[23:37:29] <mupuf> one of which was serious
[23:37:39] <agd5f> I'll drop marcoz and I guess keep stuart on the list for now until the treasurer transition is complete
[23:37:51] <danvet> mupuf, have you chatted with sarahsharp about this?
[23:38:03] <danvet> she's all over onboarding and dragging new folks into projects
[23:38:05] <robclark> mupuf, we perhaps need to emphasis a bit better the pre-req's and try to avoid students missing the deadline due to waiting too long to submit a patch, etc..
[23:38:11] <danvet> agd5f, sounds good
[23:38:37] <mupuf> danvet: well, I would need to talk to her about it
[23:38:48] <danvet> mupuf, also do we have some wiki somewhere so we don't forget stuff?
[23:38:59] <danvet> like we've done with the xdc rfp thing last year ...
[23:39:01] <mupuf> danvet: forgot what?
[23:39:13] <mupuf> ah right
[23:39:26] <danvet> do good PR upfront, make sure students aren't late, that kind of silly stuff that's easy but also easily forgotten
[23:39:46] <mupuf> danvet: I could create a page with the timing
[23:40:06] <danvet> mupuf, that might help for next time around, then we could flesh it out some more
[23:40:17] <danvet> and start brainstorming what to do better perhaps
[23:40:28] <danvet> anyway, so no real updates compared to last time around?
[23:40:38] <danvet> I think I had the 2 students already covered
[23:40:51] <mupuf> danvet: well, the news is that google announced it
[23:41:07] <danvet> ah cool
[23:41:11] * danvet types that down
[23:41:44] <danvet> so prep phase now until gsoc starts for real?
[23:41:50] <mupuf> yes!
[23:42:45] <danvet> cool
[23:42:58] <danvet> so sounds like we're done, slightly short even
[23:43:02] <danvet> anything else?
[23:43:18] <mupuf> no news for XDC... sorry...
[23:43:19] <whot> nothing from me
[23:43:22] <danvet> otherwise goodnight/day/whatever it is where you are!
[23:43:27] <agd5f> nothing here
[23:43:37] <danvet> mupuf, oh I just assume that's going to happen, and you'll pipe up if you need anything ...
[23:43:48] <mupuf> yep yep!
[23:43:52] <danvet> mupuf, should I chat with kimmo about it?
[23:44:13] <mupuf> nope, I just need to go to the Uni and see the room
[23:44:16] <danvet> or all good
[23:44:17] <danvet> ah ok
[23:44:21] <mupuf> actually, yeah, ping him about the catering
[23:44:30] <mupuf> cattering ?
[23:44:53] * mupuf keeps forgetting the spelling :D
[23:45:01] <danvet> yeah with all the budget nonsense going on ...
[23:45:21] <danvet> anyway, sounds like we're all done
[23:45:33] <danvet> bryce, thx once more for signing up as treasurer
[23:45:42] <danvet> g'night all
[23:45:52] <egbert> good night!
[23:46:06] <robclark> gn
[23:46:19] <mupuf> whot: gday mate!
[23:46:25] <bryce> night!
[23:46:26] <mupuf> to everyone else, good night!
[23:47:37] <whot> mupuf: heh. g'day is a greeting, not a farewell :)
"""]]
[23:47:45] [disconnected at Thu Apr 28 23:47:45 2016]
