[[!format txt """
[22:47:51] [connected at Thu Sep  1 22:47:51 2016]
[22:48:02] [I have joined #xf-bod]
[22:53:33] <bryce> danvet_, ah thought you were on vacation
[22:54:53] <bryce> danvet_, yes bunch of treasurer progress/stuff to discuss, although nothing urgent
[22:56:14] <bryce> my kids' have a school event this afternoon, so was hoping no meeting so I could attend :-)
[22:57:45] <danvet_> bryce, vacation starts tomorrow
[23:00:23] <danvet_> hi all
[23:00:53] * whot waves
[23:01:01] <egbert> hi guys!
[23:01:09] <danvet_> Agenda: SPI transfer, financial records,
[23:01:09] <danvet_> x.org, gsoc
[23:02:07] <danvet_> mupuf, keithp robclark: around?
[23:02:20] <robclark> o/
[23:02:39] * robclark got confused by /topic
[23:02:57] <danvet_> is it wrong?
[23:03:25] <robclark> hmm, nm, it says Jun 9 not Sep 9
[23:03:48] * robclark needs bigger fonts
[23:03:53] <danvet_> hm, I don't update those ...
[23:05:21] <bryce> danvet_, mind putting the treasurer stuff first?  If I can get through it in 15 min I'd like to nip out to my kids school event
[23:06:43] <danvet_> bryce, yeah, I guess let's start
[23:06:48] <danvet_> with you
[23:07:25] <danvet_> oh maybe point of order first, next mtg is in helsinki since I'm on vacation in 2 weeks
[23:08:58] <bryce> danvet_, on the SPI transfer, current project is the dissolution for X.org
[23:09:54] <bryce> danvet mailed me the signed document for my signature.  It looks like there's a form and $204 fee that needs submitted along with it.
[23:10:38] <bryce> I want to do a bit more investigation to understand better what we need to do exactly; there's several options to take, and given we're not a stock company we can follow one of the simplified processes
[23:10:55] <bryce>  
[23:11:29] <bryce> next, on the bank account
[23:11:52] <bryce> Stuart and I transferred credentials by phone, and I now have access to the account.
[23:12:38] <bryce> so that's a big step forward.  If we need money transfers and so on, I'll be able to handle that going forward
[23:12:39] <danvet_> dunno how this goes in the us, but does the bank also recognize you as someone with permission?
[23:12:53] <danvet_> probably matters for closing it ...
[23:12:56] <bryce> eventually once the money's transferred to SPI we can shut down the account
[23:14:48] <bryce> danvet_, the account is still under Stuart's name.  He wants to eventually transition that over so he's no longer tied to it, but I will need to do some more investigation as to what all we can/should change
[23:14:56] <bryce>  
[23:15:07] <danvet_> well maybe we'll just shutter it before that ;-)
[23:15:21] <egbert> does it make sense to transfer it even though we are going to close this very soon?
[23:15:29] <bryce> yeah that's what I'm wondering.  we can see how it goes, we'll still need it for a bit
[23:15:40] <danvet_> sure
[23:15:57] <egbert> from what i remember from our old hsbc account it was a hassle to do such things
[23:16:24] <bryce> on the topic of the banking records, last meeting we discussed encryption options for the git repo
[23:16:57] <danvet_> yeah
[23:17:03] <bryce> I posted some detail about the options.  I don't have a super strong preference for what way we go there.
[23:17:04] <danvet_> I'd kinda vote for git crypt
[23:17:13] <danvet_> since that allows us to encrypt only what needs to be encrypted
[23:17:21] <danvet_> makes it easier for me as secretary ;-)
[23:17:33] <danvet_> bryce, have you tried out any of them yet?
[23:17:36] <bryce> nope
[23:17:54] <bryce> one thing I'm not sure of if it requires administrative access to set up
[23:17:59] <danvet_> I guess in the end it should work decently for us two, and be accessible for everyone else
[23:18:09] <danvet_> we can poke daniels
[23:18:17] <bryce> if any one is interested/willing to help with experiments and/or set up, I'd hugely appreciate it
[23:18:33] <danvet_> probably not before Nov for me, because vacation/travel
[23:19:02] <bryce>  
[23:19:12] <robclark> where did we want it to live?  Just on fd.o?  In that case probably a daniels request..
[23:19:13] <danvet_> I guess we can postpone this one for now?
[23:19:28] <robclark> (but for things that don't need fd.o admin, I can help)
[23:19:37] <danvet_> robclark, atm the bod git repo is on fdo servers, but our own private x.org one
[23:19:43] <danvet_> so only bod + fd admins can get at it
[23:19:53] <robclark> k
[23:20:02] <bryce> last treasurer topic, I've been sorting through the records to understand our financial ins and outs.  Still WIP but hoping to have something sharable maybe next meeting or after.
[23:20:24] <danvet_> bryce, you'll be at xdc?
[23:20:43] <bryce> oh right, next meeting's xdc
[23:20:48] <bryce> so meeting after next then
[23:21:01] <danvet_> well there's one in 2 weeks, but I'm not here and then right afterwards xdc
[23:21:07] <danvet_> so figured I'll cancel that
[23:21:36] <danvet_> bryce, anything else?
[23:21:44] <bryce> oh ok, well in any case, coming soon.  If anyone knows of pending future expenses, I'd like to be aware just for tracking.
[23:21:49] <bryce> danvet_, nope, thanks all.
[23:22:20] <danvet_> ok, so gsoc seems to have completed successfully, XDC is on track and I guess everything else we can discuss at xdc
[23:22:29] <danvet_> I'd like to close if no one opposes
[23:22:33] <danvet_> since I still need to pack ;-)
[23:22:59] <bryce> cheers
[23:23:01] <egbert> fine with me.
[23:23:05] <egbert> good night!
[23:23:16] <jcristau> danvet_: enjoy the time off
[23:23:19] <danvet_> bryce, thx a lot
[23:23:19] <robclark> bryce, I guess an EVoC payment sometime in the nearish future.. I need to double check timeline for varad's msaa project..
[23:23:39] <danvet_> robclark, we didn't yet approve that one, right?
[23:24:16] <robclark> danvet_, no, that was the other EVoC project for the guy who had only 1mo summer vacation
[23:24:28] <danvet_> ok, just wanted to check
[23:24:30] <robclark> (or you mean approve the final payment?)
[23:24:36] <danvet_> approve the project
[23:27:05] <robclark> ahh, found it..  danvet_, mtg minutes from "2016-06-09"
"""]]
[23:29:45] [disconnected at Thu Sep  1 23:29:45 2016]
