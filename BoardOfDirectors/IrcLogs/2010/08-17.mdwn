
Date is 2010-08-17, times are UTC+02. 
[[!format txt """
23:03 <mattst88> (someone ping me when it's an appropriate time to pose a question to the board)
23:03 <+dberkholz> hi
23:04 <+emmes> mattst88: ping
23:04 <agd5f> mattst88: go ahead
23:04 <+mherrb> jokes apart, if you tell me when you arrive and feel you need a bit of assistance on arrival, I'm planning to be available all of the day  before.
23:04 <+emmes> I would say, time is appropriate as it gets
23:04 <mattst88> I wanted to ask this after I'd completed my GSoC, else it would be a bit inappropriate:
23:04 <+emmes> mherrb: noted :-) and appriciated :-)
23:05 <mattst88> I boughht 60~65 dollars worth of video cards for the project, and Ian suggested that the Foundation might reimburse me for this.
23:05 <mattst88> IIRC he mentioned it to Keith and Bart, but I haven't pursued this any further in a few months.
23:06 <+emmes> mattst88: what was your GSoC again? Sorry, Alzheimer...
23:06 <mattst88> KMS for glint, and documenting the process for writing a KMS driver.
23:07 <+emmes> yep, I think I remember now.
23:07 <alanc> $65 sounds well within the foundation's budget, especially if someone filled out the paperwork to get our $500 mentoring fee from GSoC
23:08 <mattst88> OK, great. I think I should send documentation of all this to board@.
23:08 <mattst88> Is there anything else I should do?
23:08 <alanc> do the board members want to vote now or wait until he sends the details?
23:09 <alanc> (I don't see bart in channel - are keithp & anholt actually here?)
23:09 <+anholt> yeah
23:09 <+emmes> if he passed GSoC I'm fine with it anyhow :-)
23:09 <mattst88> emmes, yep, I'm in good shape. :)
23:09 <+anholt> agreed on "sounds like a good idea", just a bit of notes on what you purchased in an email would probably be good.
23:10 <+dberkholz> i'm fine with approving it now. if you have receipts, those might be useful to have
23:10 <+mherrb> I'm ok on the principle. Just check the details to make sure it's really video cards :)
23:10 <mattst88> dberkholz, yes, of course. I'll send those along.
23:10 <alanc> I vote +1 for reimbursing approx. $65 worth of video cards
23:10 <+dberkholz> we may eventually need that kind of documentation for audits
23:10 <+anholt> +1
23:10 <agd5f> +1
23:10 <+dberkholz> +1
23:10 <+emmes> +1, agreeing on docs
23:10 <+mherrb> +1
23:11 <+Bart_Massey> Apologies for my extreme lateness.
23:11 <alanc> so mattst88 please send details to board@foundation and cc stuart.kreitman@oracle.com (our treasurer) to get the reimbursement
23:12 <mattst88> OK, will do so. Thanks!
23:12 <alanc> and we'll work out via e-mail what sort of reciepts are needed for our records
23:12 <+dberkholz> Bart_Massey: we just voted to reimburse mattst88 for $65 of video cards he bought for gsoc.
23:12 <+Bart_Massey> Excellent
23:12 <+Bart_Massey> +1
23:13 <agd5f> not much new on the 501c3 side.  apparently mail was broken for ~2 weeks, so if the board could review my email and give me their opinions on the questions, I'll take that back to Karen.  thanks to those who already replied
23:13 <+dberkholz> i'm in agreement on accepting copyright/TM. not sure on the tradeoffs of public vs private.
23:14 <alanc> so as long as we're spending money, I think we have 4 requests for XDS travel sponsorship
23:14 <+mherrb> (plus the one of Peter that we already accepted)
23:15 <agd5f> dberkholz: you generally want to be public, but that has some requirements you have to meet over the following 5 years
23:16 <+mherrb> and Jeremy Huddlestone (sp?) from apple asked me details, saying Apple won't pay for him. But I saw him remove his name from the attendees wiki today. I don't know what's up.
23:16 <alanc> plus one offer from Intel to provide funding for travel
23:16 <+dberkholz> agd5f: yeah, i recall one of the bsd's having serious issues with too much money coming from a small number of sources
23:17 <+dberkholz> not sure whether we would be affected by that too
23:17 <agd5f> some kind of 30/10 rule with regard to donations over the next 5 years.  If we don't meet the requirements we become a private charity, so no big deal
23:17 <+dberkholz> does that mean we lose tax deductibility?
23:17 <agd5f> no
23:17 <agd5f> just slightly stricter regulation in some regards.  I don't remember all the details
23:18 <+emmes> regarding travel sponsorship I guess we should fund anyone who's willing to do a talk. Opinions?
23:18 <+Bart_Massey> Up to the limits of our funding I think that's right
23:18 <agd5f> sounds good to me
23:18 <+Bart_Massey> Assuming we'd have any interest in the talk
23:18 <+emmes> that goes without saying :-)
23:18 <+Bart_Massey> I mean, vendor commercials don't count, for example
23:19 <+Bart_Massey> BTW, I'm planning to apply for some funding as well in the next day or two.
23:19 <alanc> or people we think should be present for some of the discussions/BoFs
23:19 <+emmes> right
23:19 <+Bart_Massey> Better to handle me via email, though, I think.
23:19 <+Bart_Massey> I've got a talk. :-)
23:20 <alanc> for instance, the OpenBSD DRI developer would be good for the proposed BoF on DRI/KMS for non-Linux platforms
23:20 <+Bart_Massey> Sure.
23:21 <alanc> all 3 requests/4 people I saw come in during yesterday's resumption of email delivery seemed reasonable to me
23:21 <+Bart_Massey> Me too.
23:21 <agd5f> me too
23:21 <+mherrb> and mee too.
23:22 <alanc> now if we could only get mail delivery to be reliable
23:22 <+Bart_Massey> Tolef thinks he has that under control now.  Hope he's right.
23:22 <+Bart_Massey> Mailman wedges all the time for everyone; just need a plan for noticing and/or unwedging it
23:23 <alanc> does anyone want to discuss more or vote on the requests separately, or just go ahead and vote now on approving the travel funding for Matt, Jamey, Josh & Owain?
23:23 <+Bart_Massey> I move we approve all the requests
23:24 <+mherrb> agreed
23:24 <+emmes> +1
23:24 <agd5f> +1
23:24 <alanc> +1
23:25 <+mherrb> +1
23:25 <alanc> looks like that's about $3k USD for the 3 that gave estimates, plus an unknown amount for Matt to travel from the US
23:25 <+anholt> +1
23:25 <+Bart_Massey> Matt may actually still be in China; don't know yet.
23:25 <+Bart_Massey> Can't remember
23:26 <alanc> ah, well, then about the same distance the other way around
23:26 <+mherrb> alanc a bit more for owain: he only gave me an estimate for the train. have to add 2 or 3 hotel nights.
23:27 <+Bart_Massey> Anyway, call it $5K for the lot; that still leaves plenty of headroom on Intel's generous donation.
23:27 <alanc> still, leaves most of Intel's offer left to cover additional requests (Jeremy?) or the social event
23:27 <+Bart_Massey> Don't y'all wish we were still doing two of these per year? :-)
23:28 <alanc> sure, approving spending other people's money is no sweat at all
23:28 <alanc> the organizing bits, well, we all owe mherrb much gratitude since that's the hard work
23:29 <+Bart_Massey> Indeed.  thank you mherrb!
23:29 <+Bart_Massey> And yes, the organizing and planning was what I was commenting on. :-)
23:30 <alanc> anything else for XDS we need to discuss today?   we should all be there in a month
23:30 <+emmes> any time table yet?
23:30 <+Bart_Massey> Want to talk about X.Org logo copyright?
23:30 <+Bart_Massey> Oh, sorry, XDS.
23:30 <+emmes> saturday could prove difficult for me, if we plan any bod meeting...
23:31 <alanc> last I looked at the schedule it was very light - need to encourage more people to put down topics/talks
23:31 <+emmes> so chances are we do the bod meeting thu or fri evening, correct?
23:32 <alanc> I will be in transit during our normally scheduled board meeting on 9/14 - I assumed that would be replaced with an in person "Chat with the Board" session during one of the conference days
23:32 <+emmes> sounds reasonable
23:32 <alanc> or maybe I'll already be in Toulouse by then, the time zones are confusing me
23:33 <+emmes> it's 11pm-12pm. if that helps :P
23:33 <+mherrb> emmes: one evening for the social event, the other for a board meeting.
23:34 <+mherrb> alanc: generally travelling from us->europe you loose one day.
23:34 <+emmes> mherrb: sounds good!
23:35 <+mherrb> I will probably set the day of the social event next week (the people are on vacation now)
23:36 <alanc> ah right, I will be airborne over the US at 2pm Tuesday, arrive in Toulouse at 14:30 Wed
23:38 <agd5f> I'll be on a plane as well tuesday
23:38 <agd5f> arriving at 8:15 wed
23:38 <agd5f> er 11:30 rather
23:39 <+emmes> I'll arrive pretty damn late in the evening on wed. 11pm.
23:40 <+emmes> mherrb: any ideas whether the bus to midtown is still running that late?
23:40 <+mherrb> yes, it runs up to the last plane
23:41 <+mherrb> the last one leaves at 0:15am. so you have some margin for plane delays
23:41 <agd5f> what hotels are people staying at?
23:41 <agd5f> still need to book mine
23:42 <+Bart_Massey> Not to derail the meeting, but I've got grades due in a couple of hours.  Any chance we could get the other stuff done first? 
23:42 <agd5f> Bart_Massey: sure.  what else on the agenda?
23:42 <+emmes> rt_Massey: do you know the status of our logo? appart from that we have the copyright?
23:42 <alanc> okay - X.Org logo copyright/trademark?
23:42 <+mherrb> Irecommend the Albert 1er hotel if possible. 
23:43 <+Bart_Massey> I think even the copyright is mildly dicey given the bad records we have from TOG.
23:43 <+Bart_Massey> Our best plan, far and away, is probably to get a new logo that is less ugly and that we unarguably own, and trademark it.
23:43 <alanc> and would it cover the original logo with just the X or the version we have on the website now with the added loop?
23:44 <+Bart_Massey> If we run a contest, we can do the logo for less than $500
23:44 <+Bart_Massey> The old MIT X logo is probably hopeless at this point; I don't think we have any control over it
23:44 <alanc> http://www.nettv.tv.br/images/stories/logo.gif is a nicer rendering of it than ours though
23:44 <+Bart_Massey> It will probably be another $300-$500 to get all the trademark reg stuff done, unless SFLC will do it for free.
23:44 <+Bart_Massey> alanc: Indeed :-)
23:44 <+Bart_Massey> It's still an ugly logo
23:45 <agd5f> there's still the trademark application and maintenance fees
23:45 <+Bart_Massey> Yes, the fees are a couple of hundred dollars IIRC, plus something like $50 / year.
23:46 <+Bart_Massey> The principal barrier here is finding someone to run all this, I think.  I am *not* volunteering. :-)
23:47 <+Bart_Massey> BTW, that brings up an old, old agenda item.  We had voted a long time ago to donate some money to SFLC based on their help to date.  We should revisit that and actually do it this time. :-)
23:47 <agd5f> indeed.  I very much agree with that
23:48 <+emmes> that must have been before my time. not that I'm against it ;)
23:48 <alanc> it doesn't necessarily need to be a board member, we could put out to the community to ask for volunteers for organizing a logo replacement
23:48 <+Bart_Massey> alanc: excellent point.  someone put a note to the xorg list.
23:49 <alanc> yeah, I don't remember discussing an SFLC donation, but would approve of one
23:49 <+Bart_Massey> i don't think many of you were on the Board at that time. :-)
23:49 <alanc> I can send a note to the list to ask for volunteers when I do the minutes
23:50 <+Bart_Massey> I was thinking O($7500), but some other Members thought O($5000) was sufficient.  What do other folks think of amounts?
23:50 <+Bart_Massey> alanc: thanks
23:51 <alanc> agd5f is probably in the best position to know how much time they've put in to helping on the 501(c)3 stuff for us
23:51 <+emmes> I can't tell until there is *some* list of things they helped us with. I understand they help Alex with the 501 stuff.
23:51 <+Bart_Massey> Remember, it's been 3+ years. :-0
23:51 <agd5f> they were also involved in the switch from llc to non-profit
23:52 <+Bart_Massey> Indeed
23:53 <+emmes> Right. Just asking because $5000 and up isn't exactly nothing. I know lawers can be expensive, though.
23:54 <+Bart_Massey> Conservatively they've put in 30-40 hours on us.  At a typical $400/hour, that's 12-16K.  So we're really just doing a courtesy donation here in any case.
23:55 <alanc> $7500 seems reasonable to me
23:55 <+Bart_Massey> From talking to Karen, I think $5K-$10K would be in line with what other organizations that have used their services have donated.
23:55 <alanc> though we probably ought to get a treasurer's report soon (maybe by XDS?) to find out how much money we still have to spend
23:56 <+Bart_Massey> That would be the awesome
23:56 <+emmes> as this isn't exactly pressing, that sounds like a good idea to have first.
23:56  * Bart_Massey notes that xchat won't let him type t e h as one word
23:56 <+emmes> (first this means the donation)
23:56 <+Bart_Massey> emmes: i suppose.  but I'd hate to drop it for two years again.  let's make sure to deal with it at XDS
23:57 <+dberkholz> fwiw, we don't need to register a trademark to own it
23:57 <+emmes> :-]
23:57 <+dberkholz> trademarks are "earned" by simply using them
23:57 <+anholt> Bart_Massey: settings -> advanced -> auto replace to fix that.
23:57 <agd5f> we also have the $25 delaware franchise fee and ~$2000 in old llc taxes and fees
23:57 <alanc> okay, so we'll table the SFLC donation for now
23:57 <+Bart_Massey> dberkholz: it's complicated.  much more complicated than copyright law.  you are unlikely to want to bring legal action without a registered mark
23:57 <+Bart_Massey> anholt: thanks!
23:58 <alanc> and given our hour is up, I guess we'll wait until next time to discuss more about the MIT-hosted machines and if they're still worth the expense of hosting there
23:58  * Bart_Massey is bemused that teh->the is the sole auto-replacement
23:58 <+Bart_Massey> alanc: fair enough
23:58 <+Bart_Massey> Thanks all!  Move to adjourn
23:59 <+emmes> +1
23:59 <agd5f> +1
23:59 <alanc> +1
23:59 <alanc> thanks for coming everyone
"""]]