
Date is 2010-06-22, times are UTC+02. 
[[!format txt """
23:03 <alanc> I'm trying to remember what we have for today's agenda
23:03 <alanc> any updates on the 501(3)c paperwork progress?
23:04 <agd5f> alanc: not yet
23:05 <alanc> and I saw the review on the list of mherrb's request for XDS corporate sponsorships
23:07 <+mherrb> I sent an updated version. did you guys get it?
23:07 <alanc> anything else people have to bring up today?
23:07 <+emmes> mherrb: if it was on the same day, yes.
23:07 <+mherrb> yes it was the same day or the day after, depending on time zones.
23:08 <alanc> I saw the one with the updates after dberkholz's review
23:08 <+mherrb> there was only one update from me so its the one :)
23:09 <alanc> looked fine to me
23:10 <+emmes> looked pretty good after the review. it was definitely a good change to embedd the main purpose in the title area
23:10 <+mherrb> feel free to send to your management or other potential sponsors then :)
23:10 <+emmes> i'll see Markus Rex on the weekend, i'll check how the financial situation is then. Don't have too high hopes, though.
23:11 <+mherrb> I don't, seen the poor (==0) result I got so far locally.
23:11  * alanc will pass it up the chain here
23:12 <agd5f> mherrb: I'll run in by the amd folks
23:12 <+emmes> i'll first have to check whether there's funding for me going to xds. In that kind of situation we are :-/
23:16 <alanc> sorry for being completely out of it today, and not having minutes of last meeting to feed into this meeting's agenda
23:16 <+Bart_Massey> 'scool
23:17 <+emmes> agd5f: what is currently halting us in the 501(3)c? Just being curious...
23:19 <agd5f> emmes: finding the info needed to fill out the form.  I'll be sending a list of questions to the board this week
23:19 <alanc> if you need more help from Stuart, his office is about twenty yards down the hall from mine, so I can go ping him anytime
23:20 <+emmes> np, as I said, just being curious. And I'll probably won't be much help here (don't know much about American laws, and too little about board history as well :-] )
23:20 <agd5f> alanc: cool.  we talked last week and filled in a lot of the details.  we also need to organize the financial info for the form
23:21 <+Bart_Massey> I'll help how I can, but we really need to involve Karen / SFC; have we got the tax stuff we need for this year done yet?
23:23 <agd5f> not yet as far as I know.  stuart and keith were going to look into the financial stuff.  I've got a list that should cover what we need, I'll sent it out today or tomorrow. Once I've filled in what I can I'll send everything to Karen
23:24 <+Bart_Massey> Cool.  Karen sounded a little frustrated in her last email. :-)
23:25 <agd5f> Bart_Massey, keithp: is Karen ok with lots of questions?  I've been trying to gather as much info as I can before going to her.  I don't really have a sense of how much they do vs. us
23:25 <+Bart_Massey> If it were me, I'd send her questions until she asked me to stop. :-)
23:26 <+Bart_Massey> She's really friendly, and I think her big concern is that we're ignoring our responsibilities; more questions would be helpful there. 
23:26 <+Bart_Massey> As far as distribution of responsibilities, we need to do the actual paperwork and stuff, but they need to tell us what to do.
23:27 <+Bart_Massey> They help lots of folks like us, so I'd think they are used to these questions.
23:28 <+Bart_Massey> Are we good for now?
23:29 <+emmes> nothing from my side
23:29 <alanc> anything else for today or are we ready to adjourn?
23:29 <agd5f> I think so.  I'm sending the questions to the board.  if you know any answers, please reply
23:29 <+Bart_Massey> Thanks all!
23:29 -!- Bart_Massey [bart@barton.cs.pdx.edu] has quit Quit: Leaving
23:29 <+emmes> alanc: adjourn +1
23:30 <alanc> and I think Bart just voted the same
23:30 <+emmes> :-P
23:30 <alanc> see you all in two weeks!
23:30 <+mherrb> +1 good bye.
"""]]