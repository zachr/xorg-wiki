
Date is 2010-04-13, times are UTC+02. 
[[!format txt """
22:55 <alanc> Good afternoon.   Meeting should begin in about 5 minutes
22:56 <+mherrb> Hi
23:01 <+emmes> heho
23:01 <marcheu> hmm, we have 3 Soc slots :(
23:02 <alanc> seems to be around the typical number - I think we usually have 3-4 slots for 10-12 applicants
23:03 <marcheu> alanc: usually 4-5
23:03 <+Bart_Massey> Minute or two late; sorry.
23:03 <alanc> okay, was about to say Bart might remember better how many GSoC slots we had in the past
23:03 <+Bart_Massey> It has varied widely.
23:03 <+Bart_Massey> Sometimes just two or three.  Sometimes as many as 7?
23:04 <+Bart_Massey> Do we know what we've got this year yet?
23:04 <marcheu> nope, we had 4/5/2/4
23:04 <marcheu> this year we're at 3
23:04 <+Bart_Massey> OK, you knew the answer :-)
23:04 <dberkholz> hi there
23:04 <marcheu> hopefully we get a 4th in the process
23:05 <alanc> did anyone get the mail I sent yesterday to board & members with the pointer to the summary of last meeting and reminder for today?
23:05 <dberkholz> gentoo may or may not have some extra slottage it could slide toward xorg
23:05 <+mherrb> alanc: no I didn't get it afaict.
23:05 <dberkholz> let me know how many projects xorg can't live without
23:05 <dberkholz> alanc: no, i didn't get it. i just saw you link it on irc
23:05 <alanc> I seem to be unable to send to those lists or receive mail from it since my address changed - haven't been getting notices that they'
re stuck in the moderation queues either
23:06 <agd5f> alanc: I don't recall seeing it
23:06 <alanc> guess I'll poke the sitewranglers later
23:07 <alanc> I didn't make any progress on my action of talking to the lawyers (or talking to Bart to get their contact info) - I don't see any ot
her outstanding actions from the last meeting
23:08 <agd5f> well, we can always do evoc for good applicants beyond 3
23:08 <marcheu> agd5f: yup, but I'll ask for additional slots first. you can ask to be put on the waiting line so I'll do that
23:09 <agd5f> I'd like to finalize who we want as our new tech and admin contacts for x.org
23:09 <agd5f> domain stuff
23:10 <+Bart_Massey> Sorry, AFK.  Yes, EVoC is a fine backup.  PSU may have slots for you too; it looks like we may get more than we really need if
 all goes well.
23:10 <marcheu> Bart_Massey: you cannot transfer from an org to another directly though
23:10 <+Bart_Massey> agd5f I *think* we want SFLC for admin.  Technical matters less, because the admin can change it, I think.
23:11 <dberkholz> marcheu: in my experience, you can tell google where you want extra slots to go and they will respect it
23:11 <+Bart_Massey> marcheu: No, but if we ask nicely in the past they've always said yes. :-)
23:11 <agd5f> Matt suggested SFLC for the admin contact.  is there a person we want or a generic address?  as for the tech contact, is sitewrangers
@freedesktop.org appropriate or do we want a specific person?
23:11 <+Bart_Massey> agd5f: Generic would be best.  I think they'll want a tech contact physical address, so I don't know how to do sitewranglers.
23:12 <agd5f> Bart_Massey: maybe the hosting address?
23:12 <alanc> do we still have a PO Box in portland?
23:12 <+Bart_Massey> agd5f: If they will surely forward our physical mail.
23:12 <+Bart_Massey> alanc: I don't think so.  keithp?
23:13 <alanc> hrm, http://www.x.org/wiki/XorgFoundation lists one as our contact address
23:13  * Bart_Massey s/surely/reliably/ :-(
23:13 <+mherrb> afaict the dns stuff is hosted on expo. someone with root on expo should be tech c.
23:13 <+keithp> alanc: our PO box expired; we hadn't received any mail there for over a year
23:13 <+keithp> so I didn't renew it. I didn't realize it was listed as a contact address
23:13 <+Bart_Massey> mherrb: For sure.  It would be nice to have a role rather than an individual, though.
23:13 <alanc> I think the root on expo has a high overlap with sitewranglers
23:13 <+Bart_Massey> keithp: I don't think it is right now.
23:14 <alanc> we can remove it from the wiki page easily
23:14 <+keithp> alanc: probably a good idea
23:14 <alanc> Done
23:14 <+Bart_Massey> I forget why we don't just hire a mail-forwarding agent?
23:14 <alanc> http://www.x.org/wiki/XorgFoundation?action=diff
23:15 <+Bart_Massey> It seems ideal for us...
23:16 <dberkholz> there's a startup headquartered in portland that will open and scan all your mail
23:16 <dberkholz> or forward it, if desired
23:17 <+Bart_Massey> Yes, Earth-Class Mail
23:17 <+Bart_Massey> I know the owner pretty well...
23:17 <+Bart_Massey> Their plan is to use robots, but I think right now they just hire people. :-)
23:18 <+Bart_Massey> But I was thinking just a Mailboxes-etc type place that would give us a PO Box and throw everything in a big mailing envelope
once in a while and mail it to Alan's house.
23:20 <alanc> if we actually got mail, that would seem reasonable, and better able to cope with the secretary changing every year or two
23:21 <agd5f> seems like a good plan
23:21 <alanc> so going back to the original question: did we decide on who the domain contacts should be?  sitewranglers@fd.o for admin?
23:21 <+Bart_Massey> No, sflc for admin
23:21 <+Bart_Massey> sitewranglers for tech
23:22 <+mherrb> if you look at fd.o they have generic fd.o LLC for both admin and thec
23:22 <+Bart_Massey> Get a PO box to give as a physical addr f
23:22 <+Bart_Massey> or sitewranglers
23:22 <agd5f> so for sflc, what email should we use?
23:22 <alanc> right, I was thinking sysadmin, meant tech
23:22 <+Bart_Massey> agd5f: Their office, presumably
23:22 <+Bart_Massey> Ask them.
23:23 <alanc> freedesktop.org whois gives a paper mail address for them of 7615 SW 59th Ave, Portland, OR - wonder who that actually is
23:23 <+Bart_Massey> :-)
23:23 <agd5f> ok. will do.  I just wanted to make sure there wasn't someone there that we are currently working with that we wanted to use
23:23 <+Bart_Massey> Talk to Karen Sandler
23:24 <+Bart_Massey> She's our contact person at SFLC.
23:24 <+Bart_Massey> She will tell you what to do, I think.
23:24 <agd5f> Bart_Massey: Can you email me her contact info?
23:24 <+Bart_Massey> Working.
23:25 <+Bart_Massey> agd5f: Now I just need *your* contact info and I'm set. :-)
23:25 <+Bart_Massey> Just a sec.
23:27 <+Bart_Massey> OK.  Anything else today?
23:27 <alanc> The one piece of news I have to report is that I've been informed Hideki Hiura passed away last week.   Most of you probably don't re
member him, but he was one of the original Xlib Input Method framework authors for X11R6, and one of the founders/chairs of openi18n.org/li18nux.or
g with the FSG.
23:28 <+Bart_Massey> How sad.  Can you send condolences someplace appropriate on behalf of the Board?
23:28 <alanc> I have e-mail for his wife I can send a note to
23:29 <+emmes> Yes, I think that would be appropriate.
23:29 <+mherrb> sure.
23:29 <agd5f> yes
23:29 <dberkholz> it would be really nice to send a physical card
23:29 <+Bart_Massey> Thanks!
23:30 <+Bart_Massey> Move to adjourn
23:30 <agd5f> seconded
23:30 <+keithp> ta ta
23:30 <+Bart_Massey> alanc: ?
23:31 <alanc> sounds like we're adjourned to me
23:31 <+Bart_Massey> Thanks all!
23:31 <+emmes> nothing from my side
23:31 <alanc> I've got that all the members except anholt were in attendance
"""]]