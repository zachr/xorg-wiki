[[!format txt """
[22:49:06] [connected at Thu Sep 28 22:49:06 2017]
[22:49:17] [I have joined #xf-bod]
[22:50:17] <agd5f> howdy
[22:54:38] <tlwoerner> robclark might be "busy": https://plus.google.com/u/0/photos/photo/107659861834508363082/6470628089429812482?icm=false&iso=true
[22:55:18] <robclark> heheh, yeah, you are competing against supertuxkart here :-P
[23:00:35] <danvet> hi all!
[23:00:52] <danvet> Agenda: XDC18, gsoc, travel payments, evoc, yearly report
[23:03:46] <danvet> egbert, keithp agd5f bryce_ marcheu robclark Riastradh who's all around?
[23:03:53] <bryce_> o/
[23:04:01] <danvet> well seen you and agd5f and robclark ....
[23:04:15] <robclark> o/
[23:04:19] <agd5f> hey
[23:04:51] <Riastradh> Hi!
[23:05:12] <danvet> I think mupuf is on an airplane right now
[23:05:31] <danvet> did egbert and keithp say anything that they're busy?
[23:05:47] <danvet> anything to add to the agenda?
[23:06:10] <robclark> nothing here
[23:06:17] <agd5f> nope
[23:06:58] <danvet> ok, gsoc is postponed, I think finance is all handled by spi and mupuf not around
[23:07:00] <robclark> also, fwiw, I don't really have any evoc updates (or anything else really, been pre-occupied w/ conf's recently ;-))
[23:07:00] <danvet> xdc18
[23:07:15] <danvet> robclark, nw, I'll postpone that too
[23:07:20] * danvet likes quick topics
[23:07:31] * robclark happy w/ reply on xdc18 topic.. not sure if we really have enough present to vote?
[23:07:38] <danvet> have we retired our budget concerns with samuel's answer fully? anything else?
[23:07:52] <danvet> 5 present
[23:08:04] <danvet> we need 4 votes
[23:08:15] <robclark> ahh
[23:08:21] <Riastradh> I'm postponing CoC because I suddenly have some new experience to draw from in the past couple weeks and need to digest it without revealing details.
[23:08:28] <Riastradh> (Fun!)
[23:08:37] <robclark> umm, hurray?
[23:08:46] <danvet> oh ...
[23:08:55] <bryce_> Riastradh, sympathies I'm guessing
[23:09:02] <robclark> yeah
[23:09:03] <danvet> Riastradh, on that, pls also check out the mail on fd.o from daniel stone
[23:09:15] <Riastradh> danvet: On fd.o dissolution, you mean?
[23:09:19] <danvet> yeah
[23:09:27] <Riastradh> Saw that fly by -- still catching up on my email while I was at EuroBSDcon.
[23:09:37] <danvet> we discussed it a bit at the xdc mtg, but probably best to keep it on board@ for now
[23:09:43] <danvet> yeah no worries
[23:10:05] <Riastradh> OK.
[23:10:43] <danvet> if you can reply there would be good, even if it's just a "super busy right now"
[23:10:51] <danvet> so xdc18
[23:11:54] <danvet> I propose we just vote on coruña, at least I don't remember any other concerns from last week ...
[23:12:28] <danvet> or toulouse
[23:12:45] <bryce_> +1 for Coruña
[23:12:58] <agd5f> +1 for Coruña
[23:13:01] <robclark> +1
[23:13:23] <robclark> for coruna
[23:13:27] <Riastradh> XDC has already been to Toulouse, hasn't it?  In 2014 or so?
[23:13:37] <robclark> earlier than that, but yeah
[23:13:52] <danvet> 7 years ago I think
[23:14:03] <danvet> last few in europe were helsinki, bordeaux, toulouse
[23:14:05] <danvet> reverse order
[23:14:13] <Riastradh> Ah, Bordeaux, that's what I was thinking of.
[23:14:26] <danvet> +1 on coruña too from me fwiw
[23:14:46] <Riastradh> I abstain -- not enough information from me to decide.
[23:15:23] <robclark> I think before bordeaux was germany.. nuremburg (sp?)
[23:16:32] <bryce_> Riastradh, what info would help?
[23:16:43] <danvet> robclark, oh right
[23:16:59] <danvet> toulouse was my first xdc, forgot how long ago that was
[23:17:04] <bryce_> Riastradh, or just that you want time to digest the provided info?
[23:17:16] <Riastradh> Sounds like you have 4 for Coruña already, so no need for my vote?  If you do need my vote, I will take some time to review the email thread.
[23:17:39] <danvet> 4 is enough strictly speaking, since secretary can untie
[23:17:41] <danvet> and I voted for it
[23:20:01] <danvet> I'll send out mails to both samuel and maxime
[23:20:34] <danvet> bryce_, on travel payments, do you need anything?
[23:20:40] <danvet> do we need to vote on jake's mail?
[23:20:46] * danvet not sure what we granted ...
[23:20:55] <Riastradh> Cursory skim suggests Coruña is much more enthusiastic and better-prepared than Toulouse, so +1 Coruña.
[23:21:13] <danvet> yeah it's probably the most impressive proposal we've ever gotten
[23:21:29] <bryce_> mainly I need to remember what amounts we agreed on for the sponsorees
[23:21:53] <bryce_> apart from that I think it's straightforward, I plan to send out emails post-meeting
[23:22:20] <bryce_> I don't think a vote on jake's mail is necessary if it fits within what we agreed to sponsor.
[23:22:32] <robclark> I guess there was question of splitting Jake's airfare appropriately, but I think as long as it fits in what we agree'd I'm happy
[23:22:33] <robclark> yeah
[23:22:36] <Riastradh> Are dates set yet?  (...Can they be at least a week apart from EuroBSDcon?)
[23:22:48] <robclark> dates not set yet
[23:22:58] <robclark> usually try to get them set by early next year or so
[23:23:13] <robclark> and piss off as few people as possible in the process ;-)
[23:23:23] <danvet> bryce_, yeah if it's in line with what we granted I'd just pay the full amount too
[23:23:43] <bryce_> did LWN run adequate summaries of XDC?  I wasn't keep an eye on them.
[23:23:49] <danvet> robclark, yeah ian's a bit over the top imo, it's not like we didn't try to make this as good as possible :-/
[23:24:02] <danvet> bryce_, one is out already
[23:24:04] <robclark> bryce_, they have just started to trickle out on lwn
[23:24:09] <danvet> will probably trickle more
[23:25:02] <bryce_> danvet, so that's it for that.  I'll try to dig up the amounts and chime in later in the meeting with what I find.
[23:25:28] <danvet> sounds good
[23:25:46] <danvet> bryce_, maybe also ping labbot
[23:26:46] <bryce_> Formatting is a bit messed up but here's where we voted.  Looks like $1300 for Jake, $400 for Laura.  https://www.x.org/wiki/BoardOfDirectors/IrcLogs/2017/07-20/
[23:27:21] <tlwoerner> https://lwn.net/Archives/ConferenceByYear/#2017-X.Org_Developers_Conference
[23:27:47] <bryce_> looks like Jake's issue is not that he exceeded the amount but that some of the travel was for another conference
[23:27:59] <danvet> he was at plumbers too
[23:28:03] <robclark> so I think for jake we need to see what hotel + car amounted too to see how much is left for travel
[23:28:18] <bryce_> but that's airfare only, so tbd what hotel was.
[23:28:28] <danvet> can you ask him?
[23:28:36] <bryce_> Anyway, I'll follow up with him and get final numbers.  Seems likely his expenses fit within it.
[23:28:45] <danvet> sounds good
[23:28:50] <bryce_> and will follow up with Laura.
[23:30:36] <robclark> fwiw laura is at linaro this week (so replies may delayed)
[23:30:54] <danvet> final topic is yearly report of the secretary
[23:31:02] <danvet> delivered at https://youtu.be/R2XHZd4uXRI?list=PLt6hUXQez2dcnyMdOp8E7cJCBSTZ6KMby&t=9114
[23:31:53] <danvet> since no one complained I assume it's accepted?
[23:32:15] <robclark> sgtm
[23:32:34] <agd5f> yeah
[23:33:36] <danvet> everyone watching Riastradh watch it ...
[23:35:13] <bryce_> danvet, +1
[23:35:30] <bryce_> danvet, but moar applause cues next year plz
[23:36:00] <danvet> yeah I screwed that up a bit this year
[23:36:19] <agd5f> +1
[23:36:41] <keithp> was down at osuosl and that meeting ran long
[23:38:54] <danvet> I think that's all ... anything else?
[23:40:42] <keithp> I sent along jake's pending email to the board
[23:41:00] <Riastradh> Oh, hi, uhhh, if you're waiting for approval from me on the yearly report, +1!
[23:41:17] <danvet> Riastradh, :-)
[23:41:41] <danvet> I think that's all, thx for hanging around
[23:41:54] <robclark> cya..
[23:41:57] * robclark back to stk
[23:41:57] <danvet> and I'll push the final version of the slides of the report into the bod repo
[23:42:41] <bryce_> thanks, cya!@
"""]]
[23:42:48] [disconnected at Thu Sep 28 23:42:48 2017]
