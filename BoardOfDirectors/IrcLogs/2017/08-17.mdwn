[[!format txt """
[14:03:34] [connected at Thu Aug 17 14:03:34 2017]
[14:03:45] [I have joined #xf-bod]
[14:05:58] <bryce__> == Xorg Board Meeting ==
[14:06:59] <bryce__> agenda?  Last meeting was gsoc, xdc sponsorship, xdc18, khronos, coc, evoc, xdc 17 talks
[14:07:08] <bryce__> anything to be added or removed from that?
[14:07:40] <mupuf> xdc talks are still necessary, because we got late submissions
[14:08:39] <bryce__> ok, guess we can use the list as is and add/remove as we go
[14:08:42] <bryce__> == gsoc ==
[14:08:59] <mupuf> let's keep it short for GSoC, nothing to declare!
[14:09:04] <bryce__> mupuf, thanks :-)
[14:09:12] <bryce__> == XDC Sponsorship ==
[14:11:53] <bryce__> maybe this is a topic for danvet.  keithp, unless you have anything to mention?
[14:13:22] <bryce__> I saw something to the effect that Google wouldn't allow non-Google advertising on campus, which hampers sponsorship of the event itself
[14:13:45] <bryce__> but the web sponsorship is an option if anyone wants to pursue that.
[14:13:49] <bryce__> ok, moving on.
[14:13:57] <bryce__> == XDC 18 ==
[14:15:44] <bryce__> daniel posted a proposal from samual, anyone had a chance to review?  Any comments?
[14:16:35] <mupuf> at least one other proposal is on the way
[14:17:03] <bryce__> mupuf, in what country, out of curiosity?
[14:17:55] <mupuf> france
[14:19:42] <bryce__> == Kronos ==
[14:20:30] <bryce__> mupuf, I noticed there has been some progress recently, where do things stand now?  Is stuff still moving forward or has it gotten stuck?
[14:21:27] <mupuf> yes! it is moving!
[14:21:30] <mupuf> apparently
[14:21:49] <mupuf> I would need to ping them to know what is the actual state, but I assume they consider it acceptable
[14:24:40] <bryce__> mupuf, think it's likely this'll get wrapped up and final by XDC?  Would be a nice success to report there.
[14:26:34] <mupuf> I definitely hope so!
[14:26:53] <mupuf> but they will need to sign by the end of the month, otherwise, it will be tough to figure the full process
[14:27:11] <mupuf> Nouveau is working towards 4.4 comformance as we speak
[14:30:01] <bryce__> mupuf, ok thanks.  hoping your continued pingings get met with strong pongings :-)
[14:30:10] <bryce__> == CoC ==
[14:30:10] <mupuf> ha ha 
[14:30:38] <bryce__> Riastradh, sounded like this got mostly resolved, just waiting on FDO...?
[14:31:12] <Riastradh> There remains a question of what we want to do formally.
[14:31:14] <Riastradh> (a) Nothing.
[14:31:23] <Riastradh> (b) Vote as board to approve CoC as X.org policy.
[14:31:35] <Riastradh> (c) Ask membership to vote to approve CoC as part of membership agreement.
[14:31:49] <Riastradh> See message <20170803220956.3C6A760A7F@jupiter.mumble.net> for a little more detail.
[14:33:09] <Riastradh> Aside from that, we should have: (1) A reporting channel documented on <https://www.x.org/> and (2) a reporting channel documented for each project's web page.
[14:33:54] <Riastradh> I can draft a page for <https://www.x.org/> to point to if everyone who wants to be listed as a contact can let me know name and preferably non-x.org/fd.o email address.
[14:36:03] <Riastradh> I can also go around to project admins to ask them to identify reporting channels, but I feel it might be more effective if someone more involved in the community did that.
[14:37:26] <robclark> Riastradh, fyi.. robdclark@gmail.com ..
[14:37:51] <robclark> (or if you want I guess you can just let everyone add themselves to the wiki page)
[14:37:53] <bryce__> Riastradh, from what I read of last meeting, there seems to be a consensus against #1.  So the choice is really between #2 and #3
[14:38:13] <Riastradh> robclark: OK, thanks.
[14:39:11] <bryce__> I also like keithp's suggestion that we could do #2, followed later by #3
[14:39:25] <Riastradh> bryce__: I'm not going to press one way or another, for lack of familiarity with the social implications in the X.org membership.  Obviously membership agreement would be grant greater legitimacy, but would be a lot more work.
[14:39:26] <bryce__> that seems like a very practical path
[14:39:30] <Riastradh> Sounds reasonable to me.
[14:39:52] <bryce__> Riastradh, are you attending XDC this year?
[14:40:12] <bryce__> perhaps we could consider doing the board vote at XDC to give it added weight?
[14:40:13] <Riastradh> bryce__: 'Fraid not -- I'll be in Paris at EuroBSDcon.
[14:40:36] <mupuf> let's do the vote the week before then
[14:41:06] <Riastradh> If there's a reason to do it at a time other than now, sure, whatever time you folks think is good!
[14:41:09] <bryce__> that'd be.... the 14th?
[14:41:39] <bryce__> Riastradh, on this I'd like to wait for danvet to run the vote tbh
[14:41:46] <Riastradh> Sounds good.
[14:43:38] <bryce__> Riastradh, do we have a clear statement written down that can be voted on?  If not, that might be a good goal for the near term?
[14:43:53] <Riastradh> Contributor Covenant 1.4?  Or do you mean a statement of something else?
[14:44:12] <Riastradh> (I.e., what the fd.o CoC is.)
[14:44:55] <Riastradh> Do you mean an addendum about how each X.org project should have its own reporting channel, with the following members (...) of X.Org Foundation board as fallback/appeal?
[14:45:03] <bryce__> I mean not just the CoC itself but the process we'll be following for handling infractions - i.e. all the stuff we've been hashing out in past months
[14:45:27] <bryce__> I think we have agreements on all the details we just need a doc to point to as to what we're going to be doing
[14:46:01] <Riastradh> OK, I'll dredge the logs and see what needs addending for next time.
[14:46:10] <bryce__> we've already done a vote on accepting the CoC, so this vote will be about the enforcement process itself
[14:46:30] <bryce__> and yeah, any details we'll be expecting of X.org projects for reporting and so on
[14:47:16] <Riastradh> I don't really want to spend too much effort on a detailed enforcement process because (a) the greater the detail, the more fodder for process trolls, and (b) really I think the reporting channels are more important; any enforcement is always painful no matter what the process is.
[14:47:23] <bryce__> esp. if we're going to be asking X.org projects to follow a process, XDC would be a good place to go over it and explain it
[14:47:27] <Riastradh> But I'll write a summary.
[14:47:48] <bryce__> thanks
[14:48:01] <bryce__> next meeting might be a good point to do a quick review on that
[14:48:03] <bryce__> ok, moving on
[14:48:22] <bryce__> == EVoC ==
[14:48:34] * robclark has short update..
[14:48:44] <bryce__> robclark, thanks.
[14:49:14] <robclark> after sorting out some git-send-email difficulties, Quentin sent a patchset to mesa-dev.. yesterday I think
[14:49:31] <robclark> nha had a few review comments
[14:49:46] <robclark> so hopefully soon we see v2
[14:50:17] <robclark> (that is all)
[14:51:08] <bryce__> ok great, thanks
[14:51:15] <bryce__> == XDC 17 Talks ==
[14:51:22] <mupuf> sorry
[14:51:29] <mupuf> so, we got some late submissions
[14:51:38] <mupuf> wr have 1-2 slots available
[14:51:53] <mupuf> but we would like to keep some slots for GSoC talks
[14:52:19] <mupuf> so, we are holding on to it
[14:52:33] <mupuf> I guess I need to start asking the students whether they can make it to GSoC or not
[14:54:53] <bryce__> yeah
[14:55:38] <bryce__> mupuf, do you know if they have sponsorship for the travel?  Am I correct to recall google was making some funding provisions?
[14:55:51] <mupuf> this is for another conference
[14:55:59] <mupuf> not for XDC
[14:55:59] <bryce__> ok
[14:56:10] <mupuf> we generally pay for it... if they agree to talk about their work
[14:56:22] <Riastradh> Doesn't Google provide a travel stipend to send GSoC students to conferences, if they ask for it?
[14:57:37] <Riastradh> (Memory is fuzzy; it may just be that Google suggests the lump sum might be used for that or for GSoC mentor summit or whatever else the org wants to use it for.)
[14:58:23] <mupuf> yes, it si for the mentor summit
[15:01:36] <Riastradh> Wait, students at the mentor summit?  I thought the mentor summit was mentors-only.
[15:01:59] <Riastradh> ...er, oh, by `it' you meant `the lump sum'.
[15:02:29] <bryce__> alright, we're light on time, and unfortunately I have another appointment back to back
[15:02:37] <bryce__> == Other Business ==
[15:02:45] <bryce__> any opens before we wrap things up this week?
[15:03:33] <Riastradh> I'm out of items.
[15:04:24] <bryce__> ok, great thanks all.  I'll try to get the meeting minutes up directly...
[15:04:28] <bryce__> == EOM ==
[15:04:47] <robclark> cya
[15:04:59] <agd5f> thanks
"""]]
[15:28:21] [disconnected at Thu Aug 17 15:28:21 2017]
