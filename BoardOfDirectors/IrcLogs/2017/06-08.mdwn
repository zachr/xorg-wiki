[[!format txt """
[13:55:27] <tlwoerner>	meeting in 5 minutes?
[13:56:13] <robclark>	presumably
[13:56:40] <robclark>	looks like since danvet wasn't there last time, we missed secretary automatic /topic update..
[14:00:14] <agd5f>	hi
[14:00:38] <egbert>	hi guys!
[14:00:56] <robclark>	o/
[14:00:58] <Riastradh>	Hi!
[14:01:19] <keithp>	hello!
[14:02:27] <bryce__>	heya
[14:02:43] <mupuf>	Hey hey!
[14:05:08] <tlwoerner>	who is chairing?
[14:05:19] *	Riastradh puts finger on nose
[14:05:27] <mupuf>	I can't, i'm on the phone right now
[14:06:07] <Riastradh>	I have to leave a few minutes early for my mother's birthday dinner, so if we have a short agenda, I guess I can take my finger off my nose.
[14:06:24] <tlwoerner>	can a non-elected person chair? ;-)
[14:06:46] <robclark>	not quite sure what else is on agenda.. but one EVoC topic..
[14:06:57] <robclark>	(which we can probably get thru quickly)
[14:07:15] <tlwoerner>	anyone submitted papers for xdc?
[14:07:25] <bryce__>	robclark, go ahead
[14:07:45] <keithp>	we got one submission so far, at least on the public list
[14:07:59] <robclark>	tlwoerner, starting to get submissions, but board doesn't do the approval/scheduling (and that won't happen until later anyways)
[14:08:17] <robclark>	on EVoC, re: the driconf replacement project..
[14:08:57] <robclark>	for "initial patch task", the student pushed an initial skeleton to github.. I guess that is a reasonable analog to "initial patch"..
[14:09:08] <robclark>	since it is a new project, no mailing list, etc..
[14:09:36] <mupuf>	Not really no
[14:09:55] <robclark>	mupuf, better suggestions?
[14:09:56] <keithp>	dri-devel seems like a reasonable list?
[14:09:59] <mupuf>	The goal is to show they understand what it means to collaborate
[14:10:20] <robclark>	yeah, but there isn't anything that exists to patch
[14:10:23] <mupuf>	They need to get a patch upstream, in a project not of their own
[14:10:27] <robclark>	probably mesa-dev is a good list
[14:10:38] <robclark>	(in long term)
[14:11:04] <mupuf>	There are patches to driconf that can already land. Even documentqtion
[14:11:18] <mupuf>	That would be useful
[14:11:27] <robclark>	well, I didn't want to put moving old driconf to fd.o in the critical path
[14:11:37] <keithp>	definitely not
[14:11:37] <mupuf>	Driconf is not the most obvious design :s
[14:11:48] <robclark>	is there anyone who could actually *push* an a hypothetical old-driconf patch?
[14:11:49] <mupuf>	Agreed
[14:12:09] <mupuf>	I meant a patch in mesa's parser
[14:12:21] <robclark>	hmm, ok..
[14:12:47] <mupuf>	Just some doc, to show he understands and he can get a patch upstream
[14:13:06] <robclark>	hmm, ok.. that sounds reasonable..
[14:13:24] <tlwoerner>	GSoC coding has officially started
[14:14:02] <robclark>	note that EVoC schedule != GSoC schedule..
[14:14:30] <tlwoerner>	yes, but coincidentally they're both kinda running at the same time (?)
[14:14:44] <robclark>	(although student is on US summer vacation schedule so in this case his schedule should be close)
[14:15:15] <mupuf>	Evoc is any time a student wants, as long as they have 3 consecutive months available
[14:15:28] <bryce__>	robclark, anything else on evoc?  if not we can move on to gsoc if there's stuff to discuss there.
[14:15:36] <robclark>	EVoC was intended to have more flexibility for students on different summer vacation schedules, anyways that is getting a bit off topic..
[14:16:14] <robclark>	I guess I should mention that nha is back from vacation now... there was a couple weeks where I hadn't heard anything from him which was slowing down the driconf proposal..
[14:16:27] <robclark>	so he has picked up design discussion again w/ student
[14:16:40] <robclark>	anyways, that is about all
[14:16:52] <mupuf>	good
[14:16:59] <mupuf>	I can talk about GSoC
[14:17:00] <tlwoerner>	robclark: is the discussion happening in the open?
[14:17:02] <mupuf>	it will be short
[14:17:31] <bryce__>	mupuf, yes go ahead
[14:17:37] <robclark>	tlwoerner, not necessarily.. some of it on board@ list
[14:17:57] <bryce__>	[other agenda items... website sponsorship... khronos... xdc18]
[14:18:07] <mupuf>	well, I'll keep it short
[14:18:22] <mupuf>	I screwed up and still have no sent an email to students, reminding them to blog
[14:18:28] <mupuf>	so, I will do it before going to bed
[14:18:47] <tlwoerner>	mupuf: any idea if coding has started?
[14:18:56] <Riastradh>	One other agenda item: CoC recommendations, which I haven't written up yet (sorry, crunch at $DAYJOB and local legislature hearings) but am basically ready to now after having done some more background research.
[14:18:57] <mupuf>	it started last tuesday
[14:19:11] <mupuf>	I have one email I started this day... and it still is open on my desktop machine, half-written
[14:19:12] <mupuf>	...
[14:20:02] <bryce__>	mupuf, ok sounds good.
[14:20:51] <bryce__>	Riastradh, since you're time limited, go ahead with coc.  Guess we should see your recommendations before we have anything to discuss?
[14:21:08] <Riastradh>	bryce__: Yeah, nothing really to discuss yet, I'll just write it up with any luck by this weekend.
[14:21:16] <Riastradh>	(nothing beyond what we've already discussed)
[14:21:31] <bryce__>	Riastradh, ok, we'll add it to the agenda for next meeting, but hopefully it'll get some mulling on the mailing list
[14:21:38] <Riastradh>	Yep.
[14:21:42] <bryce__>	right, on to website sponsorship.
[14:21:58] <agd5f>	bryce__, your proposal looked good to me
[14:21:59] <bryce__>	I posted a draft proposal for sponsorship levels to the list.  Hopefully everyone's had time to look it over
[14:22:25] <keithp>	yup, seemed reasonable to me too
[14:22:37] <keithp>	bryce__: did you crib a pile of that from elsewhere?
[14:22:50] <bryce__>	if there are any comments or concerns I'd like to take time to incorporate them; else next step would be a vote so it can proceed with being implemented
[14:23:09] <bryce__>	keithp, yep, pilfered it largely from a proposal I did for inkscape
[14:23:17] <keithp>	how did it work for that?
[14:23:44] <robclark>	btw, the initial request that triggered this was about sponsorship for XDC?  Not sure if we want to tackle that separately or as part of general "xorg sponsorship thing"..
[14:24:04] <bryce__>	good so far - this was actually a second iteration after our initial policy was a bit too nebulously defined for sponsors
[14:24:41] <bryce__>	robclark, the initial request was a web sponsorship from some weeks back.  The XDC sponsorship has come in subsequently and yeah should probably be treated separate
[14:25:01] <bryce__>	or at least, there's some further details that'd need defined.
[14:25:09] <robclark>	well, true.. but not sure if the first request was legit..
[14:25:13] <Riastradh>	bryce__: Proposal looks reasonable to me.  Separate conference sponsorship policy also sounds reasonable.
[14:25:28] <bryce__>	I also don't know if we should have two separate policies or one that covers the various angles; either way the XDC sponsorship can be tackled independently
[14:25:46] <robclark>	anyways, it would be nice to have coffee/pastery type sponsorship for XDC so it isn't *quite* so low budget ;-)
[14:26:10] <bryce__>	robclark, yep, I followed up and asked for examples of where they've sponsored other projects and he sent me a couple links so it does look legit
[14:26:28] <robclark>	the first one, oh, ok..
[14:26:52] <bryce__>	the one piece of all this that I don't have nailed down is verification that the donor actually sent in the money
[14:26:53] <robclark>	(collabora is legit.. they employ a bunch of people who work on xorg/mesa/etc ;-))
[14:26:54] <mupuf>	I all agree. However, if they want logos outside of slides, we'll need to ask them to print us stuff
[14:27:08] <mupuf>	and send it to us, or bring someone with it
[14:27:12] <mupuf>	what do you think?
[14:27:14] <tlwoerner>	robclark: gstreamer
[14:27:18] <mupuf>	Just thinking about the logistic here :s
[14:27:19] <bryce__>	with Inkscape we have a ledger dump from SFC where we can see the paypal entry and verify amounts and such
[14:27:24] <Riastradh>	In-kind donations presumably require a little more work to arrange; it would be a bummer if XDC served only seven thousand plain bagels to eat.
[14:27:34] <robclark>	mupuf, yeah, that is why I wondered (on list) about encouraging in-kind donations..
[14:27:35] <bryce__>	we don't have such a thing with SPI - they give us periodic reports but last one I got from them was months ago
[14:27:56] <keithp>	Riastradh: as long as they come with sufficient lox and cream cheese, I'm pretty sure we'd be fine
[14:27:57] <robclark>	bryce__, hmm, that sounds inconvenient..
[14:27:58] <bryce__>	but that's a problem separate from the policy definition, and presumably can be improved on SPI's end if needed
[14:28:09] <robclark>	right
[14:28:39] <Riastradh>	keithp: I mean, if you want everyone asleep from the mountain of carbs, then I guess you don't need coffee...
[14:28:55] <robclark>	anyways, the sponsorship policy looked reasonable to me.. not that I have tons of experience with that sorta thing.. but it looked sane
[14:29:10] <mupuf>	same here
[14:29:13] <keithp>	Riastradh: maybe arrange a two hour nap break in the afternoon
[14:29:16] <bryce__>	robclark, I figure we can iterate as we go (that's what we've done with Inkscape)
[14:29:49] <robclark>	the conf sponsorship.. I think we need to think about the logistics if it is just "here is a pile of money" instead of "here is a pile of bagels"..
[14:30:03] <robclark>	since we'd be basically forcing the orgnaizer to go spend the money for us
[14:30:41] <bryce__>	robclark, yep.  Some of that wrt in-kind donations is going to be hard to nail down in a policy and might need to be left to discretion of conference organizers kind of case-by-case
[14:30:59] <Riastradh>	Does past XDC experience inform adequately here?  Would you like to hear experiences from people who have organized other conferences, e.g. the BSDcons?
[14:31:06] <agd5f>	some venues may not allow random stuff from other vendors
[14:31:07] <bryce__>	but the money side certainly can be defined
[14:31:45] <Riastradh>	(I have some contacts with EuroBSDcon organizers, and maybe BSDCan and AsiaBSDcon.)
[14:31:53] <robclark>	Riastradh, past XDC has tended to be relatively low budget, ie. whoever is local and organizing picks up a couple bags of bagels..
[14:32:17] <robclark>	I guess if you have some experience from bsdcon then that might be helpful
[14:32:28] <Riastradh>	How many attendees usually?  Count on one hand (in unary)?  Dozens?  Hundreds?
[14:32:36] <Riastradh>	(BSDcons are usually a couple hundred.)
[14:32:37] <robclark>	60-100
[14:32:46] <Riastradh>	OK.
[14:32:53] <bryce__>	robclark, I notice that historically in the past there were a few high funded XDCs.  But sounds like they were single sponsor situations
[14:33:00] <robclark>	and like agd5f says, I think it takes coordination w/ whoever is hosting and whoever wants to sponsor due to venue related restrictions, etc..
[14:33:12] <robclark>	yeah
[14:33:29] <keithp>	Riastradh: so, sponsoring an evening event requires a lot of planning. Sponsoring coffee/tea breaks is a lot easier as it's just 'here's how much the catering costs'
[14:34:07] <keithp>	I'd say we can leave a lot of that up to the XDC org people; offer the notion of sponsoring and have them pick some things which are optional but cost money
[14:34:17] <agd5f>	maybe we talk to the host and get quotes for catering, etc. and then we can offer sponsor slots based on that
[14:34:29] <keithp>	yeah
[14:34:30] <agd5f>	it could be part of the proposal for the host
[14:34:32] <Riastradh>	Right.  EuroBSDcon always has an evening dinner -- which must be a nightmare to organize, getting a hundred scruffy BSD hackers organized to take public transit to a restaurant.
[14:34:32] <bryce__>	seems sensible, yeah
[14:34:34] <mupuf>	agd5f: that sounds like a good idea
[14:34:41] <keithp>	hence my suggestion to let the XDC org "team" figure some of it out
[14:34:58] <mupuf>	agd5f: and the xorg foundation could sponsor something, based on our budget and what sponsor paid for
[14:35:19] <bryce__>	ok, so then limiting this website sponsorship proposal to just general sponsorship, not XDC (for now anyway), are there any other things needing discussion, or does it look ripe for being voted on today?
[14:35:22] <keithp>	Riastradh: XDC often has a social event the day after the conf; we've been low-budget on that (which is awesome), but zero-budget with sponsorship would also be an option
[14:35:23] <mupuf>	I would still like sponsors to give some money to us, as a way to keep our budget stable and not going down more
[14:35:31] <keithp>	mupuf: agreed
[14:35:43] <agd5f>	right.  if the host is covering everything, maybe the only slots we could offer are logos on signs
[14:36:04] <mupuf>	but sponsoring the event by paying for cattering or a social event: I am also up for it, if a sponsor wants
[14:36:06] <keithp>	agd5f: another large expense item is travel sponsorship
[14:36:39] <mupuf>	yep, fair point, would be good if we just said the budget we spent and what sponsors paid where
[14:36:44] <agd5f>	keithp, yeah, good call
[14:36:45] <mupuf>	this way, it is super fair
[14:36:48] <robclark>	yeah, I supose part of XDC proposal should be (if applicable) a couple sponsor options (ie. "for $XYZ we can get catered coffee break", etc)
[14:37:03] <Riastradh>	For reference, if you're curious, here's the EuroBSDcon sponsorship policy: https://2017.eurobsdcon.org/sponsors/  I don't see any evidence of in-kind donations.
[14:37:38] <keithp>	in-kind would be things like venue, networking. That's what LCA has, for instance
[14:37:57] <keithp>	in-kind pastry would be nearly impossible to manage
[14:38:10] <robclark>	(unless sponsor had someone local)
[14:38:14] <mupuf>	keithp: unless someone from a company actually attends
[14:38:22] <Riastradh>	In Paris this year, I think Gandi might be hosting it.
[14:38:28] <keithp>	much better to have a directed donation ($1000 for pastry from bakery "foo")
[14:39:06] <robclark>	anyways, I guess the xdc organizers have to be in the loop since we need someone local for logistics
[14:40:10] <bryce__>	should I summarize the ^^^ above discussion as a starting point for XDC sponsorship, or would it be better left for the XDC committee to write?
[14:40:57] <bryce__>	i.e. should I take the action item to capture this or does someone else want it?
[14:41:01] <agd5f>	I guess alternatively, we could provide sponsor slots upfront and then based on how many we get, give the host a dollar figure for what xorg would provide them (by way of the sponsors)
[14:41:08] <robclark>	I think we should add something to the XDC hosting guidelines..  I guess that is the action for now..
[14:41:34] <tlwoerner>	bryce__: well, you could leave the xdc sponsorship to the xdc committee, but there's still the item of website sponsorship
[14:41:42] <bryce__>	right
[14:41:47] <mupuf>	agd5f: that is a decent proposal
[14:42:01] <bryce__>	on website sponsorship, does anyone feel there should be more to discuss, or should we do a vote now?
[14:42:24] <agd5f>	and then xorg could keep part of the sponsorship donation for things like covering travel
[14:42:52] <robclark>	I think we could vote..  and discuss the XDC sponsorship more later..
[14:43:15] <Riastradh>	No objection from me to voting now.
[14:43:19] <agd5f>	bryce__, sounds good to me as long as we specify that it's website sponsor ship to avoid confusion with conference sponsorship
[14:43:23] <bryce__>	ok sounds good
[14:43:26] <bryce__>	agd5f, right
[14:43:26] <robclark>	yeah
[14:43:49] <bryce__>	ok, so vote on proposal for general sponsorship for website?
[14:43:51] <bryce__>	+1
[14:43:53] <Riastradh>	+1
[14:43:54] <agd5f>	+1
[14:43:59] <egbert>	+1
[14:44:10] <robclark>	+1
[14:44:13] <tlwoerner>	doesn't someone have to propose something? 
[14:44:15] <keithp>	+1
[14:44:16] <tlwoerner>	"be is resolved that..."
[14:45:37] <bryce__>	tlwoerner, I gather X.org board is a bit less formal on that count.  You can consider the draft proposal I sent to the list as the proposal since there were no suggested changes needing incorporated.
[14:45:59] <mupuf>	+1
[14:46:08] <tlwoerner>	bryce__: ah, ok
[14:46:26] <Riastradh>	`motion to move for cloture on discussion of a resolution to move to resolve that...'
[14:46:29] <bryce__>	ok, vote carries, thanks all
[14:46:32] <bryce__>	Riastradh, there we go :-)
[14:46:45] <Riastradh>	I forgot WHEREAS.
[14:47:21] <bryce__>	ok, unless someone else wants to own the action item for adding sponsorship stuff to the XDC hosting guidelines, I can take that
[14:47:32] <robclark>	I can make a proposal
[14:47:50] <bryce__>	robclark, ah ok cool, will leave that to you then
[14:47:54] <bryce__>	next agenda item...
[14:48:06] <robclark>	btw, on website sponsoring, I guess if we add a web page about sponsorship we should somewhere list purpose (ie "travel sponsorships, funding EVoC, etc"..)
[14:48:32] <tlwoerner>	Riastradh: :-)
[14:49:03] <bryce__>	robclark, right, this proposal is strictly for "general donations" i.e. undesignated funds that X.org board decides how to allocate
[14:49:37] <bryce__>	next on the agenda would be khronos
[14:49:47] <bryce__>	do we have new updates there?
[14:49:48] <agd5f>	bryce__, I think robclark's point is that we should mention what we use the donations for :)
[14:50:05] <bryce__>	agd5f, ahhh yes agreed.  I think we may already but I'll doublecheck.
[14:50:07] <robclark>	right
[14:51:55] <bryce__>	== Khronos ==
[14:53:48] <bryce__>	Last meeting we were awaiting word; guessing we're still in that position?
[14:53:58] <Riastradh>	OK, I have to skedaddle, folks, sorry!
[14:54:00] *	robclark doesn't think there was any update there... or remember seeing anything on board@ since then
[14:54:04] <bryce__>	cya Riastradh 
[14:54:22] <bryce__>	ok, maybe Martin can chime in later
[14:54:27] <mupuf>	well
[14:54:29] <bryce__>	last agenda item xdc18
[14:54:34] <bryce__>	== XDC 18 ==
[14:54:34] <mupuf>	really noithing to say about Khronos
[14:55:23] <robclark>	btw, we need to remember to sort out better timeslot (on list after mtg).. ;-)
[14:56:02] <mupuf>	timeslots for the XDC?
[14:56:07] <mupuf>	what do you mean?
[14:56:11] <bryce__>	board meeting timeslot
[14:56:23] <bryce__>	it came up a few meetings back and daniel took the action item to find a new time for us
[14:57:46] <bryce__>	alright, I guess XDC 18 is another action item we'll need daniel for.
[14:57:52] <bryce__>	== Other Business ==
[14:58:03] <bryce__>	anyone got new agenda items to open?
[14:58:38] *	robclark has changed the topic to: Next Meeting: June 22, 2pm PST https://www.timeanddate.com/worldclock/fixedtime.html?iso=20170525T14&p1=202&ah=1
[14:58:52] <robclark>	nothing else here
[14:58:59] <mupuf>	nothing for me
[14:59:27] <agd5f>	nope
[14:59:52] <bryce__>	alright, if there's nothing else let's go ahead and close
[14:59:56] <bryce__>	== EOM ==
[14:59:58] <bryce__>	thanks all
[15:00:13] <robclark>	cya
[15:01:28] <tlwoerner>	bye
[15:01:41] <egbert>	good night!
"""]]
