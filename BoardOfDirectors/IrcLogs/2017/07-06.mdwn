[[!format txt """
[22:50:12] [connected at Thu Jul  6 22:50:12 2017]
[22:50:23] [I have joined #xf-bod]
[22:50:33] <danvet> mupuf, agd5f bryce_ keithp robclark Riastradh mtg in 10'
[22:50:38] <mupuf> yep!
[22:51:20] <agd5f> yes
[22:51:40] <Riastradh> Hi!
[22:54:06] <keithp> tnx for the heads up
[23:00:10] <robclark> o/
[23:01:36] <danvet> hi all
[23:02:08] <mupuf> hi :)
[23:02:23] <danvet> Agenda: travel requests, evoc, gsoc, xdc sponsorship, xdc18, khronos, coc
[23:03:23] <danvet> bryce_, around too?
[23:03:28] <danvet> anything I should add to the agenda?
[23:05:27] <mupuf> Don't think so
[23:05:28] <danvet> I guess we'll wait with travel until bryce shows up ...
[23:05:59] <danvet> robclark, new from evoc?
[23:07:05] <robclark> I sync'd up w/ nha ~1wk back.. at that point student was working on *something* for mesa patch.. I forgot to check in w/ nha this morning to see if there had been progress..
[23:07:09] <robclark> so that is about it
[23:07:27] <robclark> I asked nha to try to steer student to #dri-devel
[23:07:41] <danvet> hm ...
[23:07:59] <danvet> I guess I'll just ping you next mtg again?
[23:08:05] <robclark> sure
[23:08:18] <danvet> ok, I'll keep it on the agenda
[23:08:30] <mupuf> I guess it is my turn then
[23:08:31] <danvet> mupuf, gsoc news?
[23:08:34] <danvet> :-)
[23:08:46] <mupuf> so, the evaluation period was last week
[23:08:51] <mupuf> there are 2 of them this year
[23:09:03] <danvet> the midterm is new?
[23:09:03] <mupuf> out of 5 students, we still have 4 active
[23:09:15] <mupuf> there are no midterms anymore
[23:09:21] <mupuf> or rather, there are two terms
[23:09:28] <agd5f> about par for the course historically
[23:09:58] <mupuf> basically, there is an evaluation period every month
[23:10:00] * danvet still going to call that midterm
[23:10:04] <danvet> oh?
[23:10:21] <danvet> but still only 2 evalutions total?
[23:10:25] <mupuf> 3
[23:10:27] <danvet> ah
[23:10:34] <mupuf> there was 2 before
[23:10:35] <danvet> so maybe not midterm anymore ..
[23:10:41] <mupuf> exactly :D
[23:10:50] <mupuf> let's call it evaluation then
[23:11:06] <mupuf> anyway, 1 student was having academic duties, as you saw
[23:11:10] * danvet types something
[23:11:35] <mupuf> and when asked to make a choice, he chose the academic duties
[23:11:44] <mupuf> or maybe he did not have a choice
[23:11:51] <danvet> yeah, followed that thread
[23:11:59] <danvet> didn't sound like he had a choice really
[23:12:02] <keithp> seemed to resolve as well as could be expected
[23:12:07] <danvet> yup
[23:12:22] <mupuf> but anyway, he was active before and says he will remain active after that, so better not burn him out too
[23:12:33] <mupuf> so, this was good to have this evaluation period earlier
[23:12:49] <mupuf> and that was triggered also by forcing the students to blog every week
[23:13:30] <mupuf> sorry, I mean, the email thread
[23:13:38] <mupuf> so, I guess it is also a good requirement
[23:14:02] <mupuf> from this weekly blogging, I also make a g+ post
[23:14:15] <mupuf> which is good, as it forces me to keep track of what they are doing
[23:14:28] <danvet> sounds good
[23:14:41] <danvet> are the blogs all on planet.fdo?
[23:14:45] <danvet> I don't think I see them all
[23:14:49] <mupuf> no, only two
[23:14:52] <danvet> and they're also not on twitter ...
[23:14:53] <danvet> :-/
[23:14:54] <mupuf> the others did not ask to be on it
[23:15:01] <danvet> would be nice to have more publicity ...
[23:15:04] <mupuf> danvet: let me code :p
[23:15:08] <danvet> hm ok
[23:15:29] <mupuf> well, maybe we can get our twitter guy alanc to send a link to my blog posts
[23:15:35] <mupuf> they contain all the links
[23:15:45] <danvet> not around it seems ...
[23:15:47] <danvet> ah ok
[23:16:04] <danvet> mupuf, your blog also doesn't seem to be on plane.fdo?
[23:16:18] <mupuf> danvet: oh, no, I am doing it on our g+
[23:16:26] <mupuf> don't think there is an rss feed there
[23:16:30] <danvet> meh, not there :-)
[23:16:41] <danvet> anyway, sounds all great
[23:16:51] <danvet> mupuf, should we go to khrons right away?
[23:16:53] <mupuf> that's it for the gsoc
[23:16:55] <mupuf> we can :)
[23:17:19] <mupuf> so, khronos is basically pretty clear
[23:17:25] <mupuf> and has answered SPI properly IMO
[23:17:31] <mupuf> let's see what SPI will do!
[23:17:37] <mupuf> Hopefully, just sign the contract!
[23:18:06] <danvet> has someone from spi answered and confirmed it's on the agenda for their next mtg?
[23:19:06] <mupuf> no, would be great to ask them to put it there
[23:20:56] <mupuf> keithp: do you have better contacts than I have?
[23:21:04] <keithp> probably not
[23:21:24] <keithp> I can ask tbm if you like
[23:22:19] <mupuf> yeah, please do
[23:22:24] <keithp> will do
[23:22:27] <mupuf> thanks
[23:22:39] <mupuf> I already wrote an email two days ago, probably should not spam them
[23:23:30] <keithp> meh
[23:23:35] <keithp> sent
[23:23:43] <mupuf> thanks
[23:23:43] <danvet> mupuf, I've put you on the minutes too for asking ... 
[23:23:48] <danvet> I guess I'll change
[23:23:57] <mupuf> thanks :)
[23:24:02] <danvet> :-)
[23:24:25] <danvet> I guess I can give an xdc sponsorship update
[23:24:33] <danvet> not really news yet, need to ping people again
[23:25:22] <danvet> bryce_ and me also chatted a bit this week, and it sounds like the website sponsor is indeed serious
[23:25:38] <danvet> but he wanted half the price for 6 months instead of the full year, with maybe extending
[23:25:52] <danvet> we figured that's ok and within the rules we voted on
[23:27:18] <mupuf> sounds good, yeah
[23:27:27] <mupuf> we can have pro-rata service
[23:28:00] <agd5f> yeah, no objections here
[23:28:09] <danvet> Riastradh, anything on coc, or should I drop that from the agenda?
[23:28:19] <Riastradh> danvet: I am about to send my recommendations to board@.
[23:28:28] <Riastradh> Sorry for the delay about all this.
[23:28:43] <danvet> Riastradh, np, just wanted to check
[23:28:45] <Riastradh> (Also, sorry I missed the last meeting: had a conflict and didn't realize until too late!)
[23:28:49] <danvet> stuff sometimes takes time and we're all busy
[23:29:36] <danvet> ok, I think travel requests without our treasurer then?
[23:29:39] <danvet> does that make sense?
[23:30:25] <mupuf> danvet: sure
[23:30:59] <danvet> if I haven't missed one we have 2 open requests
[23:31:11] <danvet> jake edge from lwn for about 1500 usd
[23:31:33] <danvet> and laura abbott for around 1200 usd
[23:31:39] <danvet> mv is expensive it seems
[23:32:24] <keithp> yeah, hotels are ridiculous
[23:32:29] <mupuf> yeah...
[23:32:40] <mupuf> but we cannot ask both of them to share a room
[23:32:47] <agd5f> might want to ask jake to double check his numbers having seen the ones laura got
[23:32:54] <robclark> jake is probably under on his estimate
[23:33:00] <robclark> right
[23:33:11] <keithp> could be flights; he's coming from further away
[23:33:16] <danvet> yeah 300 for flight+rental
[23:33:27] <danvet> keithp, laura said her's is without flight
[23:33:46] <keithp> sounds about right
[23:33:57] <robclark> that said we (RH) did look around a while back and found some things at ~ $100/night, iirc...  more motel than hotel..
[23:34:06] <danvet> yeah jake might fly as aircargo for that price I think
[23:36:02] <danvet> robclark, can you reply with that on the thread? and I guess you could do an rh car or so?
[23:38:47] <robclark> i'll see if I can dig it up again..
[23:39:12] <robclark> probably some carpooling, given the public transportation situation there would be a good thing..
[23:40:24] <mupuf> one got to love california for its wonderful transportation system :D
[23:40:42] <danvet> might as well say US, excluding very few pieces ...
[23:41:05] <mupuf> true, except you probably get even worse trafic jams in california :D
[23:41:05] <keithp> nyc being a notable exception
[23:41:16] <mupuf> and boston, and chicago
[23:41:28] <keithp> although, the airports are still hard to get to/from almost everywhere
[23:41:40] <robclark> just don't drive in boston.. problem solved :-P
[23:42:06] <mupuf> just don't live in the USA, problem solved :D
[23:42:50] <danvet> so should we vote on those already, or postpone until rob dug out that motel stuff?
[23:43:11] <robclark> mupuf, heheh
[23:43:29] <robclark> danvet, I guess we can postpone
[23:43:44] <mupuf> let's move this discussion on the ML
[23:43:53] <robclark> k
[23:44:02] <mupuf> and send them the info whenever we have it
[23:44:06] <mupuf> XDC is coming fast
[23:44:58] <danvet> yeah
[23:45:13] <danvet> next mtg we'll definitely need to decide
[23:45:18] <danvet> robclark, can you do those mails?
[23:45:34] <robclark> yeah
[23:46:09] <mupuf> danvet: can't we decide on the ML?
[23:47:31] <danvet> that's an option too, but ime voting on the m-l takes months :-)
[23:48:34] <mupuf> ack, let's try and, if we fail, then let's do it at the next meeting :)
[23:48:38] <Riastradh> Voting, or hashing out the details of a question to vote on?
[23:49:10] <robclark> so, fwiw.. https://www.motel6.com/en/motels.ca.sunnyvale.1054.html#?checkinDate=2017-03-12&numNights=1&numGuests=2&travelAgentNumber=TA001124&corporatePlusNumber=CP571532&mid=GHA
[23:49:17] <robclark> (for example)
[23:49:49] <mupuf> this is more than decent
[23:50:14] <danvet> robclark, ok if I volunteer you to reply with that as an option on the thread
[23:50:17] <danvet> ?
[23:50:24] <danvet> folks need a rental anyway ...
[23:50:35] <robclark> not exactly the ritz but at least won't break the bank
[23:50:37] <robclark> sure
[23:50:40] <danvet> thx
[23:50:43] <agd5f> I think I actually stayed there at a previous XDC
[23:50:46] <danvet> I think that's it then
[23:50:51] <danvet> anything else?
[23:51:23] <Riastradh> ...Could possibly borrow bicycles from Google instead of renting cars...
[23:51:46] <robclark> that said, looks like not even a whole lot of rooms left in the motel6..
[23:51:54] <robclark> anyways, I'll try to dig up a few options
[23:52:01] <Riastradh> (Dunno if they do that for visitors, but they did that for employees when I was one briefly some years ago.)
[23:52:13] <mupuf> Riastradh: ha ha
"""]]
[23:52:18] [disconnected at Thu Jul  6 23:52:18 2017]
