
Date is 2011-11-10, times are UTC+01. 
[[!format txt """
22:59 <+alanc> oh duh, I do have the password to the elections committee mailman
23:00 <+alanc> just had to dig deeper in old e-mail
23:01 <+alanc> good afternoon, btw
23:01 <stukreit> hi!
23:01 <+emmes> good news. and hi.
23:04 <+alanc> okay, elections@foundation.x.org updated now
23:04 <+alanc> nothing like meeting time to remind you to clear action items from previous meetings
23:05 <+alanc> I unsubscribed all previous members and subscribed myself, anholt, stukreit & bart, and also listed us as admins/moderators in case stuff gets stuck in spam filters
23:06 <+alanc> so are our other members here?   agd5f?  keithp?   don't see bart or anholt in channel, and mherrb said he was travelling this week
23:06 <agd5f> hi
23:07 <+alanc> that makes 4 of us
23:08 <+alanc> stukreit: want to start off with treasurer's report?   everything taken care of for getting money from google?
23:08 <keithp> alanc: hi
23:09 <stukreit> we did get the application form submitted on time
23:10 <stukreit> and have a PO number.  I just pinged Stephane again about whether he'd write the invoice against this PO
23:10 <+Bart_Massey> Apologies for my lateness as usual.
23:10 <stukreit> And I have an email from Donnie with travel receipts, so his thing will go into the invoice and we'll flow the $$.
23:11 <stukreit> I need to talk to Justin, our sflc rep about whether he saw the prev. year's gsoc funds come, because his address is still on the PO.
23:12 <stukreit> Also, don't remember whether I reported last time: we have cleared up and are all paid up with IIT, so the ACM guys should be content now.
23:12 <stukreit> EOF
23:13 <stukreit> xof,  ttfn...
23:13 <+Bart_Massey> peace. thanks for the report.
23:13 <+Bart_Massey> and thanks as always for the hard work.
23:14 <agd5f> we may owe taxes in delaware again soon
23:15 <stukreit> Are you the one who finds that stuff out?
23:15 <agd5f> sflc let me know last time when we were working on the 501c3 stuff
23:15 <agd5f> they are still the contacts for all that stuff
23:16 <agd5f> they recommended we get a real address
23:16 <+Bart_Massey> i thought stukreit got a box near him?
23:16 <stukreit> I volunteered my home address for 5 years. I'll ask Justin about this.
23:16 <+Bart_Massey> perfect
23:17 <agd5f> let me see if I can find out about the taxes on the delaware website
23:19 <agd5f> ok, I paid it for 2010 when I paid for 2009, but we'll need to pay 2011 next year
23:19 <agd5f> for reference the site is https://delecorp.delaware.gov/eCorp/LoginAnnualReportsCLF
23:21 <agd5f> also, FWIW, the old xorg llc still owes several thousand in back taxes
23:21 <stukreit> I do not know this.  Do you have paperwork?
23:22 <stukreit> Are we accumulating penalties and interest?
23:22 <agd5f> stukreit: karen mentioned it back when we were working on the 501c3
23:23 <agd5f> the new delaware corp isn't responsible for them
23:23 <mherrb> ho - sorry to be late. Forgot that in .pt I was 1hr early
23:24 <+alanc> welcome, thought you weren't coming at all this week
23:24 <stukreit> agd5f: ok, I'll let it lie.
23:24 <+alanc> so are we done with treasury matters?
23:25 <stukreit> unless anyone has questions or is waiting on anything..
23:25 <+alanc> and for those who came late, I found the elections committee mailman password today and updated the list, so we should start discussing election calendar there soon
23:25 <agd5f> still might be worth talking to sflc about them at some point
23:26 <stukreit> 'k, I'll mention it.
23:26 <+alanc> libv sent mail about FOSDEM & XDC
23:27 <+alanc> sounds like FOSDEM is looking good for a joint Xorg/Wayland/ICC devroom for the weekend
23:27 <+alanc> and there's nothing the board needs to do unless/until people request travel funding to go there
23:28 <+alanc> as for XDC, I guess we need to ping airlied to see how serious he is about putting forth a Dublin proposal so we can decide soonish between that & the Nuremberg one
23:29 <agd5f> sounds good
23:29 <+emmes> yes, the earlier this is decided the better.
23:29 <mherrb> I agree
23:29 <mupuf> who will talk about wayland at the FOSDEM?
23:30 <+emmes> mupuf: this is nothing the board decides :-)
23:30 <marcoz> mupuf, are you volunteering?
23:30 <+alanc> I thought keithp had some wayland talks lined up
23:30 <mupuf> no, no. I was just wondering
23:31 <mupuf> alanc: IIRC, he wasn't sure he would get his travel funding accepted
23:31 <agd5f> I think libv has a list
23:31 <keithp> alanc: Kristian was asked to speak about wayland in the main track; I haven't gotten funding for some other wayland adventures yet though
23:31 <+emmes> keithp: but if he's already in europe for the main track, a second talk about a different, similar topic might be possible  *g*
23:32 <keithp> emmes: indeed
23:32 <keithp> I don't know the status of his main track talk
23:33 <+emmes> something else:
23:33 <+emmes> As libv (and earlier Michael) noted, there was a major hickup with paying fees for XDC.
23:33 <+emmes> We can't do anything about it anymore, but I think we seriously owe them an apology.
23:34 <+emmes> Given that they had their student's rights taken away - for some finite time.
23:34 <+alanc> it wasn't clear it was on our end though, since we'd already (over)paid and gotten a partial refund from the university at the time the student group was complaining to us
23:35 <+emmes> I think you have some information I'm missing here ;-)  Or I missread something.
23:35 <stukreit> fwiw, it is ack'd that iit's accounting was confusing to work with
23:35 <+alanc> if we screwed up, then yes we should apologize
23:35 <+emmes> doesn't really cost us anything, and it does help everybody to feel better.
23:36 <stukreit> I would like to understand who/what I'm apologizing for.
23:36 <+Bart_Massey> It would be nice to have a detailed, accurate account of what happened, though. Does anybody know for sure?
23:37 <stukreit> I can tell you what was happening on my end of things.
23:37 <+emmes> I have a writeup from Michael, but I'd like to hear the story from a different point of view.
23:37 <+alanc> the mail that went to the board said that Stuart paid on Oct. 19, yet the ACM chapter contacted us on Oct. 27
23:37 <+emmes> Yes, appreciated!
23:38 <stukreit> Is Michael open about what he wrote up? I don't see it as particularly useful to our org. otherwise
23:39 <+emmes> I'll have to ask him to be on the correct side, but I'm pretty sure he's fine. I'll send it to the board mailing list
23:40 <+Bart_Massey> Thanks!
23:42 <+alanc> okay, so we'll be taking that to e-mail for the details?
23:42 <+emmes> I'd think so. It's easier to deal with.
23:43 <+alanc> okay, the other item I have a note asking to discuss today is the virtual book sprint, if Bart's had any time for organization on that
23:43 <+Bart_Massey> Not so much.
23:43 <+Bart_Massey> I've had some talks with the XCB folks about a workshop, though
23:44 <+Bart_Massey> I wish I could say we'd resolved it :-)
23:44 <+Bart_Massey> But I'll let the Board know as it progresses
23:44 <+Bart_Massey> I think right now we're in the planning dates and pricing venues stage
23:45 <+Bart_Massey> marcoz and I need to coordinate on the book sprint.
23:45 <+Bart_Massey> marcoz: where are you physically located these days?
23:46 <marcoz> Colorado
23:46 <+Bart_Massey> guess he's not around
23:46 <+Bart_Massey> oh, there you are :-)
23:46 <marcoz> sorry, stepped away for a bit
23:46 <+Bart_Massey> cool. give me a call on my cell sometime soon and we'll just discuss. 
23:46 <+Bart_Massey> NP!
23:46 <marcoz> do I have your #?
23:46 <+Bart_Massey> XXX XXX XXXX
23:47 <marcoz> thx
23:47 <+Bart_Massey> Well, gtg: Got a meeting in a few minutes. come late, leave early, that's my motto
23:47 <+alanc> anything else anyone wants to discuss today?
23:48 <+Bart_Massey> thanks all!
23:48 <marcoz> anyone up for being interviewed for HPR?
23:48 <+alanc> oh, I vote to cancel the next meeting, since it falls on Thanksgiving in the US
23:48 <marcoz> thoughts on wiki rework?
23:48 <+Bart_Massey> alanc: +1
23:48 <+Bart_Massey> marcoz: HPR?
23:48 <marcoz> hackerpublicradio (.org)
23:48 <marcoz> public radio for hackers.  
23:48 <+Bart_Massey> marcoz: happy to play anytime they like. lmk.
23:48 <marcoz> anyone can send in episodes. I did 4 interviews at XDC
23:49 <+Bart_Massey> marcoz: nice
23:49 <+emmes> alanc: +1 guess it doesn't make much sense in this case
23:49 <agd5f> +1
23:49 <stukreit> +1
23:49 <+Bart_Massey> See you all in about 4 weeks!
23:50 <+alanc> wiki reorganization is mostly an implementation detail the board leaves up to the development community to handle, unless you need us to allocate funding or something (though please don't stomp on the board pages without talking to us first so we know where to find them)
23:51 <marcoz> no funding, more of asking about thoughts on how to do it without stomping on what's there.
23:51 <mherrb> same goes for security pages. They are a bit of mess already since the last reorganization.
23:52 <marcoz> the last thing I want to do is reorg a reorg and make things worse.
23:53 <marcoz> i can't really do much until after Dec anyway. I want to finish the docbook stuff, but I wanted to get thoughts in advance
23:54 <mherrb> Are planning to stay with MoinMoin or switch to something else? If you stick with Moin  it's easy to draft some pages...
23:54 <marcoz> mherrb: my initial thinking is mediawiki
23:55 <marcoz> but i realize that move isn't quick or easy and that the fd.o admins have to set things up right?
23:56 <mherrb> as far and I understand it yes. you can still draft stuff somewhere else though.
23:56 <marcoz> i'm thinking to set something up on a separate site to try things out.
23:57 <marcoz> soas to not step on things on the wiki.  mupuf and I hashed some things out at XDC.  I'm gonna take that and mediawiki and see what I can do.
23:58 <mupuf> marcoz: don't hesitate to ping me if you need help
23:58 <marcoz> mupuf, guaranteed I'll be pinging you. :)
23:58 <+alanc> yes, you'll need to talk to tollef or one of the other fd.o admins if you're considering a change of software
23:59 <+alanc> I do like the way the ikiwiki on xcb.freedesktop.org lets me git clone & push to the wiki, but that's far more hacker friendly than user friendly
00:00 <marcoz> my thinking is it has to be user friendly. I really want to get more people into X, especially wiki stuff, and I think something that's wellknown and used, like mediawiki, is probably the best bet.
00:01 <marcoz> i reserve the right to be completely wrong.
00:01 <+alanc> spam protection is essential
00:02 <+alanc> (not that I'm biased from being the one to hit the despam button 50 times a week on the current wiki)
00:02 <marcoz> yea, I see the wiki recent history.  you're constantly busy with that.
00:03 <+alanc> having an e-mail subscription for the wiki page "*" helps me see it soon after the spam comes in
00:03 <marcheu> stukreit: RE invoice: haven't done it yet, I think the deadline is soon
00:03 <marcheu> stukreit: will get to it
00:03 <+alanc> most of the time it's just a matter of clicking the link in the e-mail, then clicking the despam link on the page
00:04 <stukreit> alanc: there is no invoice page. we have to write one up.  I think there's 30 days for this, but if I can get marcheu to do it now, all the better ;-)
00:05 <+alanc> stukreit: I was still in the wiki conversation with marcoz, not talking about invoices
00:05 <+alanc> in any case, I think we've run out of board meeting, as we passed the hour
"""]]