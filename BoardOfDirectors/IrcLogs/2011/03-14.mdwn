
Date is 2011-03-14, times are UTC+01. 
[[!format txt """
22:00 <alanc> good afternoon
22:00 <agd5f> hello
22:01 <alanc> stukreit sends his regrets, he's off breaking spring with his offspring
22:01 <agd5f> congrats to all the new board members
22:02 <+Bart_Massey> howdy!
22:02 <+anholt> hello
22:03 <alanc> though other than stuart, all the "new" board members are the old ones back again 8-)
22:03 <+mherrb> hi
22:04 <alanc> thanks to the election committee for steering us through the election process successfully
22:04 <alanc> may those who were just elected do as well next year, as they are now the election committee
22:05 <+Bart_Massey> let me be really clear--I will never again chair an election committee
22:05 <+dberkholz> indeed, congrats to you
22:05 <+dberkholz> feel free to get rid of my autovoice if anyone is so inclined =)
22:05 <agd5f> Bart_Massey: that makes two of us ;)
22:08 <alanc> does having voice really mean much in this channel other than an extra mark/icon by your name?
22:09 <+Bart_Massey> beats me
22:10 <+alanc> I guess this way I can easily see all 8 board members are logged into the channel, though I've not heard emmes or keithp speak up yet
22:11 <+alanc> so does the new board want to select a new secretary and/or treasurer?
22:12 <+Bart_Massey> I move we retain both alan and Stuart
22:12 <+mherrb> They're both doing a great job.
22:12 <+Bart_Massey> yep
22:12 <+Bart_Massey> +1s?
22:12 <+alanc> I'm willing to continue, and I think stuart is as well (sounded like it from his mail over the weekend about the bank status)
22:13 <+Bart_Massey> Stuart already promised us 5 years or somesuch...
22:13 <+Bart_Massey> :-)
22:14 <+alanc> I'm certainly in favor of letting him continue to deal with the banking mess
22:14 <+Bart_Massey> Would the various Board members please vote on my motions? I'll start with Stuart:
22:14 <+Bart_Massey> Move to retain Stuart as Treasurer
22:15 <+mherrb> +1
22:15 <+alanc> +1
22:15 <+Bart_Massey> agd5f:?
22:15 <+Bart_Massey> anholt:?
22:16 <+agd5f> +1
22:16 <+Bart_Massey> tx! anholt?
22:17 <+Bart_Massey> 'k, looks like no quorum
22:17 <+Bart_Massey> next meeting, I guess
22:18 <+alanc> do we need to change meeting times?   or is that best discussed via e-mail to get input from those who aren't here?
22:18 <+Bart_Massey> Yeah, lets ask on email. I don't remember  
22:18 <+Bart_Massey> if this time works for me next school quarter
22:19 <+Bart_Massey> Kind of think so, but I'm not too picky
22:20 <+Bart_Massey> Only new business I have is to discuss the recent, probably on-hold offer by a company to directly fund some specific X development. What do people think about this in general?
22:20 <+agd5f> seems fine to me.
22:20 <+Bart_Massey> It's a bit awkward, because the Board, who presumably decides this stuff, specifically doesn't set technical direciton.
22:21 <+Bart_Massey> agd5f: My concern is that we end up funding work that helps a company but makes X worse overall.
22:21 <+alanc> do we need to get involved at all?  if they have money to pay someone, can't they just do it directly, like the many other companies paying people to work on X?
22:21 <+Bart_Massey> You'd think. BUt in this case I think they didn't know how to do that, or who to do it with---and wanted to help X.Org
22:21 <+agd5f> Bart_Massey: we can't decide what gets committed per se.  the company is free to do the work.  the community has to decide if it goes in
22:22 <+alanc> certainly we haven't integrated all of the work students did for GSoC projects in the past
22:22 <+Bart_Massey> I guess I'm just wondering if we have any existing general principles or if we need any, or whether we should just handle it all on a case-by-case basis, at least for now.
22:23 <+Bart_Massey> You are both correct, of course. The joy of GIt is that we don't lose work entirely by leaving it off mainline.
22:23 <+alanc> I think we'd want to set the general principal up front that funding development does not guarantee integration
22:23 <+mherrb> They may prefer making a donation to the X.Org fundation rather than hiring someone directly to do the job.
22:23 <dberkholz> acting as intermediary for paid work might put the foundation in an awkward place
22:23 <+mherrb> In many .eu contries hiring people has constraints.
22:24 <dberkholz> might want to check w/ legal about possible issues
22:24 <+Bart_Massey> I think if we're looking at something like EVoC as ave
22:24 <+Bart_Massey> vehicle, that part is ok.
22:25 <+Bart_Massey> The trick is typically to set the funding as a stipend rather than a salary
22:26 <+Bart_Massey> Anyway, I promised to raise the issue here and I have. It sounds like we are all in general agreement.
22:26 <+Bart_Massey> At least we'll have some notes in the minutes for next time this comes up.
22:26 <+Bart_Massey> So do we have a date and venue for XDC 2011 yet?
22:28 <+alanc> don't think so
22:28 <+mherrb> we have Michael Larabel proposal, but we haven't heard anything new from him on this since last september
22:28 <+mherrb> or did I miss something/
22:28 <+Bart_Massey> need to get it done now or we won;t make
22:28 <+alanc> most serious proposal so far seemed to be Michael Larabel's - I don't remember if he was actually planning to do all the organization work though, which is the hard part
22:29 <+alanc> if we wanted to piggyback on Linux Plumbers again, that appears to be "in Santa Rosa, CA, September 7-9, 2011"
22:30 <+alanc> which is unfortunately, not really local to anyone
22:30 <+alanc> though only a few hours drive for those of us at the other end of the SF Bay
22:30 <+Bart_Massey> I'm good with whatever ;
22:30 <+Bart_Massey> we mostly just need someone to step up and organize
22:30 <+Bart_Massey> like in the next week or so
22:31 <+Bart_Massey> doesn't need to be  a Board  member
22:31 <+alanc> do we need to put out a call for volunteers?
22:31 <+Bart_Massey> Yes
22:32 <+Bart_Massey> SInce I don't hear anyone here taking point :-0
22:32 <+Bart_Massey> First step would be to check back in with Larabel I think
22:32 <+alanc> I guess I can write up a message to post and run it by the board for comments
22:32 <+Bart_Massey> Thanks!
22:32 <+Bart_Massey> Or just post it; I personally don't need to review it
22:33 <+agd5f> agreed
22:34 <+Bart_Massey> Anybody got other business for today?
22:35 <+mherrb> not me
22:35 <+agd5f> hopefully stukreit can get the financial info sorted.  We are running out of time on the 501c3 window with the IRS
22:35 <+Bart_Massey> cool. move to adjourn
22:35 <+Bart_Massey> agd5f: when exactly?
22:36 <+agd5f> Bart_Massey: ~1 month
22:36 <+Bart_Massey> geez
22:36 <+agd5f> I'll have to ask Karen for the exact date
22:36 <+alanc> stuart should be back in a couple days, I'll poke him when I see him
22:37 <+Bart_Massey> Please make sure Stuart and Keithp are aware of this deadline.
22:37 <+Bart_Massey> It would really, really stink to miss it.
22:37 <+alanc> though I think he took his laptop to check e-mail too
22:37 <+agd5f> It's pretty bad.  We had like 2 years to file...
22:37 <+Bart_Massey> s/file/flail/ :-)
22:38 <+Bart_Massey> Oh well. 
22:38 <+Bart_Massey> Anything else?
22:38 <+alanc> I don't have anything else
22:39 <+Bart_Massey> cool. Move to adjourn
22:39 <+agd5f> +1
22:39 <+Bart_Massey> opposed?
22:39 <+Bart_Massey> bye all!
"""]]