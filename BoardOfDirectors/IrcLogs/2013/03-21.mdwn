
Date is 2013-03-21, times are UTC+02. 
[[!format txt """
22:00 <agd5f> hello
22:00 <+Bart_Massey> Hey all!
22:01 <marcoz> hi
22:02 <+alanc> hello
22:02 <+alanc> stuart just walked by and said he was logging in
22:03 <stukreit> hello
22:04 <+Bart_Massey> OK, I guess we'll get going and see who else shows...
22:05 <+alanc> looks like we're short keithp, anholt, and emmes?
22:05 <+Bart_Massey> Yep
22:05 <stukreit> bart, I don't know what we're gonna do without your engine of activity to help propel us along
22:05 <+Bart_Massey> Are "elections" on track? Has the voting been opened to the membership, and are votes being collected?
22:05 <+alanc> well, hopefully their irc clients just beeped or flashed at them to wake them up
22:06 <marcoz> yes, yes and yes
22:06 <+Bart_Massey> stukreit: Kind of you to say, but I think you'll be OK :-)
22:06 <+Bart_Massey> marcoz: Awesome. Thanks.
22:06 <+alanc> I saw mail telling me I can vote, but another member I asked hadn't seen it
22:06 <+alanc> of course, that could be a factor of the mail system, not the sender
22:06 <marcoz> did everyone here see the email?
22:06 <agd5f> yes
22:06 <+Bart_Massey> Let me check.
22:06 <agd5f> and I voted already
22:06 <+keithp> alanc: I'm around
22:07 <+Bart_Massey> Yeah, I saw it.
22:07  * keithp voted even
22:07 <marcoz> cool.  
22:08 <+Bart_Massey> OK, second item: 501(c)3. Anything to report / do there? keithp, did you want to talk about the alt plan there?
22:08 <stukreit> Aaron made contact with the IRS, they will reply in more detail in 30 days, but basically, we have to re-gen the app materials and resubmit
22:08 <+keithp> Bart_Massey: the alternative would be to become an SPI affiliate project and use their 501(c) 3 status for new monies
22:09 <+keithp> keep our existing accounts intact, but route future charitable donations through SPI
22:09 <marcoz> pros? cons?
22:09 <+keithp> oddly, they have an accountant running their books and keeping filings up to date
22:09 <stukreit> would we be parking funds there and able to pull them across when this is straightened out?
22:09 <+Bart_Massey> stukreit: Is the application likely to be a multi-year process the second time?
22:10 <stukreit> Don't know.
22:10 <+keithp> stukreit: that's one option, the other would be to just remain a spi affiliate indefinitely
22:10 <stukreit> if it were possible to get the same IRS reviewers lined up, maybe that would expeedite.
22:10 <agd5f> It shouldn't take as long since we don't have to gather all the info for the application
22:10 <+Bart_Massey> The nice thing about the SPI plan, which we've discussed before, is that they would handle books, corporation, taxes, etc.
22:10 <agd5f> that's what dragged on last time
22:10 <+alanc> right, I thought it was only because of history that we were a separate corporation in the first place
22:11 <stukreit> that's true, the time it took for the final approval was short
22:11 <+Bart_Massey> agd5f: Yeah, our end should be quicker, but it took more than a year for IRS to respond IIRC?
22:11 <agd5f> Bart_Massey: I think their response was pretty quick.  took us over a year to apply
22:11 <+Bart_Massey> 'k.
22:12 <stukreit> anyway, I'm on the hook to push this paper along. Hope to know more in a month or 2 what kind of time it will take
22:12 <+Bart_Massey> We don't have to settle this today, but if X.Org is going to go SPI, probably should decide soon to avoid the rework of filing again.
22:13 <+Bart_Massey> Let's all y'all take it to email to discuss, unless anybody has ideas or opinions or questions now?
22:14 <stukreit> we'll find out soon whether refiling consists of reading the docs once, updating to 2013, and stuffing an envelope again. It shouldn't be much harder than that
22:14 <agd5f> I'd rather be our own if we can do it
22:14 <+Bart_Massey> agd5f: How come?
22:14 <stukreit> I'll inform the board of each step/expense before taking any action
22:15 <agd5f> Bart_Massey: seems like a shame to forgo all the effort we put in.  that said, I don't know much about spi
22:15 <+Bart_Massey> agd5f: Fair enough. 
22:16 <+Bart_Massey> Any other discussion?
22:16 <+Bart_Massey> OK. Last thing; for some time now Karen Sandler has been urging us to take part in a Gnome Women's Outreach program they have going.
22:17 <+Bart_Massey> The deadline is next week or something now, so I told her I would bring it to our Board and we'd discuss.
22:17 <stukreit> got a pointer to explanation? How would we engage with it?
22:17 <+Bart_Massey> AFAICT, the immediate commitment would be for us to fund an EVoC-like slot for a Gnome Women Student position.
22:18 <+Bart_Massey> With said student to be supplied by them, as near as I can gather.
22:18 <+alanc> it sounds like it's an EVoC-like program, but targeted at increasing female involvement in open source communities, instead of student involvement
22:18 <agd5f> seems fine to me
22:18 <marcoz> yea
22:18 <+Bart_Massey> alanc: I think it's maybe female intersect student?
22:18 <+alanc> I don't remember that detail - think I sent a link to the board list a few months back
22:19 <+Bart_Massey> I think we can only fund students according to current policy, so we need to make that clear to them.
22:19 <+alanc> https://www.gnome.org/news/2013/01/25-women-in-10-free-software-organizations-for-gnomes-outreach-program-for-women/
22:20 <+Bart_Massey> Let's do a formal motion. I move to fund a position for a female student in Gnome's outreach program.
22:20 <stukreit> +1
22:20 <marcoz> would this be an X EVoC project?  or X funding a gnome project?
22:20 <jcristau> "Unlike Google Summer of Code, the Outreach Program for Women is open to non-students and non-coders, so you should just apply for the Outreach Program for Women internship if you are either not a student or not a coder.", from https://live.gnome.org/OutreachProgramForWomen
22:21 <+alanc> https://live.gnome.org/OutreachProgramForWomen sounds like they don't necessarily restrict to students, though we may be able to do so for our slot
22:21 <+Bart_Massey> jcristau: Thanks
22:21 <+Bart_Massey> alanc: I think we have to be able to for our slot.
22:22 <+alanc> also says cost for 1 participant is $5,750 - $5,000 (USD) stipend, $500 travel allowance, and a small administrative fee for the GNOME Foundation. - which is in line with our normal EVoC costs
22:22 <+alanc> +1 for funding that
22:23 <marcoz> reads like evoc, just not coding specific.  It mentions documenation and marketing so +1 from me.  :)
22:24 <+Bart_Massey> We need one more +1 to carry, by my count...
22:24 <+keithp> Bart_Massey: restate as a motion to fund a women student?
22:24 <+Bart_Massey> keithp: I will restate as a motion to fund a woman student, with other details to be worked out later.
22:25 <+alanc> oh, and like EVoC, I think our funded slot should be open to work on X11, Mesa, Wayland, or other closely affiliated projects (they specify that people funding slots can decide which projects they are funding it for)
22:25 <+keithp> +1 for a women student then
22:25 <+Bart_Massey> alanc: I think someone needs to negotiate with Karen on how specific we can be. Can you or keithp call her?
22:27 <agd5f_> sorry, having issues here
22:27 <+Bart_Massey> I promised her someone would call her today or tomorrow. I can do it if no one else will, but it would be better to have someone who is continuing with the Board IMHO.
22:27 <+alanc> I'm traveling the rest of this week - I could do it next week, but not this week
22:28 <+alanc> can keithp do it this week?
22:29 <+keithp> I should have time tomorrow
22:29 <+Bart_Massey> Great!
22:29 <+Bart_Massey> I think that does it. Any other business?
22:29 <+keithp> Bart_Massey: breakfast tomorrow, then you'll remind me :-)
22:30 <agd5f_> I guess I'm going to fill out the GSoC application if anyone wants to help
22:30 <+Bart_Massey> keithp: Have to be downtown at 10, so if you want *early* breakfast...
22:30 <+keithp> harsh, dude
22:30 <+Bart_Massey> agd5f: What happened to our volunteer GSoC admin?
22:30 <+alanc> agd5f_: wasn't tom stellard helping with that, or just with the ideas page?
22:30 <agd5f_> Bart_Massey: he's moving this week and next
22:30 <+alanc> oh
22:31 <+Bart_Massey> Oh. Thanks for taking it, then! I'll help how I can--LMK.
22:31 <+Bart_Massey> Anything else?
22:32 <+Bart_Massey> OK. Move to adjourn. Been great working with all of you lo these many years. An honor and a privilege.
22:32 <stukreit> thanks for all your help, Bart!
22:32 <marcoz> Bart_Massey: it's been brief for me, but a pleasure.
22:32 <+alanc> you're still organizing an XDC, we won't let you escape talking to us that quickly
22:32 <+Bart_Massey> And it's not like I'm disappearing or anything--I'll try to keep helping.
22:32 <+Bart_Massey> alanc: Indeed. Among other things.
22:32 <+alanc> or at least co-organizing with idr, keithp, etal
22:32 <agd5f_> thanks Bart_Massey!
22:33 <marcoz> any swag this year?
22:33 <+Bart_Massey> Thank y'all!
22:33 <+alanc> thanks for all the work in the past years
22:33 <stukreit> Elvis has left the building, sniff
22:34 <marcoz> we actually have one more meeting before the new board members 
22:34 <+alanc> well that'll be anticlimatic for Bart 8-)
"""]]