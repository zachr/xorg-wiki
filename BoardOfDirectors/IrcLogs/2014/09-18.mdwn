Date is 2014-09-19, times are UTC+10.  
[[!format txt """
--- Day changed Fri Sep 19 2014
06:57 < whot> good morning
06:58 < stukreit> hi
06:58 <+alanc> good afternoon
07:02 < agd5f> hi
07:05 < whot> keithp, mupuf: ping
07:06 < whot> agenda for today is quite limited, I don't have much. For the archives/minutes: Check's EVoC project didn't pass the midterm evals. That was handled over the board mailing list since last weekend
07:07 < stukreit> but he appealed to soldier on.  What status do we give that?
07:07 < whot> Nothing much to report on OPW, we have a couple of xcb projects submitted and Chrisitan is talking to at least one interested party regarding server-side xcb
07:08 < whot> I need to send another reminder for projects to all the lists. I also had Marina ask whether we can sponsor more projects if the need arrives and gave her a tentative yes
07:08 < whot> but given we don't have a lot of projects yet anyway that question is moot atm
07:10 < whot> stukreit: yeah, he re-submitted a proposal. same conditions apply though, he'll have to find a mentor to say yes first, so until Dave is convinced this time he can stick to the time-frame it's idling time for us.
07:10 < stukreit> ok
07:14 < whot> if mupuf was were I'd ask him what's up with XDC but I guess that's moot too :)
07:14 < whot> stukreit: can you send me a list of the ppl that we have granted travel assistance for? need that for the state of the union talk
07:14 < whot> and I'm too lazy to fish it out from the minutes :)
07:15 < stukreit> I guess I have to unlazy myself then..
07:15 < stukreit> this is a good list to have.
07:16 -!- marcoz [~oftc-webi@masquerade.micron.com] has joined #xf-bod
07:18 < whot> i think that's all for today unless someone else has something
07:19 < whot> btw, we have 73 ppl registered for XDC, which I think is the most we've had in a while
07:19 < agd5f> nice!
07:20 < stukreit> whot:  from Sept 4:   David Herrmann, Jamey Sharp, Roy Spliet
07:21 < whot> stukreit: just email it
07:21 <+alanc> and it sounded like we'd have a good full schedule of talks from what mupuf has so far
07:21 < whot> otherwise I have to email it to myself to make sure it's there when I need it :)
07:22 < stukreit> k
07:22 < whot> ok, I guess that's it for today with anything official. thanks for attending
"""]]
