[[!format txt """
[14:13:23] [connected at Thu May 10 14:13:23 2018]
[14:13:35] [I have joined #xf-bod]
[14:13:44] <bryce> robclark, I can do it
[14:13:51] <robclark> ahh, good, we have a secretary :-)
[14:13:59] <bryce> I've just kicked off the script
[14:14:05] <robclark> thx
[14:14:13] <bryce> no clue on agenda tho
[14:14:44] <bryce> did danvet say what the agenda should be, before he left?
[14:14:58] <robclark> I don't see anything..
[14:15:22] <anholt> nothing that I could find.  fdo merger stuff seems like the main thing on my list, but I would want danvet around for that.
[14:15:36] <hwentlan> do we need to review the previous minutes and follow up on action items?
[14:15:37] <robclark> so, I guess there is ongoing discussion about fd.o plus related CoC topic on board list.. but I guess unless anyone has anything to add here, we can just keep discussing that on list.. I guess not really something ready to vote on
[14:15:54] <bryce> last time's minutes https://www.x.org/wiki/BoardOfDirectors/MeetingSummaries/2018/04-26/
[14:16:05] * robclark looks
[14:16:22] <bryce> yeah let's table fdo until danvet is around
[14:16:42] <bryce> == Treasurer's Report ==
[14:16:47] <robclark> first of all.. I might have missed the first minute or two.. anyone else besides hwentlan / anholt / Riastradh  / bryce here?
[14:16:49] <bryce> this is done, and posted
[14:17:07] <robclark> cool
[14:17:11] <bryce> there were some follow up items (which I think were listed in the report)
[14:17:16] <mupuf> Hi guys, sorry for being late, but I can't stay either. For GSoC, we officially have both students, and they will start working on their projects next week or the following one (been on vacation this week).
[14:17:37] <robclark> hi mupuf 
[14:17:38] <bryce> there was a large chunk of money we got sept 2017, about $6k, as "donation" which we didn't know what it was
[14:18:12] <bryce> checking with SPI, they revealed it to be $5.2k from GSoC, plus $1k from a web sponsor, plus misc.
[14:18:42] <robclark> ok
[14:18:53] <bryce> the other issue was that when we transferred assets from BofA, Stuart did it in two transactions
[14:19:07] <bryce> the first, $33k, landed in 2016 and is verified
[14:19:40] <bryce> the second, which was about $300 or so, Stuart got the check from BofA mailed to him, but there's no record of it reaching SPI
[14:20:00] <bryce> I've contacted Stuart to find out what happened, and he said he'll investigate but no word so far.
[14:21:14] <bryce> we also have a ticket open with SPI for verification of payment from our 2018 sponsors, for XDC.  We're still waiting for word on that.
[14:21:32] <robclark> ok, not sure if it is possible now that BofA account is closed.. if the account was open at least we could probably get details of whether / where it was cashed..
[14:21:41] <bryce> SPI says they're going to work on a new system that'll give us better visibility into current transactions.
[14:21:49] <bryce> robclark, yeah...
[14:23:03] <bryce> anyway, so that all kept me pretty busy on X last week ;-)
[14:23:45] <bryce> that's it for treasurer stuff I think.
[14:23:55] <bryce> == GSoC ==
[14:24:16] <robclark> <mupuf> Hi guys, sorry for being late, but I can't stay either. For GSoC, we officially have both students, and they will start working on their projects next week or the following one (been on vacation this week).
[14:24:28] <bryce> sounds good
[14:25:12] <robclark> if nothing more to mention on GSoC, I have one small note about EVoC..
[14:25:17] <bryce> ok
[14:25:21] <bryce> == EVoC ==
[14:26:40] <robclark> I setup (or rather asked daniels and he setup) a evoc@foundation.x.org list, with the idea that it could be contact point for EVoC related requests (basicaly so things don't end up burried in my inbox if someoen asks something while I'm at a conference or busy on a deadline..)
[14:26:50] <robclark> I mentioned this on board list too, fwiw
[14:27:13] <robclark> right now that goes to me and tlwoerner who volunteered to help.. if anyone else wants to help I can add you
[14:27:21] <tlwoerner> o/
[14:27:35] * tlwoerner hasn't seen any EVoC-related emails so far
[14:27:38] <robclark> so if ok with everyone, I'll update evoc wiki page to direct students to that email address
[14:27:45] <bryce> robclark, +1
[14:27:46] * robclark hasn't updated wiki yet
[14:27:50] <tlwoerner> ah
[14:27:53] <tlwoerner> :-)
[14:27:54] <hwentlan> sounds good
[14:28:10] <robclark> ok
[14:28:44] <robclark> I should start a cleanup of project ideas page too, I suppose
[14:29:09] <robclark> anyways, that is about all I have
[14:29:20] <bryce> == Other Business ==
[14:29:29] <bryce> Anyone got anything else?
[14:29:52] <anholt> nothing from me
[14:29:52] <bryce> hwentlan, anholt any remaining issues from onboarding we can help on?
[14:30:14] * robclark has nothing else
[14:30:18] <hwentlan> looks like i'm all setup as far as i can tell
[14:30:51] <bryce> cool
[14:31:45] <bryce> robclark, on the mailing list you asked about places to store credentials
[14:32:10] <robclark> ahh, right.. yeah board list archives might not be ideal long-term ;-)
[14:32:23] <bryce> robclark, since git-crypt is set up already for the archives repo, could just create an encrypted subdir there with everyone's gpg keys
[14:33:25] <robclark> hmm, I guess then it needs to get updated, and list admin passwords changed when board members come and go..  although maybe that isn't a bad plan
[14:34:04] <anholt> I like the sound of this
[14:34:22] <robclark> fwiw, I have passwd for evoc list and election list currently.. I guess the election list should be accessible to whoever is one the election committee that year?
[14:35:00] <robclark> I guess beyond those two lists, there is the board list and the members list?  Anything else?
[14:35:37] <bryce> anything related to the elections tool maybe?
[14:35:49] <bryce> db pwds or whatnot?
[14:36:12] <robclark> the election tool is really just the pile of php that is members.x.org.. I think anyone with an account on gabe can access it?
[14:36:39] <robclark> (maybe we should do better than that, tho?)
[14:37:43] <robclark> I guess the first thing is we should enumerate what different things we have where password needs to be managed or access controlled, and then figure out who should have access to what
[14:38:06] <bryce> robclark, there's two approaches I can think of
[14:39:03] <robclark> I guess I can start a thread on board list about that.. because there are probably other list or things that I'm not thinking of atm.. probably most things all board members should have access to with the exception of members.x.org and election related stuff..
[14:39:17] <bryce> 1. all board members have access to the entire set of credentials.  Could be done via git-crypt in a subdir of the archives repository.
[14:40:08] <bryce> 2. limited access per-service to a subset of board members (even non-board members), with each credential stored in a separate file that are manually encrypted with the specific authorized people's gpg keys
[14:41:09] <robclark> offhand, I think most things fall into category #1 with exception of members.x.org and election list, which should be restricted to election committee
[14:41:09] <bryce> the second approach would be more flexible, but also a bit more complex to use
[14:41:58] <bryce> oh one other caveat with git-crypt, I'm not sure there's a way to *remove* people from access.  I'd need to check on that
[14:42:12] <bryce> anyway, yes good thing to hash out on the mailing list.
[14:42:23] <bryce>  
[14:42:31] <robclark> remove possibly == create new file with new generated passwords
[14:42:33] <bryce> if there's no other business, guess we can close the meeting?
[14:42:40] <robclark> sure, sgtm
[14:42:46] <bryce> == EOM ==
[14:43:02] <hwentlan> sure
[14:43:11] <tlwoerner> doesn't the secretary have some sort of "meeting recording" api?
[14:43:51] <robclark> I'm not sure details of how it works but I believe secretary logs the irc channel during the mtg.. if that is the question?
[14:44:20] <tlwoerner> yes, that's the question. but doesn't it need commands to start and stop?
[14:44:40] <bryce> no, it actually pops up an editor section for me to write the minutes into
[14:45:05] <bryce> so when I close that editor it snags the log
[14:45:06] <tlwoerner> ah
[14:45:33] <bryce> I think I'm supposed to fill it in as the meeting goes, but I am not so resourceful, I just fill it in after
"""]]
[14:51:47] [disconnected at Thu May 10 14:51:47 2018]
