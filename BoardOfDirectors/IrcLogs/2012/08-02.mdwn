
Date is 2012-08-02, times are UTC+02. 
[[!format txt """
23:00 <agd5f> hello
23:01 <stukreit> hi
23:01 <+Bart_Massey> Howdy
23:01 <+alanc> good afternoon
23:01 <+Bart_Massey> alanc, emmes, keithp: ?
23:01 <+Bart_Massey> er
23:01 <marcoz> hi all
23:01 <+Bart_Massey> anholt
23:02 <+Bart_Massey> OK, looks like we're 5, which looks like what we've got today
23:03 <+Bart_Massey> Start with the obvious: it looks like no one has an objection to joining OIN. Correct?
23:03 <agd5f> +1
23:03 <+Bart_Massey> +1
23:03 <stukreit> +1
23:04 <marcoz> +1
23:04 <+Bart_Massey> alanc: ?
23:04 <+alanc> sorry, got interrupted
23:04 <+alanc> +1
23:05 <+Bart_Massey> Motion made and carried. alanc: let us know what we need to do.
23:05 <+anholt> hi
23:05 <+anholt> +1
23:05 <+Bart_Massey> Hi!
23:05 <+alanc> I'll respond to them and find out
23:05 <+Bart_Massey> alanc: thanks
23:05 <+Bart_Massey> marcoz: EVoC stuff?
23:06 <+emmes> hey all
23:06 <agd5f> hi
23:06 <stukreit> evoc: all paid up
23:06 <+emmes> about OIN i'm +1 as well
23:06 <+Bart_Massey> OK, we're 7/8 on that one.
23:07 <+Bart_Massey> stukreit: Thanks huge
23:07 <marcoz> last i pinged the mentors.  Blaz, Supreet and Jess continue to do well.
23:07 <marcoz> Unfortunately, Ashwin has had some trouble.
23:07 <+Bart_Massey> we talked about that some last week.
23:07 <+Bart_Massey> er last meeting
23:07 <+Bart_Massey> do we still have an app outstanding?
23:07 <+Bart_Massey> or are we caught up there?
23:08 <marcoz> We have an app outstanding, but it's taking some time
23:08 <marcoz> He seems eager, has some commit history, 
23:08 <marcoz> There's  a conversation going with some of the piglit/mesa folks,
23:09 <marcoz> trying to narrow down his focus,
23:09 <marcoz> but finding a Mentor is proving difficult
23:09 <+Bart_Massey> OK. Do let us know when it's figured out. Thanks much as always.
23:09 <marcoz> will do.
23:09 <+Bart_Massey> Any XDC stuff we need to deal with here today?
23:10 <+Bart_Massey> Not hearing anything.
23:10 <marcoz> re:  CFP.  I'm afraid I've dropped the ball on this. 
23:10 <marcoz> At this point, is it worth sending out?
23:11 <marcoz> It's ready, but the dates are so short.
23:11 <+Bart_Massey> Yeah, we have to send something.
23:11 <+Bart_Massey> Just stretch the dates again.
23:11 <+Bart_Massey> What is it currently set at?
23:11 <agd5f> we need to get slots filled up
23:11 <+alanc> I can't remember when, if ever, we've had a formal CFP before, so I don't think anyone would notice not having one, just having a informal request for talks
23:11 <+Bart_Massey> We have a CFP ready, let's just send it.
23:12 <marcoz> **Proposals due:** Monday 6 August 2012 17:00 UTC
23:12 <marcoz> lol
23:12 <marcoz> **Notification of acceptance:** Monday 13 August 2012
23:12 <agd5f> maybe also a reminder to sign up for slots
23:12 <agd5f> or no one will be presenting
23:12 <marcoz> What makes sense? I'll change and send out
23:12 <+alanc> set deadline to Aug. 15, i.e. ~ 2 weeks, just before our next meeting & 1 month prior to conference?
23:12 <+Bart_Massey> Works for me
23:13 <agd5f> sounds good
23:13 <marcoz> Aug 15 for deadline,  and Sep 3 for Notification?
23:13 <agd5f> sure
23:13 <+emmes> yes, reasonable. I mean, we don't need much time to validate the entries
23:14 <marcoz> that's 2 weeks to validate.
23:14 <agd5f> also maybe mention travel funding
23:14 <+alanc> still expect most people will just sign up for a talk slot, not formally submit a paper
23:15 <+Bart_Massey> alanc: almost certainly, which is fine. I think this thing is a "Call For Proposals"; we've pretty much abandoned the refereed paper track
23:15 <+Bart_Massey> er Call For Presentations
23:15 <+Bart_Massey> er whatever
23:15  * Bart_Massey is mightily confused today
23:16 <stukreit> Its sad that we're late on this, but we must write copy that encourages preparation and organization of presentations. This has occasionally been an issue.
23:16 <+Bart_Massey> On another topic...
23:16 <+Bart_Massey> Next topic: How are we doing on paying our Delaware taxes
23:17 <stukreit> didn't do it yet. I know I said I would. need to get it done
23:17 <+Bart_Massey> Thanks much!
23:17 <stukreit> yeah...
23:17 <+Bart_Massey> I think those are the topics I had for today. Anyone got anything else?
23:18 <agd5f> nothing here
23:18 <marcoz> any updates on the book sprint?  topics or location?
23:18 <+emmes> location is clear
23:18 <+emmes> but i'm interested about the exact dates...
23:18 <+alanc> attendees confirmed?
23:18 <+alanc> I can make it if we hold it, but if not, I'll go visit our Prague office those two days instead
23:19 <marcoz> exact dates:  Mon and Tues before XDS 
23:19 <marcoz> XDC
23:19 <+emmes> weekend as well or rather not?
23:20 <+Bart_Massey> I think 2 days is good...
23:20 <+Bart_Massey> I'm in, of course
23:20 <marcoz> I hadn't planned on the weekend.
23:20 <marcoz> I think I'll be there anyway (two more days in Deutschland. ;)  )
23:21 <+Bart_Massey> Yeah, I'll visit friends in Berlin before, likely
23:21 <marcoz> http://www.x.org/wiki/Events/BookSprint2012
23:21 <+Bart_Massey> marcoz: How many exactly do we have confirmed right now?
23:21  * marcoz checking
23:22 <agd5f> Unfortunately, I won't be there.  I think Michel and Christian are going to XDS from AMD this year since they are based in Europe
23:23 <+Bart_Massey> agd5f you are definitely eligible to ask the Board for travel money if thats an issue
23:23 <marcoz> Bart, me, and alan sadly are the only confirms I can find 
23:23 <+emmes> just asking because ATM I have the room reserved also over the weekend. will keep it that way, nevertheless, but i might be planing something for the weekend if nobody shows up then ;-)
23:24 <+Bart_Massey> If we can't get to 6 by next meeting, I'd say cancel it.
23:24 <+Bart_Massey> I don't want to watch marcoz and alanc write epically by themselves again: that's harsh :-)
23:24 <marcoz> ouch.  ok. everyone can expect me to bug them 
23:24 <marcoz> ;)
23:24 <+Bart_Massey> Thanks!
23:25 <+Bart_Massey> emmes: I think there's 0 chance we'll want the room for the weekend
23:25 <+Bart_Massey> so unless you want it for something else feel free to let those days go.
23:26 <+Bart_Massey> OK. I think we're done for today. Move to adjourn.
23:26 <stukreit> +1
23:26 <+alanc> +1
23:26 <agd5f> +1
23:26 <marcoz> +1
23:26 <+Bart_Massey> Thanks much all!
23:27 <+emmes> +1
"""]]