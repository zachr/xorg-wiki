
Date is 2012-01-19, times are UTC+01. 
[[!format txt """
23:02 <+alanc> good afternoon
23:02 <+Bart_Massey> Hi all
23:02 <+emmes> hey
23:03 <+Bart_Massey> Looks like I'll be in Wurzburg 2nd or 3rd week of April; don't know if that fits with anybody's schedule. Teaching at FHWS for a week.
23:03 <+alanc> so shall we start with the XDS dates from Egberts mail?
23:04 <egbert> looks like everybody received my email.
23:05 <+emmes> thanks a lot, again, for that tremendously difficult work
23:05 <+alanc> sounds like the choices are the beginning of September without Keith, or that final weekend in September if we want to let him attend 
23:05 <egbert> there are still several unknowns there.
23:06 <+alanc> speaking of which, keithp?  anholt? you here today?
23:06 <egbert> it could be that our best bets would be either early september or over weekends.
23:06 <egbert> either 21-23 or 28-30
23:06 <egbert> i'd now even favor the second.
23:06 <+Bart_Massey> Is there a problem with 9/21-9/23?
23:07 <egbert> there is a chance that we can do it during week 3 but this is an option i still need to wait for feedback.
23:07 <+Bart_Massey> I'm fine with either 9/21-23 or 9/28-30
23:07 <+alanc> oops, sorry, I missed there were two weekends listed there
23:07 <egbert> so let's plan for the worst case scenario.
23:07 <+mherrb> I'd prefer 21-23. I have to be in Switerland 24-28.
23:07 <egbert> could be that i know more by eob tomorrow.
23:08 <+Bart_Massey> Are there issues with 9/21-23 that anyone is aware of?
23:08 <egbert> ok, let's aim for 21-23. 
23:08 <egbert> hi keith.
23:09 <egbert> we are just contemplating about the date for xds.
23:09 <keithp-phone> Eric and i are at lunch
23:09 <egbert> you may have seen my email.
23:09 <keithp-phone>  Yes
23:09 <keithp-phone> Eric slso likes the later date
23:09 <+Bart_Massey> We're going to hold XDS 2012 9/21-9/23 unless someone has a reason this might not work, I think?
23:10 <egbert> currently i'm still waiting for some feedback but until now i'm sure about weekend dates.
23:10 <+alanc> which is the earlier of the later dates
23:11 <egbert> and as i said i'm still waiting for feedback if the room or i can be available during week 3 in september.
23:11 <egbert> but this is less than sure.
23:11 <egbert> point is: there is susecon in exactly this week.
23:12 <+Bart_Massey> I'm not understanding. You say that there is a conflict with 9/21-9/23?
23:12 <egbert> so it looks like we can aim for 21-23 if we cannot get a more favorable date.
23:12 <+alanc> I see, Sept. 18–21, 2012 Caribe Royale Hotel Orlando, FL
23:12 <+Bart_Massey> Ah
23:12 <egbert> right. but 21-23 should be ok.
23:13 <+Bart_Massey> well, 9/28-9/30?
23:13 <egbert> i may not be available earlier in case i'm needed and the room may not be available either.
23:13 <+alanc> that will be tight for flying back to set up for you
23:13 <+alanc> but hopefully your two co-organizers can help with that
23:13 <egbert> if i have to go i don't have to be there all week.
23:14 <egbert> or maybe not at all.
23:14 <egbert> but i need to find out. i also need to find out if the room is needed for some video feed or not.
23:14 <+alanc> okay, so it sounds like the board preference is for the 21-23, but we need to wait on Egbert to find out about availability before deciding for sure
23:14 <egbert> that's the feedback i'm waiting for.
23:15 <+alanc> if necessary, we can probably find a volunteer to spend a week in Florida on your behalf
23:15 <egbert> as said i should know more by eob tomorrow. we should then be able to continue this by email.
23:16 <+alanc> okay, thanks for the update and we look forward to more via e-mail
23:16 <egbert> the other thing i'm also exploring is if we can get decend rates for rooms even during the trade shows when we bring in enough people.
23:17 <egbert> but this is a route i don't really want to go. i'd rather negotiate rates for rooms at the hotel for days with no special events.
23:18 <egbert> ok, so let's aim for 21-23 and i'll keep you posted by email as soon as i know more.
23:18 <egbert> let you go on with your other topics then
23:18 <egbert> good night!
23:19 <+emmes> good night, Egbert
23:20 <egbert> emmes, you know where to find me ;P
23:20  * emmes is scsred
23:21 <+alanc> the other scheduling issue to discuss is the election calendar I also mailed out last night
23:21 <+alanc> so far I've got two "looks good to me" responses and no complaints - shall we move forward with it?
23:21 <stukreit> alanc: +1
23:21 <+mherrb> +1
23:22 <+emmes> +1
23:22 <agd5f> +1
23:22 <+Bart_Massey> sounds like it's official
23:23 <+alanc> okay, will send out the mail next week to kick off the membership drive and post the calendar to the public wiki then
23:24 <+alanc> stukreit, agd5f: anything to say about the 501c3 paperwork that we can discuss here?
23:24 <+alanc> (i.e., non lawyer-privileged bits)
23:25 <agd5f> alanc: nothing new that I know of
23:25 <stukreit> I haven't heard from aaron other than his request for member list a few days ago. Hope he's getting finished up with the doc
23:25 <+emmes> did we had a final idea where to collect data about our publications / lecturing materials / etc?
23:26 <+alanc> so we're on track to make the response deadline?  don't need to poke members to remind them to submit any more?
23:26 <stukreit> A good poke is always helpful ;-)
23:27 <stukreit> Does someone here own the job of making a collected page or 2 on the wiki for Aaron to point to?  I don't have a talent for that.
23:28 <+emmes> i'll be busy correcting student tests for the next few weeks, otherwise I'd do that :-/
23:30 <+mherrb> I'm also quite busy, but I can probably do something during the coming week-end.
23:30 <+alanc> we have the events page on the wiki, but that's probably not very well organized
23:30 <+Bart_Massey> Yeah, did somebody ever point out to Aaron that we have two professors on our Board? :-)
23:30 <stukreit> We need a page devoted to published papers.
23:31 <+Bart_Massey> Maybe somebody could bug marcoz to collect this stuff?
23:31 <+Bart_Massey> He owns the wiki more than anybody, I think...
23:31 <+emmes> papers only, or rather papers and talks? AFAICS we do have more talks than published papers. OTOH papers weight much more.
23:32 <stukreit> well, sure, both, but the page should be clearly devoted to published stuff rather than mixed with conference calendards or whatever
23:32 <+mherrb> The 1st videos on compiz had a huge impact.
23:32 <+alanc> http://lanyrd.com/topics/x-window-system/past/ & http://lanyrd.com/search/?q=X+Window+System&type=coverage also have a bunch of links to past conferences & talks
23:34 <+alanc> if I recall correctly we're trying to show our educational mission with these, not getting people excited about upcoming "product"
23:34 <+mherrb> I think it's good to mention such external links (or phoronix fwiw), it shows that the foundation action is actively followed
23:38 <+alanc> okay, so mherrb will make a "Papers" page in the wiki for that,  anything else we need to get done?
23:39 <+Bart_Massey> I haven't done anything about tracking down GSoC stuff yet.
23:39 <+Bart_Massey> I think what we need to do there is to bug whot to write something, have marcheu pick a couple of recent successful project to highlight, and call it good.
23:40 <+Bart_Massey> We also need to get one of the mentors to write something about our lone EVoC project. :-)
23:41 <+Bart_Massey> Nobody needs to do more than a paragraph or two, but I think it will go a ways toward showing the ed value of these activities.
23:42 <stukreit> bart: can you volunteer to poke them?
23:42 <+Bart_Massey> I will if no one else is willing; my email is really messed up right now, though :-)
23:42 <+Bart_Massey> Maybe marcheu?
23:44 <+Bart_Massey> Maybe not. :-) OK, I'll try to send a few emails this afternoon.
23:45 <stukreit> that would help a lot. thanks!
23:46 <+Bart_Massey> What's the deadline on all this, anyhow? Did Aaron want stuff by tomorrow?
23:47 <stukreit> deadline keeps moving. I'll ping him now
23:50 <stukreit> pinged
23:53 <+alanc> anything else to discuss before adjourning today?
23:55 <+emmes> i'm good
23:58 <+mherrb> me too and feeling sleepy... +1 for adjourning
23:58 <+Bart_Massey> +1
23:58 <marcheu> Bart_Massey: I'm not sure what you mean "pick projects and highlight them"?
23:58 <stukreit> +1 good night to all our sleepy european friends
23:58 <+alanc> okay, see you all in two weeks, on Feb. 2nd then
23:59 <+mherrb> oh I'm already committed to something else on Feb 2, I'll miss the meeting, sorry.
"""]]