
Date is 2012-10-04, times are UTC+02.  
[[!format txt """
23:00 <+Bart_Massey> Hey all
23:00 <agd5f> we having a meeting today?
23:00 <+Bart_Massey> That's my plan.
23:00 <agd5f> cool
23:00 <+Bart_Massey> Assuming we have a quorum.
23:00 <+anholt> moving rooms...
23:00 <+Bart_Massey> k!
23:03 <stukreit> hi
23:03 <+Bart_Massey> Howdy!
23:03 <+Bart_Massey> Should have anholt back shortly, which will make four of us
23:04 <+alanc> hello
23:04 <+Bart_Massey> Greetings!
23:05 <+Bart_Massey> Give a couple more minutes for anholt to return, and we should be able to get started.
23:08 <+Bart_Massey> anholt: ?
23:08 <+Bart_Massey> OK, let's go ahead and get started; he'll show when he shows.
23:08 <+Bart_Massey> My count is myself, agd5f, stukreit and alanc makes 4; did I miss anyone?
23:09 <+alanc> no keithp, emmes, or marcoz?
23:09 <agd5f> so are we going to start on a new two week meeting schedule starting today? i.e., no meeting next week, next meeting the following week?
23:09 <+keithp> alanc: sorry, bisecting X server failure
23:09 <+Bart_Massey> OK, keithp makes 5
23:09 <+Bart_Massey> IDK where marcoz is
23:09 <+keithp> Bart_Massey: anholt has a conflicting meeting on Thursdays...
23:10 <+keithp> sometimes he joins from the middle of that meeting
23:10 <+Bart_Massey> agd5f: I move we start a new two-week schedule today
23:10 <+Bart_Massey> Any objections?
23:10 <stukreit> go for it
23:10 <+Bart_Massey> With no opposition, the motion is carried
23:11 <+Bart_Massey> Next meeting 15 October
23:11 <agd5f> Bart_Massey: 15th is a monday
23:11 <+Bart_Massey> So, I'm going to start running this giant agenda, and we'll get where we get
23:12 <+Bart_Massey> er 18 October :-) :-)
23:12 <+Bart_Massey> cannot add 14 and 4 today, apparently
23:12 <+Bart_Massey> So, first: XDC wrapup. What kinds of things do we need to discuss re XDC?
23:13 <+keithp> how much we enjoyed eating pork?
23:13 <+Bart_Massey> In particular: (1) are there administrative things left that need to get done right away?
23:13 <+Bart_Massey> keithp: Rather not think about the pork
23:13 <+Bart_Massey> Also: (2) Lessons learned, things to do differently next year, etc?
23:13 <+alanc> need to send thanks to egbert, emmes, & libv
23:14 <+keithp> Yeah, I can't think of a more professionally run XDC.
23:14 <+keithp> it was fabulous
23:14 <+Bart_Massey> alanc: Absolutely. I'll work on that letter today. Thanks for the reminder!
23:14 <+Bart_Massey> There was a strong suggestion that we do better/different publicity for the event next time.
23:15 <+alanc> need to get egbert, etal to put together some notes for the wiki on things that worked, like getting mobile phone numbers for all presenters, getting the video recording setup so well, etc.
23:15 <+Bart_Massey> In particular, that we get as much agenda posted as early as possible; make sure the previous meeting was well-documented, etc.
23:15 <+alanc> yeah, Michael from LWN had a lot of feedback on publishing agenda earlier to help people decide if they should come
23:16 <+Bart_Massey> Of course one of the fundamental XDC conflicts I don't have a strong opinion on is how much this is an outreach vs "inreach" event
23:16 <+alanc> guess we're used to everyone just knowing they'll come no matter what we talk about, but that may limit attendance to those who are willing to come no matter what
23:16 <+Bart_Massey> We talked about maybe doing a federated freedesktop-ish event with XDC next year, as more of an outreach thing
23:17 <+alanc> like the old DesktopCon in Ottawa were supposed to be?
23:18 <+Bart_Massey> alanc: Yeah, that
23:19 <+Bart_Massey> Sounds like there's not so much feedback there, but wanted to get this stuff on the record to remind us later (if we could ever be bothered to read our meeting logs)
23:19 <+Bart_Massey> Are we done with TOPIC:XDC, or is there more to discuss?
23:20 <+Bart_Massey> OK, check.
23:20 <+Bart_Massey> Next topic: election status. Are people starting to work on that?
23:20 <stukreit> I want to say thanks to the sponsered folks for getting their paperwork in. At this time, all checks have been paid, including direct payment for the people staying at Azimut
23:20 <+Bart_Massey> stukreit: Everybody but me. I suck. My apologies. Working on it today.
23:21 <+anholt> Bart_Massey: no progress other than what happened at xdc
23:21 <+Bart_Massey> OK! Just going to keep bringing it up every meeting on the theory that it might be helpful :-). Any blocking issues there?
23:21 <stukreit> bart: don't be so sucky. get the receipts. You at least can get reliable usps mail and no international exchange foolery
23:22 <+Bart_Massey> stukreit: I was thinking I'd just email you stuff. RSN.
23:22 <stukreit> Scan it, muy better
23:22 <+Bart_Massey> 'k.
23:23 <+Bart_Massey> Next topic: infrastructure stuff. Are there things the Board should be doing here, or should we just wait for keithp and jrayhawk to magically get things under control, or what?
23:23 <+anholt> I need to write up a proposed schedule, but in the past our schedule has basically started in january.
23:24 <+Bart_Massey> I think we can agree that using alanc as a human spam filter for the wiki is no good, and having our web presence crashed half the time is no good, etc.
23:24 <+alanc> spam rate on fd.o wiki has dropped since the latest admin tweaks, still coming in (though slower) on x.org wiki
23:24 <+Bart_Massey> anholt: I think we've started in January after repeated failures in October/November/December to move things forward :-)
23:25 <agd5f> fall has historically been fail for elections.  do I bylaws require any particular time period?
23:25 <agd5f> *our
23:25 <+Bart_Massey> agd5f: I'm not sure what the bylaws actually require; it's kind of ambiguous
23:25 <+anholt> yeah, any intersection between elections and end of year was disaster.  if we're not restricted on scheduling, I'd like to start in jan.
23:25 <+Bart_Massey> I'm fine with starting in January, but let's make sure everything is in place to just push the button in January and get it done.
23:25 <+Bart_Massey> So I'll quit badgering about elections for now.
23:26 <agd5f> jan works for me too
23:26 <+Bart_Massey> kk
23:26 <+Bart_Massey> So infrastructure wise, is there anything the Board should be doing this week or next that it's not doing right now?
23:27 <+Bart_Massey> I hear nothing
23:27 <+alanc> not that I can think of
23:27 <+Bart_Massey> Since marcoz doesn't seem to be here, I think we'll table EVoC-related topics until next meeting. Objections?
23:28 <agd5f> the dri wiki uses capcha and doesn't seem to have as much spam
23:28 <agd5f> although I think that wiki may have limited edit group
23:28 <+Bart_Massey> I've had really poor results with captcha on my personal wiki; apparently people are being paid to solve them AFAICT.
23:29 <+Bart_Massey> Anyhow, we'll bug the fd.o sysadmins about a grand technology plan for spam mgmt I guess.
23:29 <+Bart_Massey> They're pretty smart.
23:30 <+Bart_Massey> Next topic: Wayland. Specifically, IMHO it's time the Board developed an official, well-publicized position about Wayland. We've talked about this before.
23:30 <+Bart_Massey> I think at the least we should identify Wayland as part of the X.Org Foundation purview.
23:30 <+alanc> can we just declare Wayland to be the code name for X12?   8-)
23:30 <+Bart_Massey> alanc: de facto, I think that's exactly what we should do :-)
23:32 <+Bart_Massey> I would like to see somebody on the Board (who is not me) produce a wiki page we can point to that describes Wayland for outsiders, describes the relationship(s) between Wayland and X, and declares that the Board wants to support Wayland in the same way that it's supporting Boston...er, X11
23:32 <+anholt> as far as I know, krh still plans on not ever dealing with making wayland able to replace the x desktop.
23:32  * Bart_Massey wishes he had said "Cambridge"
23:32 <+anholt> it's all about embedded these days
23:32 <+alanc> but yes, I think it would be good when we revise our bylaws to change the Purpose section in the preamble to state that our role is for the entire FOSS graphics stack, including the traditional X Window System plus Mesa, Wayland, DRI
23:33 <+Bart_Massey> alanc: Yeah, what you said.
23:33 <stukreit> +1
23:33 <agd5f> +1
23:33 <agd5f> I guess we should compile a list of bylaw changes for the election
23:33 <+Bart_Massey> But even before the Bylaws revision I think we can publically say this.
23:34 <+alanc> yes, we've informally said it on a number of occasions, but should write it down
23:34 <+Bart_Massey> Except that if we're going to claim to support Mesa we should probably talk to Brian and the other Mesa leads first :-).
23:34 <+Bart_Massey> Does somebody on the Board want to take each of these two tasks?
23:35 <+Bart_Massey> (1) Talk to Brian about X.Org Foundation support of Mesa
23:35 <+Bart_Massey> (2) Write a page saying what alanc said in more detail
23:35 <agd5f> I'll take (2)
23:35 <+Bart_Massey> Thank you!
23:35 <stukreit> agd5f: I can help edit
23:36 <agd5f> stukreit: thanks
23:36 <+Bart_Massey> I don't know the Mesa folks at all, so while I'd be happy to take (1) I'd want some help from someone who does.
23:36 <+Bart_Massey> Should I try to draft idr?
23:36 <agd5f> half of us are mesa folks.  Can probably just email mesa-dev
23:37 <+Bart_Massey> agd5f: If you could do this also, I'd be forever grateful. Use my name freely if it's at all helpful, which I doubt.
23:38 <+Bart_Massey> If you want me to produce a draft or read one, I'd be good with that also.
23:38 <+Bart_Massey> :-) :-)
23:38 <agd5f> Bart_Massey: I'm not entirely sure we need to do it since we aren't claiming ownership or anything, mostly just providing help, but I guess it's a nice courtesy
23:39 <+Bart_Massey> agd5f: Exactly. And a genuine offer, I think, to help do more if they want it.
23:39 <agd5f> fine.  I'll take (1) as well
23:39 <+Bart_Massey> Thanks hugely. Like I say, LMK how I can help.
23:40 <+Bart_Massey> OK, one more topic, since we have a couple more minutes.
23:40 <+Bart_Massey> I'm looking right now at about 5 different "logo design contest" sites.
23:40 <+Bart_Massey> Is there consensus that it's time for a new, officially-trademarked X.Org logo?
23:40 <+Bart_Massey> If so, how much are we willing to pay for it, and shall I set the contest up this week?
23:42 <+Bart_Massey> Anybody?
23:42 <agd5f> I don't have a strong opinion either way
23:43 <+Bart_Massey> Alright, I guess I'll take that one to email or we'll try again next mtg
23:43 <stukreit> bart: was it you or someone else who raised this issue?
23:43 <+Bart_Massey> stukreit: It has been raised several times in the last year or two.
23:43 <+Bart_Massey> But there was a lot of talk of it at XDC
23:43 <stukreit> please refresh me on 2 or 3 reasons why its important
23:44 <+Bart_Massey> The recent "Messenger-X" thing highlighted the fact that we currently have no logo that is trademarked or even trademarkable
23:44 <+Bart_Massey> The current logo doesn't reproduce well, doesn't look good on t-shirts and the like, is terrible at small sizes, and is generally disliked
23:45 <+Bart_Massey> We have a new organization now, and might want to differentiate ourselves
23:45 <stukreit> maybe in sync with the wayland shift we can do a bit of name change, like call it Xorg and nothing else
23:46 <+Bart_Massey> I think using Xorg as a nickname is fine; do keep in mind that we are officially the X.Org Foundation, so we have to be a bit careful in official docs, but I don't think that matters for most purposes
23:47 <+Bart_Massey> Anyway, it looks to me like the meeting's about played out. Move to adjourn...
23:47 <agd5f> +1
23:47 <+keithp> yeah, seconded.
23:47 <stukreit> +1
23:47 <+alanc> +1
"""]]