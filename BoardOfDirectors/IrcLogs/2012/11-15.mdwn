
Date is 2012-11-15, times are UTC+01. The log had to to be altered, because htt.p links are not allowed due to antispam rules. \ 
[[!format txt """
23:00 <agd5f> hello
23:00 <+alanc> good afternoon
23:00 <stukreit> hi
23:01 <stukreit> la la la
23:04 <marcoz> hi, sorry Im' late.  meeting.
23:04 <+alanc> it's okay, you still beat Bart, so we haven't really started yet
23:05 <marcoz> It's not official until Bart's here. :)
23:14 <+alanc> well he said he had a short agenda
23:14 <+alanc> I don't remember anything specific to talk about off hand
23:15 <marcoz> any wiki news?
23:16 <+alanc> spam rate still running at zero due to the ban on "htt.p"
23:16 <+alanc> every time I lift that it returns, so I put it back
23:17 <+alanc> haven't heard from fd.o admins on replacement plans
23:17 <+alanc> I think keithp was talking to jrayhawk about that
23:17 <marcoz> ikiwiki ?
23:18 <+Bart_Massey> aiiee. I was programming and totally lost track of time. My sincere apologies.
23:19 <+Bart_Massey> Did I miss everything and everybody?
23:19 <+alanc> we didn't really have much to talk about
23:19 <+alanc> mostly been sitting around waiting 8-)
23:20 <+alanc> agd5f, stukreit, marcoz & I are actually here
23:20 <+Bart_Massey> Apologies. I went out of my way to be at my desk by meeting time :-(.
23:21 <+Bart_Massey> Like I said in my email, I don't have too much business anyhow. 
23:21 <+Bart_Massey> Some stuff has come up, and I do not yet have the logo thing started.
23:21 <+Bart_Massey> Nor the first book ready to go.
23:21 <+Bart_Massey> But both things are on my stack.
23:21 <+Bart_Massey> I believe other things on our stack are: bylaws revision, fundraising, elections, XDC 2013
23:22 <+Bart_Massey> Has anyone heard any other proposals for XDC 2013?
23:22 <+alanc> I haven't
23:23 <+Bart_Massey> Then, if we have a quorum, I'd like to move we officially set up XDC 2013 for PDX and announce this somewhere.
23:24 <+Bart_Massey> Or maybe choose a date first and then announce; let's try to get that done before next mtg.
23:24 <+alanc> who is the organizing commitee?  you, keithp & idr?
23:25 <+Bart_Massey> I think so.
23:25 <+Bart_Massey> We may be able to draft cworth and/or anholt also.
23:26 <+alanc> okay, I'm happy to vote to officially appoint you folks as the XDC 2013 committee for Portland, and let you find a venue/date before making the official announcement to the world
23:26 <+Bart_Massey> +1
23:26 <marcoz> +1
23:27 <+alanc> +1 if that wasn't already clear
23:27 <stukreit> +1
23:28 <agd5f> +1
23:28 <+Bart_Massey> motion carries. thanks all.
23:29 <+Bart_Massey> i will bug keithp as soon as he gets back from his latest travel adventure and we will get the ball rolling
23:29 <+Bart_Massey> any other urgent business?
23:30 <stukreit> none here
23:30 <+alanc> I don't have any
23:31 <+Bart_Massey> alright; move to adjourn. thanks all
23:31 <stukreit> +1
23:31 <agd5f> +1
23:32 <+alanc> +1
23:34 <marcoz> +1
"""]]