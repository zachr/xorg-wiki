
Date is 2012-11-01, times are UTC+01. The log had to to be altered, because htt.p links are not allowed due to antispam rules. 
[[!format txt """
22:04 <+Bart_Massey> Hey all. Sorry to be a bit late.
22:05 <+alanc> we'd just gotten as far as all saying hello
22:06 <+Bart_Massey> Cool. 
22:06 <+Bart_Massey> OK. Status reports. 
22:06 <+Bart_Massey> marcoz: Any EVoC news?
22:06 <+alanc> agd5f, alanc, marcoz, anholt, & stukreit at least - no word yet from emmes or keithp
22:07 <marcoz> nothing new.    One or two new people pinged me but haven't heard back from them
22:07 <+Bart_Massey> On the infrastructure front, keithp and I are moving forward with plans. We will let you know when we've accomplished anything.
22:07 <+keithp> oh, I'm here, just being boring
22:08 <+Bart_Massey> On the book front, I need to get the NDG guide someplace where people can help finish it. Hopefully this week...
22:08 <+Bart_Massey> For the logo contest, I am just finishing up the wording I want to use. I'll run it by you all when I'm ready to go.
22:08 <marcoz> also,  Sent Supreet that certificate. He ack'd but hasn't replied since.
22:08 <+Bart_Massey> marcoz: Thanks
22:08 <marcoz> Bart_Massey: yw
22:08 <+alanc> marcheu did merge in the changes to the driver guide to his git repo as well
22:09 <+Bart_Massey> What is our current tax/fee status? Is there work to be done there?
22:09 <+Bart_Massey> Did someone post the CFP for XDC 2013 to the lists? Have we heard anything?
22:10 <agd5f> I posted an RFP for locations/plans, but no response
22:11 <+Bart_Massey> agd5f: Thanks. We'll give it another week or two, and if we hear nothing we'll assume PDX. I'd like to get it settled ASAP.
22:11 <agd5f> sounds good
22:12 <+Bart_Massey> A long time ago we started to go over the Bylaws, but I'm not sure we ended up with anything like an actual list of proposed amendments.
22:12 <+Bart_Massey> I'd be happy to work with someone on this, but I'm a bit swamped to do it myself...
22:12 <stukreit> I paid a fee required by nsaid (spelling?) a few weeks ago. That's all I know of.
22:13 <agd5f> did we ever pay the delaware franchise fee?
22:13 <stukreit> bart: I owe you a check.
22:13 <+Bart_Massey> stukreit: absolutely no rush
22:13 <stukreit> agd5f: I think that's what nsaid took care of.
22:13 <agd5f> ah, ok
22:13 <+Bart_Massey> Who is NSAID? 
22:13 <stukreit> some kind of broker for fees. I'll dig around here and see if I can find the form
22:14 <stukreit> (my office is in boxes atm)
22:14 <+Bart_Massey> np. sounds like things are under control.
22:14 <+Bart_Massey> So: volunteers for bylaws work, or am I stuck with it? :-)
22:15 <+Bart_Massey> Oh, did anyone ever hear back from the Mesa folks etc re our desire to announce explicit support of their work?
22:15 <agd5f> no response
22:15 <+Bart_Massey> Huh. Should we try again, or what?
22:16 <agd5f> I think everyone already assumes we support mesa and related projects
22:16 <+Bart_Massey> I don't think most people have much idea what we do. :-)
22:16 <agd5f> since most of our evoc and gsoc are non-X of late
22:16 <marcoz> :q
22:16 <+Bart_Massey> Anyway, I'm fine with just going ahead and listing them and waiting for them to object, unless people think otherwise...
22:16 <+keithp> wfm
22:17 <+Bart_Massey> keithp: wfm?
22:17 <+keithp> works for me
22:17 <+alanc> +1
22:17 <+Bart_Massey> k
22:17 <agd5f> +1
22:17 <+Bart_Massey> alanc: Didn't you draft some kind of statement already?
22:17 <+Bart_Massey> Or do I need to do this?
22:17 <marcoz> +1
22:18 <agd5f> Bart_Massey: I updated some language on the wiki
22:18 <+Bart_Massey> Ah, that must be what I'm thinking of. Thanks
22:18 <+alanc> yeah, was agd5f not me
22:19 <+Bart_Massey> OK, I'm out of current business. Anyone else got anything?
22:19 <stukreit> so what's the ptr to the book git repo?
22:20 <+Bart_Massey> stukreit: Old book or new?
22:20 <stukreit> marceau's
22:20 <+Bart_Massey> IDK offhand. Anyone got it handy?
22:20 <agd5f> htt.p://cgit.freedesktop.org/~marcheu/lgd/
22:20 <stukreit> the driver guide you referred to earler
22:20 <stukreit> Thanks!
22:21 <+Bart_Massey> Move to adjourn.
22:21 <agd5f> seconded
22:21 <stukreit> +1
22:21 <marcoz> +1
22:21 <+anholt> +1
22:21 <+Bart_Massey> Thanks all! See you in two weeks!
22:21 <+alanc> +1
22:21 <+keithp> enjoy!
"""]]