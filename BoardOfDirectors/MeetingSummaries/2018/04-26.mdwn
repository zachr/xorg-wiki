Summary of the 26 April 2018 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/]]

## Attendees

Present:
Martin Peres,
Daniel Vetter,
Bryce Harrington,
Keith Packard,
Rob Clark,
Taylor Campbell,
Eric Anholt,
Harry Wentland,

Absent:


## Summary

Agenda: elections/ramp-up, treasurer's report, gsoc

Huge thanks to our outgoing board members Alex Deucher and Egbert Eich!

Congrats to re-elected and new board members Bryce Harrington, Eric Anholt,
Keith Packard and Harray Wentland!

And big thanks to Rob for running the election tech.

Board approves FY2017 treasurer's report.

Gsoc: 2 students approved!

## Items discussed

[[!format txt """
ITEM: New boad members
We're trying to document board roles a bit better at https://www.x.org/wiki/BoardOfDirectors/Duties
ACTIONS: Eric/Anholt: make sure you have access to gabe.fd.o and all that stuff.
ACTIONS: Martin to put gsoc details there.
ACTIONS: Bryce to hand out gpg details
STATUS: Pending

ITEM: treasurer's report
Got the reports from SPI now, unfortunately only monthly lump sums. Good enough
for the report, not good enough yet for handling sponsors.
Board approves report.
ACTIONS: Daniel files a ticket with SPI to get full details.
ACTIONS: Bryce to follow up on transaction details too.
ACTIONS: Bryce to send out report to members@.
STATUS: Pending

ITEM: Gsoc
2 students approved. Inconclusive discussion on why it's again much less than
historically.
STATUS: Pending
"""]]
