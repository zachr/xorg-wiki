Summary of the 02 August 2018 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/]]

## Attendees

Present:
Harry Wentland,

Absent:
Martin Peres,
Daniel Vetter,
Bryce Harrington,
Keith Packard,
Rob Clark,
Taylor Campbell,
Eric Anholt,


## Summary

Agenda: xdc lunch/coffee break, xdc travel, gsoc, evoc, xdc19, fd.o

## Items discussed
Looks like we're deep in the summer lull so no meeting today.


"""]]
