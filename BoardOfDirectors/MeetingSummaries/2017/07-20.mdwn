Summary of the 20 Juli 2017 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/]]

## Attendees

Present:
Martin Peres,
Daniel Vetter,
Taylor Campbell,
Bryce Harrington,

Absent:

Alex Deucher,
Egbert Eich,
Keith Packard,
Rob Clark,

## Summary

Agenda: Agenda: travel requests, gsoc, xdc sponsorship, xdc18, khronos, coc

Gsoc on track, XDC17 nearing (get your talks in), still looking for XDC18 proposals.

## Items discussed

[[!format txt """
ITEM: Travel sponsoring
Board approves travel request form Laura and Jake.
STATUS: Pending

ITEM: Gsoc
2/3 evaluation coming up, everything on track
STATUS: Pending

ITEM: khronos agreement
Nothing new
ACTIONS: Martin to poke again
STATUS: Pending
"""]]
