Summary of the 26 Oktober 2017 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/]]

## Attendees

Present:
Martin Peres,
Daniel Vetter,
Egbert Eich,
Keith Packard,
Taylor Campbell,
Alex Deucher,

Absent:

Rob Clark,
Bryce Harrington,

## Summary

Agenda: XDC18 sponsoring, gsoc, travel payments, evoc

Most topics tabled for the next mtg in 2 weeks.

## Items discussed

[[!format txt """
ITEM: gsoc
Need to get successful students to conferences.
ACTIONS: Martin still to ping successful students to submit talks for fosdem
(and travel sponsoring requests to the board if needed).
STATUS: Pending
"""]]
