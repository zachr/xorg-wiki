Summary of the 03 Septmber 2015 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/2015/]]

## Attendees

Present:

* Peter Hutterer, Rob Clark, Martin Peres, Alex Deucher, Daniel Vetter, Matt Dew, Egbert Eich, Keith Packard

Absent:

*


## Summary

GSoC has finished, EVoC has really started. 3 approvals for travel
assistance.

## Items discussed

[[!format txt """
ITEM: GSoC
Pencils-down deadline is past, waiting for students to write the final blog
post, then Martin P can accumulate them and summarise them. We should get
org payments from Google too.
ACTIONS: Martin P to do the writeup blog post
STATUS: Pending

ITEM: XDC Travel assistance
Travel funding approved for Varad Gautam, Samuel Pitoiset and Stephen
Chandler Paul. [Chandler since declined]
ACTIONS: Review applications as they come in
STATUS: Pending

ITEM: EVoC
Juliet sent the first couple patches to the list, and confirmed the test
amount we transferred came through. So we approved the initial
EVoC payment, Stuart K transfered it.
ACTIONS: None
STATUS: Pending

ITEM: expo.x.org
Still in the same state, waiting for sendmail configuration to be sorted.
Configuration is largely there, the pieces just need to be connected.
ACTIONS: Keith P to finish configuration
STATUS: Pending
"""]]

