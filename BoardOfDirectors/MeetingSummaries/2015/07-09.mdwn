Summary of the 09 July 2015 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/2015/]]

## Attendees

Present:

* Peter Hutterer, Egbert Eich, Rob Clark, Martin Peres, Alex Deucher, Keith Packard, Daniel Vetter

Absent:

* Egbert Eich, Matt Dew


## Summary

EVoC proposal to port Glean tests to the piglit framework (re)approved.
Migration of expo still ongoing, GSoC students successfully passed midterms.

## Items discussed

[[!format txt """
ITEM: expo.x.org
openshift test installation, still sorting out email routing. We'll try for
an authenticated smtp relay through fdo. the two mailing lists (board@ and
members@) will be transferred to be hosted on fdo as well.
ACTIONS: Rob C and Keith P to coordinate with the fdo admins
STATUS: Pending

ITEM: EVoC
Brian P is happy to mentor Juliet for her proposal, porting
Glean tests to the piglit framework. This project was already approved
pending mentor acceptance in May but the original mentor was unavailable for
the timeframe.
ACTIONS: Matt D to notify Juliet and Brian
STATUS: Complete

ITEM: GSoC
Midterm evaluation time, all our students have passed.
ACTIONS: None
STATUS: Complete
"""]]



