
Summary of the 8 November 2010 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]] 

Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/2010/11-08|BoardOfDirectors/IrcLogs/2010/11-08]]. 


### Board Member Attendance

* **Present**: Eric Anholt, Donnie Berkholz, Alan Coopersmith, Alex Deucher, Matthias Hopf, Bart Massey 
* **Absent**:  Matthieu Herrb, Keith Packard 

### Reports, Discussion & Decisions


#### Google Code-In

X.Org was not one of the projects Google selected for the Code-In program - they accepted less than 1/7th the number from the recent Summer of Code. 


#### Treasurer Status

Stuart is working on gathering details for outstanding checks, and is sending the check for the Delaware Corporate Franchise fee to Keith for cosigning. 


#### Election Committee

Alex asked the election committee members to respond to his proposed election schedule e-mail. 
