

[[!format txt """
Notes from X.Org BoD telecon July 14, 2006

Present:
Stuart Kreitman (SK)
Stuart Anderson (SA)
Jim Gettys (JG)
Kevin Martin (KM)
Leon Shiman (LS)
Egbert Eich (EE)

Topics
------
1. Bylaw Subcommittee Report
2. Planning an F2F Meeting this Year
3. Board Elections
4. Status Incorporation Documents / other unfinished Business
5. Status of Server at MIT

1. Bylaw Subcommittee Report
   The bylaw subcommittee has received feedback from the laywer.
   The feedback was discussed issues that the subcommittee felt
   it was unable to solve itself were posted back to the laywer
   with questions and comments.

2. Planning of F2F Meetings this Year
   There will be no f2f meeting of the board at OLS as not all members
   will be there. The next phone meeting will be scheduled for Jul, 25th.

3. Board Elections
   The election process will not start until after the summer (ie. middle
   of August). Election subcommittee will meet in Ottawa to discuss
   schedule details.
   JG is presently looking into the election software that is used by
   the Gnome Foundation.

4. Status Incorporation Documents / other unfinished Business
   LS hopes to be able to have the Incorporation Documents
   which include the non profit status documents and the paperwork
   for the IRS filings ready for the next meeting.
   He will also refile the applications to give SK counter signature
   authority for money transfers with our bank.
   Recent difficulties with money transfers were discussed. LS assured
   the board that the procedures the bank applied for money transfers
   were reasonable and were not the cause of the delays.

5. Status of Server at MIT
   The status of the servers at MIT were discussed.
   SA would like to reinstall expo. The current setup is suboptimal
   which caused the delay in email delivery to the board when mailman
   died due to a full root fs.
   The SUN machines have not been mounted in the racks, yet. Adam Jackson
   who volunteered to do this together with LS was busy with other things.
   SK asks to run OpenSolaris on the SUN machines. He will take over the 
   responsibility to administrate these machines.
   
"""]]