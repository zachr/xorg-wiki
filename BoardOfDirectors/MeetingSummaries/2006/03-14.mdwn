

[[!format txt """
Notes form X.Org BoD telecon March 14, 2006

Present:
Stuart Kreitman (SK)
Stuart Anderson (SA)
Jim McQuillan (JQ)
Kevin Martin (KM)
Leon Shiman (LS)
Egbert Eich (EE)

Topics
------
1. Recreation of Banner for booth.
2. Approval of minutes and notes for publication.
3. Further discussion on bylaw draft.

1. Recreation of Banner
   The main X.Org banner with logo for the LWE booth got lost and
   therefore needs to be recreated.
   The following suggestions were put on the table:
   - Remove dated information (possibly candidates
     lists of technologies, sponsors)
   - Minimise cost of graphics work required, make it reusable.
   LS will have the graphics work done and propose a new design to the
   board. Board authorized funds.
   The cost are estimated at USD 150 for the graphics work and
   USD 600 for the printing.

2. Approval of minutes
   Notes for publication approved immediately, minutes approved if
   no objections within 24h.

3. Bylaws
   Bylaw committee pointed out that closure on bylaws is prerequisite
   for upcoming BoD election. Any delay will delay election thus because
   of the tightness of schedule cooperation of board members is required:
   Comments on bylaws not mentioned during this meeting are due by end
   of day Wednesday (March 15, 2006).
   Continued discussion of bylaws: - correction of numerous typos,
   ambiguous and unclear wordings and phrases were discussed changes
   were suggested or left for discussion by the bylaws committee.
   Further issues to be included into the bylaws were mentioned.
   Consensus on most issues was reached. Remaining issues are pending
   further discussion by email.
   LS agreed to send in his comments and suggestions.
"""]]