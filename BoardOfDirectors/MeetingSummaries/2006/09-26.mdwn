

[[!format txt """
Notes from X.Org BoD telecon September 26, 2006

Present:
========
Leon Shiman (LS)
Kevin Martin (KM)
Stuart Kreitman (SK)
Stuart Anderson (SA)
Jim McQillan (JQ)
Egbert Eich (EE)

Topics:
=======
1. Membership Applications
2. Status of IRS Filings and Incorporation
3. Debate over LS's Request for Travel Funding for LWE, Colone, Germany.

1. Membership Applications
   The new system for Membership Renewal and Application has been set up.
   Requests for Renewal as well as new applications are coming in. Debate
   sparked on the issue which criteria to apply to new applications. To 
   meet the requirements for non profit the organization may not be able 
   to apply too strict rules. 
   It was pointed out that it should be left to the Committee which decides
   over the applications to determine the policies and reevaluate them from
   time to time.
   It was decided that the Board Members who are up for reelection this
   year should not participate in the decisions over the acceptance or
   rejection of applications.

2. Status of IRS Filings and Incorporation
   LS waiting for the legal counsel to finish the work and get back to him.

3. Debate over LS's Request for Travel Funding for LWE, Colone, Germany.
   LS had requested USD 500 to cover the costs for his accomodation at 
   Colone.
   Objections were raised: X.Org did not have a sponsorship program.
   Without that Board Members were at an advantage when applying for 
   sponsorship as they knew about this opportunity and had direct 
   access to the Board.
   EE proposed to instead set aside a fund of USD 1000 for travel 
   sponsorship and advertise the sponsorship opportunity to the 
   community. Furthermore to bring in as many people as possible 
   the focus of funding should be on the local community.
   LS pointed out that LWE had approached him about the booth but time 
   to accept the offer was running out.  Therefore there wasn't enough
   time to wait for the Board to decide over the distribution of the 
   funds.
   No resolution was reached.
   SK will submit an alternative motion to the BoD mailing list which
   would allow to award USD 500 to LS in advance distributing the rest
   to other applicants.
"""]]