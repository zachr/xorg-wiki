

[[!format txt """
Notes from X.Org BoD telecon September 5, 2006

Present:
========
Leon Shiman (LS)
Kevin Martin (KM)
Stuart Kreitman (SK)
Jim McQillan (JQ)
Egbert Eich (EE)

Topics:
=======

1. Public By-Law Review
2. X.Org Trademark Issues
3. Membership Management

1. Public By-Law Review
   Resonance on the public bylaw review has not been very big.
   One issue broad up was that the new bylaws have a one minimum
   year membership requirement for people who whould like to run
   for the board. People found this not very realistic as they feel
   that many individuals who have been active in the community may not
   bother to become members until shortly before the election.
   One possible solution may be to 
   * require membership only at the time of the election
   * base eligibility on active participation (that would qualify 
   for membership)
   * reduce the period to 6 month.
   KM will draft and publish an updated wording.

2. X.Org Trademark Issues
   The discussion on this topic continued.

3. Membership Management
   SA has added some more pieces to the membership management 
   infrastructure.
   The board would like to express its appreciation for his effords.

"""]]