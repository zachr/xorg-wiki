

[[!format txt """
Summary of the BoD meeting Jan 10, 2006:

Present Stuart Kreitman, Jim McQuillan, Kevin Martin, Stuart Anderson,
        Leon Shiman, Egbert Eich

Topics discussed:
- Management
  * note taking
  * system administration
- Budget

Details:
* Note taking: we will continue to collect notes taken by board
  members.
  + Leon offered to create edited notes.
  + Egbert offered to create a summary from notes he took.
  Use these as a basis for decision on the format for publication
* System administration
  + The question was raised if the organization needs somebody hired to
    do system administration and possibly share this person with other
    OS projects (idea from last year's XDevConf)
  + Stuart Anderson offerd to continue to do this for us pro bono
    which would require that he is allowed to do it his way without
    major interference from others to keep the burdeon on him low.
  + For backup and emergency he will give the root passwords to other
    board members.
  + Stuart would also be happy to take requests to improve things like the
    web site according to ideas from our community provided he is
    given the time to look around for the best existing software solution.
* Budget
  + Discussion form previous week was continued and closed.
  + People agreed that we don't have to make provisions for paid management
    tasks as yet as current proposal leaves enough room to add these as
    discretionary items.
  + Egbert Eich agreed to provide insight into the data that he used for
    his estimates.
  + He will also turn the budget into a publishable form adding the minor
    modifications discussed and circulate this to the board for apporval.
  + A consensus exists that we need to work with the community to see
    which events interesting for X.Org exist that we can use our funds for.

"""]]