Summary of the 10 November 2016 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]]
Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/]]

## Attendees

Present:
Peter Hutterer,
Martin Peres,
Daniel Vetter,
Bryce Harrington,
Rob Clark,
Keith Packard,

Guests: Brian Paul, Ian Rommanick

Absent:

Alex Deucher,
Egbert Eich,

## Summary

Agenda: spi transition, khronos adoption agreement, membership agreement

Board decided to move forward with becoming a khronos adopter for the mesa
project.

Next meeting: Election season will start soonish, need to prep.

## Items discussed

[[!format txt """
ITEM: khronos adoption agreement
Discussions on the general plan. Agreement from Brian&Ian as mesa
representatives that we should move forward.
ACTIONS: Martin to follow up with Neil on what exactly "internal" use implies
(since "internal" is a bit nebulous with open source projects).
ACTIONS: Daniel to ask formally ask SPI to look at the Adopters agreement.
STATUS: Pending

ITEM: spi transition
No udpates this week, waiting for Stuart currently.
ACTIONS: Bryce keeps chasing stuff.
STATUS: Pending

ITEM: membership agreement
A new member noticed that the membership agreement is massively outdated
(references bylaws from 2003 and the old LLC which was shuttered 10 years ago).
Need to update this, but there's not even sources for the pdf around. The
version linked from https://members.x.org/join.php is at
https://members.x.org/docs/MemberAgreement02Apr2004.pdf
ACTIONS: Rob to try to find sources and if not, transcribe in latex into a new
git repo.
STATUS: Pending
"""]]
