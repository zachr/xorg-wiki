## Attendees

Present:
Matt Dew,
Peter Hutterer,
Alex Deucher,
Egbert Eich,
Keith Packard,
Rob Clark,
Martin Peres,

Absent:
Daniel Vetter

[Peter had to deal with other business in parallel while the meeting went on,
so Egbert stepped in doing the minutes]

## Summary

Agenda: BoD Election, Expo Replacement, DNS GLUE Records, XDC, GSOC, GSOC/Evoc Payments

DNS resolution has been fixed temporarily so x.org is resolved, but final solution
still pending.

## Items discussed

[[!format txt """
ITEM: BoD Election
ACTIONS: Peter: Emails were sent out, reminder for nomination period were sent
STATUS: Ongoing

ITEM: Expo Replacement
ACTIONS: Rob Clark: Everything was moved over, some issues with MoinMoin still to be
         looked at. Will probably not be able to work on it until getting back from
         the Linaro conference.
STATUS:  Ongoing

ITEM: DNS Glue Records
ACTIONS: Egbert: There was some confusion about the correct addresses communicated
         to Leon Shiman as Admin, fixed now, correctness confirmed by Daniel Stone
         last Sunday. However until the meeting, records were not resolved correctly.
         Oddly, Google resolves them correctly.
ACTION ITEMS: Egbert to check again the next day and get in touch with Leon again.
STATUS: Pending

ITEM: XDC
ACTIONS: Martin: Not much new. Next thing to do: find hotel recommendation and
         plan social event (for Saturday) so that people can start booking their trips.
ACTION ITEM: Martin to check for hotels and social event
STATUS: Pending

ITEM: GSOC
ACTIONS: Martin: x.org got accepted as an org, wayland did as well. Wayland, as a
         new org, is limited to 2 slots. Encourage people to apply to both orgs.
         Possible, but each applicant can only one project accepted at the end.
         Waiting for students to show up.
STATUS:  Ongoing

ITEM:    GSOC/EVOC Payments
ACTIONS: No news.
STATUS:  Pending

"""]]
