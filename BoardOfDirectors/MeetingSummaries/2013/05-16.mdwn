
Summary of the 16 May 2013 meeting of the [[X.Org Foundation|XorgFoundation]] [[Board of Directors|BoardOfDirectors]] 

Full IRC meeting logs are posted at [[BoardOfDirectors/IrcLogs/2013/05-16|BoardOfDirectors/IrcLogs/2013/05-16]]. 


## Attendees

Present: 

         * Alan Coopersmith, Martin Peres, Peter Hutterer, Stuart Kreitmann, Matt Dew, Keith Packard 
Absent: 

         * Matthias Hopf, Alex Deucher 

## Summary

XDC venue has a contract pending, needs the paperwork and first round of finances done to finalize it. 


## Items discussed


[[!format txt """
ITEM: Updates from XDC 1/2
PSU venue is confirmed for Sep 23-25 (Mon - Wed), needs a check for
USD 1800 by May 31. Vote carried.
ACTION: Stuart K doing the paperwork
STATUS: Pending

ITEM: Updates from XDC 2/2
Conference committe (Bart M, Keith P, Ian R) to prepare a budget including
incidentals, catering, A/V hire costs, set. 
ACTION: Keith P to present budget when complete (not until closer to the
conference)
STATUS: Pending

ITEM: 501c3 re-application
No current updates
ACTION: Stuart K to compile the data and send it to the lawyer
STATUS: Pending
"""]]