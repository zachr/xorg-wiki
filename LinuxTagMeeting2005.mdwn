

# The European X.Org Developers Meeting

* Time: Sunday June 19th and Monday June 20th 2005 
* Place: **[[LinuxTag|http://linuxtag.org]]** in Karlsruhe, Germany. 
The X.Org Foundation is planning its first European Developers Meeting right before **[[LinuxTag|http://linuxtag.org]]** in Karlsruhe, Germany ** on Sunday June 19th and Monday June 20th**. 

The **purpose** of the conference is to bootstrap a European developers community. 

We expect to have 5-6 **talks** a day, 3 in the morning and 3 in the afternoon. This gives plenty of time to add additional presentations and discussions as well as extensive hacking. 

The **conference language** is English. 


## Call For Papers

There are still slots open for presentations so if you are interested in giving a talk please send us the title and a brief abstract to our [[contact address|LinuxTagMeeting2005]]. 

Presentations will be accepted until the first day of the conference, however we encourage you to contact us early as interest is great. 


## Schedule

Please check our preliminary [[LinuxTagMeeting2005Schedule|LinuxTagMeeting2005Schedule]]. 


## Fees

The conference will have **free entrance**, participants will have to pay travel and accommodation. 

Some **funding** is available to X.Org to sponsor travel and accommodation for people who are unable to take care of these expenses themselves. People applying for this should be prepared to give a talk. When submitting your abstract please indicate if you require funding. 

We are still working on getting free admission to the LinuxTag show for all those who participate in the conference - at least those who give a presentation. 

We've got a number of tickets that we will distribute among those participants who don't have other ways of getting into the show (for example as exhibitors) if they have [[registered|LinuxTagMeeting2005]] with us and indicated their need for a ticket. As the number of ticktets is limited please only get one if you really plan to go to the show. 

X.Org will organize a social event for all those who participate in the meeting. (Don't confuse this with the [[LinuxTag social event|http://www.linuxtag.org/typo3site/socialevent.0.html?L=1]]!) Admission is free for those who are giving a presentation on the meeting. Please see [[below|LinuxTagMeeting2005]] for further information. 


## Location

The meeting will take palce in room **S1** in the **upper level** of the **Konzerthaus**. Please check the [[area map|http://www.infodrom.org/Debian/events/LinuxTag2002/lageplan.html]] for the exact location. You can find links to further maps and information in our [[LinuxTag2005Infos|LinuxTag2005Infos]].  
 

_Please NOTE:_ The location of the room may still change, therefore make sure to check this page for updated information.  
 There is a [[list of participants|LinuxTagMeeting2005Participants]]. 


# X.Org on LinuxTag

X.Org is also going to have booth on [[LinuxTag|http://linuxtag.org]] from Wednesday, June 22, to Saturday, June 25. 

This booth is ment to be a gathering place where developers can meet and talk. At the same time people will be available on the booth to give demonstrations and to talk to the public. 


## Presentations

We are still looking for people who would like to give an X Window System related presentation on the show. Please indicate if you are willing to do so and what you would like to present. 

Tuesday, June 21 is planned for booth setup for all those who would like to give a presentation.   
 We expect that not all presentations can run simultaneously so once we get a rough idea of what you have planned to demo we will make up a schedule.  
 You should be prepared to bring your own equipment for your demo (ie. Laptop etc.), monitors or flat screens should be available on the booth, also there will be connectivity on the booth. If you need other special equipment that you won't be able to bring along we will do our best to provide it. Please indicate with your registration if you have special needs. 

If you have a **logo and/or a poster** in your demo please send us a PostScript file. We will try to print it and put it on display on the booth. 


## Social Event

<a name="social_event"></a> Everybody deserves a break! 

There will be a social event (not to be confused with the [[LinuxTag social event|http://www.linuxtag.org/typo3site/socialevent.0.html?L=1]]) for the people attending our meeting or participating in the booth activities to get together, eat, drink and talk. It will take place during LinuxTag right after the show. 

We don't know the exact location and date, yet. This information will be given when available 

The **costs** will be around EUR 20, for all those who give a presentation on our meeting or on the booth, or otherwise help with our booth activities admission will be free. If you are interested in coming - please let us know! 


## Booth Helpers

We are looking for volunteers to help out with booth duty. If you are interested please let us know. You will receive a free X.Org LinuxTag T-shirt as a little thank you. 


## Further Information

Please check the [[LinuxTag2005Infos|LinuxTag2005Infos]] for useful information concerning travel and accomodiations. 


## Documentation

Please send us your ideas and notes from the conference. As for pictures - please upload them to you website and send us its address. Thankyou! 


## Organizers

* The X.Org Foundation 
* Leon Shiman 
* Egbert Eich 

## Contacts

<a name="contact"></a> Please feel free to contact the organizers at [[linuxtag-org_AT_lists_x_DOT_org|mailto://<linuxtag-org_AT_lists_x_DOT_org>]] if you have further questions. 
