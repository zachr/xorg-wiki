# X.org Treasurer's Report ~ 2016

| BEGINNING BALANCE (Jan 1, 2016):                  ||    $38,448.79 |
| ------------------------------------ | -----------:| -------------:|
| … X.org Bank of America Account      |  $38,448.79 |               |
| … SPI Account                        |       $0.00 |               |
|                                                                  |||
| **Income**                                                       |||
| … Donations:                         |     $551.00 |               |
| … GSoC 2015 Stipends:                |   $3,000.00 |               |
| … **TOTAL INCOME:**                  |   $3,551.00 |               |
|                                                                  |||
| **Expenses**                                                     |||
| … EVoC 2015 Sponsorship:             |  -$4,500.00 |               |
| … XDC Sponsorships:                  |  -$2,959.44 |               |
| … NRAI (X.org Foundation):           |    -$518.82 |               |
| … BofA Fees:                         |     -$79.00 |               |
| … SPI:                               |     -$27.55 |               |
| … PayPal Fees:                       |      -$2.12 |               |
| … Click&Pay Fees:                    |     -$20.94 |               |
| … Chargeback:                        |     -$56.00 |               |
| … **TOTAL EXPENSES:**                |  -$8,163.87 |               |
|                                                                  |||
| **ENDING BALANCE (Dec 31, 2016):**                ||**$33,835.92** |
| … X.org Bank of America Account      |     $391.53 |               |
| … SPI Account                        |  $33,444.39 |               |


## Financial Management

This year marked a major transition for the X.org treasury.  The X.org
Foundation moved from being an independent organization to become a
member project within the Software for the Public Interest (SPI).

Over the course of the year, we first established financial management
within SPI, initiating our SPI-managed accounts with Click&Pledge and
PayPal, then transitioned the bulk of X.org's cash assets from the
organization's Bank of America account to SPI.  We are in process of
filing for dissolution of the X.org Foundation legal structure with the
State of Delaware, and then will close out the Bank of America account
and transfer the remainder of funds to SPI.

Along with this transition, we have also restructured our handling of
X.org's financial data.  The project's historical financial
documentation has been organized into an encrypted repository shared and
managed by X.org board members; this should help ensure X.org's
'institutional memory' is easily accessible to future board members.  A
Ledger CLI instance has been established in this repository for
registering all monetary transactions.

With these steps taken, X.org's financial management should be much more
streamlined and its legal costs significantly reduced.  Having SPI to
handle tax filing and cash accounts management removes a big time burden
from X.org.


## Fundraising

Google's Summer of Code program remained the principle source of income
for X.org this period, however the addition of SPI's PayPal and
Click&Pledge donation services in the 2nd half of the year augmented
X.org's income via numerous individual donations.  These services now
registered and in place to support future targeted X.org fundraising
activities as needed.  Member projects can link to the donation forms
from their own website, and SPI also maintains a multi-project form to
allow donors to distribute their donation across several projects.


## Funded Activities

The primary expenditures for the project in 2016 were sponsorships
relating to its 2015 Endless Vacation of Code (EVoC) and 2016 X
Developers' Conference (XDC) programs.  The EVoC sponsorship occurred in
2015 but due to complications related to international financial
transfers, the payment was finalized only in 2016.  For XDC, sponsorship
was for travel expenses for three attendees to Helsinki.
