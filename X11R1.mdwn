<div itemscope itemtype="http://schema.org/SoftwareApplication">

# <span itemprop="name">X Window System</span>, <span itemprop="version">Version 11, Release 1</span>

<span itemprop="description">
X11R1 was the first release of the X Window System, Version 11 from MIT.
</span>
It was <a href="https://groups.google.com/d/msg/comp.windows.x/NFT3Ax2Io7g/Jn37aUty-2QJ">released in
<span itemprop="datePublished" content="1987-09-15">September 1987</span></a>,
with the following changes excerpted from the release notes.
The sources are available for download for historical reference from
<a href="http://www.x.org/releases/X11R1/" itemprop="downloadUrl">http://www.x.org/releases/X11R1/</a>.

[[!toc levels=4 startlevel=2]]

## Overview

Welcome to the first release of version 11 of The X Window System.  This release
represents a major redesign and enhancement of X and signals its graduation from
the research community into the product engineering and development community.
The X Window System version 11 is intended to be able to support virtually all
known instances of raster display hardware and reasonable future hardware,
including hardware supporting deep frame buffers, multiple colormaps and various
levels of hardware graphics assist.

A note about terminology is in order here.  First, the document *X Window
System Protocol, Version 11* is the definitive description of what is (and
what is not) part of the core X Window System.  This is distinct from the
sample server implementation that forms the foundation of this release.
The sample server is provided here for the benefit of developers who wish
to quickly produce an implementation for a new piece of hardware.  The
sample server is not necessarily intended to be a reference
implementation, although that remains a possibility for the future.
Instead, it is our intention that future implementors will be able to rely on
the protocol specification and related documents and not be forced to refer to
the code.  The sample server, with device-dependent and device-independent
implementations as contributed from various places, will remain the foundation
of the M.I.T.  X Window System distribution.

The C language interface defined by Xlib is only one of many possible language
bindings to the protocol.  While we certainly encourage standardization of the
Xlib interface (and at least one international standards organization is
considering this), we are not dogmatic on this point.  Xlib has two somewhat
conflicting requirements; first, it is the lowest level procedural interface to
the X protocol.  As such, it must implement all the nuances possible without
encroaching on the vendors' and application developers' prerogative to set
conventions for style.  Xlib's second mission is to be the first level interface
for applications development, in anticipation of future work (such as the
toolkit) on common higher-level applications interfaces.  In fulfillment of this
objective, Xlib includes many "convenience routines" that are a re-packaging of
one or more protocol operations to simplify common cases, and it includes
several additional utility routines that have no direct relationship to the
protocol but that are required when building "real" applications, such as window
managers.

The toolkit is a 'beta test' release.  The design here should look familiar to
those of you who used our early prototypes under version 10, but we have changed
most of the interfaces in ways that we hope make the task of writing widgets
much easier to accomplish.  The earlier prototype toolkit is included on the
tape for those of you who need it to assist your conversion.  You should not
expect it to appear on the next release.

The clients provided on this release should not be construed as the correct way
for them to be written, but should be regarded as what most of them really are:
straight-forward ports of existing X version 10 clients.  Only a few use the
toolkit and only the old version at that.  Those of you who used previous
versions of X will find several differences in their input, output and options,
not all of them documented.  Some of the differences are intentional and some
not.  Feel free to let us know if you really think something is broken, but we
will also feel free to respond with an alternative to your proposal.

By default, X is distributed to install on a stock 4.3BSD system; installation
under Ultrix-32 release 2.0 should essentially be the same.  It has been known
to compile and run on ATT System V systems but we haven't had access to such a
system to verify this.

## Contents of this Release

The enclosed tape contains nineteen megabytes of source files and documentation
including:

 * A sample server with device-dependent layers (DDX) for several
   displays and vendors, including IBM, DEC, Apollo and Sun workstations.

 * Several libraries including a toolkit and both C and Common-Lisp
   language bindings for the X11 protocol.

 * Some bitmaps in include/bitmaps for use with xsetroot, et. al.

 * Several clients, including a terminal emulator and a toolkit-based client
   for browsing mh-style mail folders.

 * Some demo clients

 * Some random utilities to help you build this release

There are many aspects to the software in this release, so in the following we
provide a perusal of the release software by directory name, calling attention
to salient features or notable bugs.  To be brief, not all directories are
present in this discussion.

 * clients/bitmap

   This is a very nice program for editing small bitmaps such as cursors or
   icons.  However, it comes up a silly size under the "wm" window manager, and
   puts dialog box in wrong place.  With the "woman" bitmap, the small images
   are corrupt on color screens (server bug likely).  The program dumps core
   when colors are specified in .Xdefaults.

 * clients/emacs

   All that is provided here are the files that needed to change in gnuemacs so
   that it could use the X11 window system.  You must obtain the complete source
   for this separately.  Consult the files DISTRIB and README in this directory
   for more information.  This implementation is known to be slow and to have
   screen-updating problems, especially when doing incremental searches with
   control-S.

 * clients/gnuplot

   This is a plotting program converted by an undergraduate summer student at
   MIT and he was never able to test it on anything but a microvax.  The display
   window is totally white on Sun, for both BW and color.  The program fails to
   link on Ultrix 2.0 because it is missing -lgamma on the load line.

 * clients/puzzle

   A very simple program that animates a game you probably carried in your
   pocket as a child.  This really belongs in the hacks directory, but we
   noticed it too late.  It doesn't center text in squares correctly when its
   window is created with the default height and width.  The window never
   appears when run under wm.

 * clients/uwm

   This is the most mature window manager available on the release.  Resizing
   with this window manager causes double exposures because of the tiny XxY
   display box placed over the window being resized and because of lack of
   save-unders.  The server should do save-unders (the saving and restoring the
   contents of the screen under a very short-lived window), but in the interim,
   uwm should either place the display box outside the bounds of the resized
   window, or perhaps unmap the display box after resize, so that least both
   repaints are at the new size.  The program doesn't deal with multiple
   screens.

 * clients/xcalc

   This is a clever tool that implements a hand-held calculator or a slide-rule
   on the screen.  Unfortunatley, it dies with a parameter mismatch on
   ChangeWindowAttributes when the cursor leaves the xcalc window on color
   screen, and it dies immediately in -analog mode on a color screen.  The
   program dumps core when run without a window manager in -analog mode, and has
   a strange initial window size when started under wm.

 * clients/xclock

   This displays digital or analog time.  The "-update N" option has an old bug
   of not keeping the second hand pointing to a multiple of N seconds.  It also
   doesn't center the time string in digital mode.

 * clients/xedit

   This is a nice attempt at implementing an editor under X11 using the toolkit.
   But it dumps core on a black-and-white Sun.  It gets parameter mismatch for
   CreateGC when run on a color screen.  It makes the assumption that
   background=white.  Resizing this client sometimes seems to do strange things.

 * clients/xfd

   This program will display the X11 fonts.  Pressing the right button should
   page through the font, but continues to wrap around and continue on forever.
   The -icon option doesn't seem to work with uwm.

 * clients/xhost

   This provides a way to allow or deny access to specific hosts.
   Regretably, it does not allow the user to enable/disable access control.

 * clients/xload

   This client displays the "load average" in a histogram plot, but it may need
   to be set-userid to root or set-groupid to kmem if the permissions to
   /dev/kmem don't allow normal users to read it.  In addition, the toolkit
   widget may need to be reimplemented for your kernel.

 * clients/xlsfonts

   This client will list the fonts known to the server, but the undocumented -l
   option (long listing ala QueryFontsWithInfo) brings out a shortcoming in the
   sample server: it will freeze your display until the request is complete,
   which may take several seconds.

 * clients/xmh

   This is potentially a very nice interface to the mh-style tools used to read
   one's mail.  But it gets parameter mismatch on CreateGC on a color screen.
   It, too, has the background=white assumption.

 * clients/xmodmap

   This client will allow you to change the modifier mappings of your keyboard.
   Sometimes, additions will fail because it doesn't know how to "stretch"
   the modifier map, but success is still indicated.

 * clients/xpr

   This program prints out a screendump created with xwd.  It seems to have VAX
   byte order dependencies for -dev ps (postscript output).  Postscript output
   generates printer errors sometimes.

 * clients/xprop

   It displays properties of windows and fonts, but it should have a way to get
   at subwindows.  As is, it is useless under wm because it will only give
   information about the "frame" window.

 * clients/xrdb

   This client will take an Xdefaults file and load it into the server as a
   property for use by clients globally.

 * clients/xsetroot

   Among other things, try the command "xsetroot -bitmap include/bitmaps/woman".

 * clients/xterm

   This is the one and only terminal emulator for X11 available publicly.  If it
   is resized by wm, it paints twice; once at the old size and once at the new.
   Selecting reverse video from the menu doesn't invert scroll bar.  The -ms and
   -cr options don't work.

 * clients/xwd

   Dumping a window or a portion thereof is the purpose of this client.  The -xy
   mode on color screens is broken (server bug likely).  It doesn't retain
   button release during grab, resulting in spurious up event to the window you
   wanted dumped.  If the output is desired in z format from a 1-bit screen, it
   duplicates a top stripe of the selected window instead of dumping the window
   as seen (server bug likely).  It should check for the selected window being
   on-screen.  Large windows cause the Xqdss server to dump core (server bug).

 * clients/xwininfo

   Like xprop, the program doesn't retain button release during grab, resulting
   in spurious up event to another window.

 * clients/xwud

   This client will dump to the screen, the file created by xwd, but a partial
   refresh of Z format image produces garbage (library bug).  A 1-bit image on
   N-bit screen is garbled (server bug likely).  It dies in strange ways on
   large color images.

 * contrib/InterViews

   This is the only software found in the "contributed software" directory:
   i.e. untested software.  This is a C++ implementation of a toolkit and some
   clients.

 * demos/ico, muncher, plaid

   These are cute programs that do nothing but paint somewhat pretty pictures.
   They don't obey window resizing or exposure correctly.  The "plaid" program
   needs to throttle output to keep other clients happy, but this may suggest
   that the server isn't round-robining properly.

 * demos/wm

   This is the "other" window manager; it is pretty simple minded, but it
   implements title bars, real-estate style window management and understands
   multiple screens.  It seems to cause various clients grief, particularly with
   respect to initial size.  It should do something about borders so that they
   arent always white, and when it closes down, it should restore rational
   borders.

 * doc

   As might be expected, this contains all the documentation.  Any file in
   this directory named \*.ps or \*.psc can be handed directly to a postscript
   printer, as it is the postscript output from programs such as troff.

 * doc/Protocol

   Here you will find the formal description of the protocol, the numeric
   encoding of same and the keysym description.

 * doc/Server

   There are several documents in this directory dealing with the implementation
   of the sample server and helpful hints for porting.

 * doc/Xlib

   The C language X interface is documented here.  The printed document is just
   under 300 pages and is quite complete.

 * doc/Xlib/Xman

   The section 3 man pages can be found here.  If you can tolerate file names
   longer than 14 characters, you should consider running the (largely untested)
   shell script named "expand.names" in this directory that will change the
   prefix of the .3X files to names matching the names of the Xlib routines they
   describe.

 * doc/bugs

   A bug report template appears in this directory.

 * doc/releasenotes

   You will find the troff source for this document here.

 * extensions

   This directory contains a lib, server, test and include directory for two
   extensions: a useless trapezoid filling extensioin called "zoid" and a rather
   interesting one for implementing bezier curve fitting called "bezier".  The
   beziertest program repaints on the wrong kind of events.

 * fonts/bdf

   The ascii representation of the fonts live in this directory.  You should try
   to remember that the "bdf" stands for Bitmap Distribution Format.  This
   format is an extension of Adobe, Inc's standard ASCII format for font
   distribution.  It should be noted that you can purchase fonts from Adobe,
   Inc. in a format close to this.  When doing so, you should ask for both the
   bitmap and the font metric files ('afm'), which are combined together for an
   X11 .bdf file.  Please do not contact MIT to buy fonts.

 * fonts/snf

   This directory holds the compiled fonts.  The "snf" stands for Server Natural
   Format.  snf files are not portable between servers except in rare cases.

 * include/bitmaps

   Bitmap files found in this directory have a different format in X11 than they
   did in X10.  The "bitmap" program can read files in either format, but always
   writes out X11-format files.  In addition, there is a utility program called
   "bm-convert" which converts X10-format bitmap files to X11 format.  For more
   information, see clients/bitmap/bitmap.man and
   util/bm-convert/bm-convert.doc.

 * lib/CLX

   This contains the Common - Lisp language interface to the X11 protocol and
   has been tested under TI, Symbolics, and Lucid.

 * lib/X

   Here you can find the source for the C - language interface to the X11
   protocol.  The next section is devoted to its discussion.

 * lib/Xrm

   This is the directory containing the implementation of the "new" resource
   manager.  None of the clients use it and neither the old nor new toolkits use
   it.  However, this will be the resource manager used in future releases.  As
   soon as the new toolkit is stable and has been converted to use this, all
   clients will be converted as well.  If you write a new client, it should use
   this resource manager. Note that both the new and the old resource manager
   are case-sensitive now.  You should keep this in mind when you build your
   .Xdefaults file.

 * lib/Xtk

   The Xtk library ("new toolkit") is still in a very unfinished state.
   We've identified three bugs that cause problems on the Sun, but
   not on the Vax:

   * Resources.c fails to correctly handle resources whose size is 1; in
     particular, it gets confused on Boolean resources such as the
     "analog/digital" option to the Clock widget.  There is currently special
     case code, surrounded by "#ifdef BIGENDIAN", to handle resources whose size
     is 2 (= sizeof(short)); this code probably needs to be extended to
     similarly handle resources whose size is 1.

   * Text.c incorrectly calls XtGetSubresources with a Display argument
     instead of the proper Widget argument. This core-dumps the "xtext"
     application instantly on a Sun.

   * AsciiSink.c contains bitmap constants declared as arrays of shorts,
     rather than arrays of char, and assumes that the shorts are stored in
     LSBFirst (little-endian) order.  The result is that on big-endian
     machines such as the Sun, Text widgets don't display a caret at the
     insertion point.

   All of the widgets should be using XtCreateWindow instead of XCreateWindow,
   but currently none of them do.  The SetValues routines in most of the widgets
   are either incorrect or are complete no-ops.  None of the widgets' SetValues
   routines have been exercised by any code yet, with the exception of the
   "top-level" widget that is returned by XtInitialize.

   The IBM hc compiler will not compile lib/Xtk directory, but the pcc compiler
   will.  We suggest you build this directory by hand with pcc before doing a
   full make.

 * lib/Xtk/clients

   With the exception of "xboxes", all the sample clients in this subdirectory
   run more-or-less correctly on a Vax.  However, various bugs become evident
   when you run them on a Sun.  For instance, "xtext" immediately core-dumps,
   and "xclock" comes up in digital mode regardless of .Xdefaults options or
   command line arguments.

 * lib/oldX

   This library contains code that existed in X10 such as the "XAssocTable"
   routines and an implementation of the X10 XDraw and XDrawFilled requests that
   turn spline descriptions into X11 PolyLine requests.  You should not assume
   that these routines will continue to exist in this form in future releases.

 * lib/oldXMenu

   This is a deck-of-cards menu facility that has simply been ported to X11 and
   is known to run slowly.  Eventually, this will be subsumed by a menu widget
   in the toolkit.

 * lib/oldXrm

   This is the "old" resource manager as it appeared on the Beta release.
   It will remain a part of the release only as long as there remains a
   client that has not been converted to the new resource manager.

 * lib/oldXtk

   This contains the "old" toolkit, with the old intrinsics interface.  As soon
   as the new toolkit becomes stable all clients will be converted to it and
   this will not appear on future releases.  Currently, only xload, xmh, xclock,
   xedit and xterm use this toolkit.

 * server/ddx/apollo

   This holds the Apollo device-dependent code (ddx) for the sample server.
   It will run on any Apollo display, color or monochrome, but it will only do
   1-bit black-and-white graphics.  You should consult the README file in
   this directory before trying to build an Apollo server.
   The only problem not mentioned there is that the server builds with about
   a half-dozen undefined variables, but since they are never referenced, the
   server runs.

 * server/ddx/cfb

   In here, Sun Microsystems has placed a "generic" analogue of "mfb" for memory
   color frame-buffers.  The code has been tested only on displays with a pixel
   size of 8 bits, both with all 8 planes implemented (Sun) and less than 8
   planes implemented (HP).  As distributed, it is set up for 8-bit pixels with
   all 8 planes implemented.  The code is intended to be highly portable, and to
   use as much as possible of the "mi" code.  As a result, it is very slow, but
   very portable.  You should read the README file in this directory before
   using it for a new port.

 * server/ddx/ibm

   IBM has provided us with ddx layers for IBM PC/RT with AED and/or APA16/APA19
   display.  The hc compiler available on this machine is known to have problems
   with *foo++ = *bar++ constructs.  To get around this, you should run the
   command "RTSETUP init" in this directory before the server is built.  This
   will build patch, makedepend and imake, then it will patch sources for the
   sake of the RT compiler (miregion.c and ../../../lib/Xtk/Imakefile).
   Finally, it will build and depend the makefiles.  After this, a top-level
   make should complete without errors.  Note that source files will be patched
   only if the "init" argument is specified on the command line.

 * server/ddx/lk201

   This is the ddx driver code for the keyboard used by most DEC displays.

 * server/ddx/mfb

   This is the "monochrome frame-buffer" ddx code.  If you are beginning a new
   port and plan to use this, you should read the code very carefully in
   maskbits.h and the comments in ../../include/servermd.h, and then choose
   appropriate constants for IMAGE_BYTE_ORDER, BITMAP_BIT_ORDER,
   GETLEFTBITS_ALIGNMENT and GLYPHPADBYTES.

 * server/ddx/mi

   This is the "machine-independent" ddx layer.

 * server/ddx/qdss

   The pre-compiled libraries found here are are for DEC's GPX display.  No
   source code is available.

 * server/ddx/qvss

   This holds the DEC version of the QVSS display ddx code.

 * server/ddx/sun

   This is the directory containing general Sun ddx code.  It expects to be
   linked with the cfb code.  You should consult the README file in this
   directory before using this server.

 * server/dix

   In here are the sources for the device-independent layer of the sample
   server.

 * util/bm-convert

   This is a special utility for converting X10-format bitmaps to X11-style.
   Alternatively, you can run the bitmap program, which can read X10-format
   bitmaps and will always write X11-format bitmaps.  This program will never be
   built unless you explicitly do so in this directory.

 * util/imake

   This facility has become crucial to the maintenance of the X11 source tree.
   If you have a machines other that what this release has been tested for, I
   suggest that you read the man page (found in this directory) very carefully.
   This is the program used to create all the Makefiles from the files named
   "Imakefile" throughout the source tree.

 * util/imake.includes

   This is where the header files and template are kept for the imake utility.
   You should study the \*.macros files carefully to decide how to configure
   your machine.

 * util/makedepend

   This utility is used to generate <i>make</i>(1) dependencies for all the .o
   files.  It runs an order of magnitude faster than any other implementation of
   a dependency-generator by caching the include files for each directory.

 * util/patch

   This is the source-code patching utility written by Larry Wall and
   distributed in mod.sources.  If there are patches sent out from MIT, this
   program can be used to apply them.  This program will never be built unless
   you explicitly do so in this directory.

 * util/rgb

   This the the unchanged X10 version of program by the same name.  It reads a
   text file describing a set of colors, and creates a database for the server
   to consult.

 * util/vtobdf

   This program reads a Sun vfont file and creates a character bitmap
   distribution format file.

### Your .Xdefaults file

In previous releases of X, the specification of configuration
options in the .Xdefaults file was not case-sensitive,
although this fact was never explicitly mentioned in any
documentation.

For version 11, we have chosen to define the contents of .Xdefaults
to be case-sensitive.  This is not yet fully
implemented; some clients may still allow non-matching case in
their options.

The principal reason for this change is the addition of option "classes"
to the Resource Manager, where before everything was
only an option "instance".  The new naming and classing
hierarchy will enable users to customize large parts of client
interfaces with much fewer lines than before, at the expense
of having to read the man pages somewhat more carefully when
customizing.

## Xlib - C Language X Interface

These notes document the known problems in the base X library as of the first
release of X Version 11, as well as some comments of what has changed during the
last phases of beta test, since the beta test was so large.  Bugs reported after
early September are unlikely to have been fixed unless extremely important.  My
thanks to the beta testers for their efforts and comments.

<b>XPutImage</b> was extensively reworked at the last moment; see the comments
at the beginning of the routine.  There are a few remaining nits with nibble
swapping.  <b>XGetImage</b> semantics are slightly different than during most of
beta test.  If you only ask for certain planes in x-y mode, the image returned
is of the depth of the number of planes requested.

<b>XPutSubImage</b> in Z format does not work properly.

<b>XPending</b> still uses FIONREAD, which means that it does not detect dropped
connections properly.  This was the cause of many client looping bugs when
connections failed on V10.  It should be be recoded to use a non-blocking read
of 0 bytes to get status back.

<b>XGetDefault</b> now has the correct interface with a (Display *) as the first
argument.  Note that during beta test, the document was correct, and the code
was wrong.  Your code which you may have been running during beta test using
this routine will have to be converted.

The types of members of some structures in Xlib.h were fixed so that machines on
which int=16 bits would work correctly.  Some members were declared int but were
plane masks and should have been declared unsigned long.  Some other members
were renamed to type <b>Bool</b> and <b>Time</b>, though the underlying type did
not change.

The region code in the library has been regenerated after extraction from the
server.  Since no one reported a single bug in the region code during beta test
and we know there were many bugs in it from server bug reports, it clearly was
not heavily exercised (if at all) then.

There ought to be some way to generate a region from a series of exposure
events, to make it very easy to do clever repaint on exposure of only exposed
areas, using the GC region.  No one volunteered to specify it/write it, so there
still is no way.  The region code was known to be buggy during beta test, which
may have reduced the inclination.

V10 conversion aid code documented in the X manual appendix is in a separate
library; to link against it under UNIX add a <i>-loldX</i> to your link command.

The resource manager is currently split out into a separate library called
<i>Xrm</i>.  It will be merged in a future release.  This occurred since the
tool kit did not get converted to use the correct interfaces.  The old (beta
version) is in oldXrm and is still used by the tool kits.  Since the X tool kit
is the primary user of the resource manager, this also means that the current
resource manager has not had the extensive exercise of the rest of the library
(or even of the old resource manager), and will undergo some final changes
before being incorporated, though the interface is believed correct except for
<i>XrmPutResource</i>, which should take a string rather than a quark.  This
change was dropped on the floor until after the code had been frozen for
release.  Note that only applications linked against one of the resource manager
libraries will use the error database as documented in the manual.  Clients
linking only against Xlib will only print the default strings.  Until the next
MIT release, you should therefore understand that it is conceivable that these
routines may change interfaces; somewhat more likely is that more sophisticated
matching will be implemented without interface changes.  The most unfortunate
part of this is that only <b>XGetDefault</b> in the new resource manager knows
how to deal with the server resource data base property.  (See the xrdb
utility).  (One of the real pains to life in V10 was keeping your .Xdefaults
file up to date on all the machines you used).

The library is not as portable to some machine architectures as we would like.
The changes required to deal with the Cray for example, were more extensive than
we wished to incorporate during beta test, affecting too many files with
insufficient time for shake down.  My apologies to them, since it will have cost
them additional work until the changes are made.  Machines on which short!=16
bits may have difficulty in a port at the moment.

The V10 functionality that allowed remapping keys to strings read in from a file
did not get reimplemented, due to upheavals during both alpha and beta tests in
keyboard handling in general.  This is less needed in V11, where there is global
renaming of keysyms, but would still be nice to have.  The compose processing
(yuk) in <b>XLookupString</b> is not implemented.

There are an extensive set of routines/macros for extracting screen structure
information added late in beta test.  The tool kit is being reorganized to deal
with multiple screen applications in a graceful way, and these additions were
clearly useful.

Manual pages for Xlib now exist, though are quite terse.

Extensive work on the Xlib manual took place; the most glaring deficiencies are
the lack of illustrations in the right places.  There will be some additional
work on the manual until it goes to Digital Press for printing as a book.

We still don't have an introductory Xlib Primer.  Any volunteers out there?

Locking for multi-threading is still untested.
Unix needs a new system call interface!

## Credits

Whereas the design and implementation of the first 10 versions of X were
primarily the work of three individuals, Robert Scheifler of the M.I.T.
Laboratory for Computer Science and Jim Gettys of Digital Equipment Corporation
and Ron Newman of M.I.T., both working at Project Athena, X version 11 is the
result of the efforts of literally dozens of individuals at almost as many
locations and organizations.  At the risk of offending some of the players by
exclusion, several people deserve special credit and recognition.

Managerial Support (people without whose support and political battles the
release would never have materialized):

<pre>
Carol Berry, IBM            Steve Lerman, MIT
Larry Breed, IBM            Al Lopez, Apollo
Michael Dertouzos, MIT      Scott McGregor, Digital
Sam Fuller, Digital         Jerry Saltzer, MIT
Dan Geer, MIT               Warren Teitelman, Sun
Jeff Lane, Digital          Dave Verhoven, Tektronix
Rick LeFaivre, Tektronix    Smokey Wallace, Digital
</pre>

Thanks also to DARPA, for supporting the research side of the X protocol.

Our undying gratitude to Todd Brunhoff (and to Tektronix for sending him);
Todd performed wonders in making the alpha and beta tests happen, filtering
bug reports, integrating sources from everywhere, putting the releases
and updates together, and generally hacking up a storm.

Infinite thanks to Ken Goldman, MIT, for accepting thousands of phone calls,
mail messages, being patient, sending out documentation, and generally keeping
us insulated from the outside world.

Great appreciation goes to Phil Karlton and Scott McGregor, both of DEC, for
their considerable contributions to the specification of the version 11
protocol.  Also Sue Angebranndt, Raymond Drewry and Todd Newman of DEC's Western
Software Laboratory, with Phil, have worked long and hard to produce the sample
server implementation.

Special thanks must also go to Sam Fuller, Vice President of Research at
DEC, who has remained committed to the widest public availability of X and
who foresaw a year ago the need to greatly supplement Project Athena's
resources with the WSL staff in order to make version 11 a reality.

Many thanks from us and the several vendors who have a head start on porting X11
to their color frambuffers goes to Dave Rosenthal of Sun Microsystems.  He
provided the sample generic color frame buffer ddx code and also contributed to
the protocol.

Many other people made it happen, either with code or documentation; so many of
these people worked on more than one major component of the system (toolkits,
libraries, applications, servers) that we have not attempted to list people by
component:

<pre>
Allen Akin, Digital         Geoff Lee, Sun
Susan Angebranndt, Digital  Ed Lee, HP
Tom Benson, Digital         Bob Leichner, HP
Adam de Boor, UC Berkeley   Mark Manasse, Digital
Todd Brunhoff, Tektronix    Stuart Marks, Sun
Richard Carling, Masscomp   Joel McCormack, Digital
Colyn Case, Digital         Rick McKay, HP
Ann Mei Chang, Digital      Al Mento, Digital
Mike Chow, Digital          Don Mullis, Digital
Larry Church, Digital       Hal Murray, Digital
Jeff Dike, Digital          Todd Newman, Digital
Raymond Drewry, Digital     Ron Newman, MIT
Jerry Farrell, Sun          Mark Opperman, Sun
Burns Fisher, Digital       Vince Orgovan, Digital
Erik Fortune, IBM           Jack Palevich, HP
Hania Gajewska, Digital     Matt Pappas, Digital
Mike Gancarz, Digital       Ram Rao, Digital
Peter George, Digital       Loretta Guarino Reid, Digital
Jim Gettys, Digital         David Rosenthal, Sun
Dave Gorgen, Apollo         Paul Rubin, MIT
Jackie Grandfield, Digital  Robin Schaufler, Sun
Joel Gringorten, Digital    Bob Scheifler, MIT
Phil Gust, HP               Mike Schwartz, Sun
Frank Hall, HP              Alok Singhania, Sun
Peter Ham, Digital          Ralph Swick, MIT, Digital
Charles Haynes, Digital     Al Tabayoyon, Apollo
Scott Hennes, Apollo        Fred Taft, HP
Harry Hersh, Digital        Leo Treggiari, Digital
Frances Ho, Sun             Jake VanNoy, Digital
Tom Houser, HP              Pam Verno, Digital
Rich Hyde, Digital          Smokey Wallace, Digital
Richard Johnsson, Digital   Jeff Weinstein, IBM
Phil Karlton, Digital       Terry Weissman, Digital
Kathy Langone, Digital      Ted Wilson, HP
Mary Larson, Digital        Al Wojtas, Digital
</pre>

Thanks to the alpha and beta sites, who contributed much in the way of bug
reports and fixes.

Thanks to the MIT Statistics Center, for the use of their Suns and for putting
up with us.

Thanks to John Guttag of MIT LCS for the use of an 8600 for final build test.

<div style="font-size: small; border-top: 1px solid black;">
<p>
Copyright &copy; <span itemprop="copyrightYear">1985, 1986, 1987</span> by the
<span itemprop="copyrightHolder"
 itemscope itemtype="http://www.schema.org/Organization">
<span itemprop="name">Massachusetts Institute of Technology</span>
</span>.
</p><p>
Permission to use, copy, modify, and distribute this
software and its documentation for any purpose and without
fee is hereby granted, provided that the above copyright
notice appear in all copies and that both that copyright
notice and this permission notice appear in supporting
documentation, and that the name of M.I.T. not be used in
advertising or publicity pertaining to distribution of the
software without specific, written prior permission.
M.I.T. makes no representations about the suitability of
this software for any purpose.  It is provided "as is"
without express or implied warranty.
</p>
<p>
Ultrix and Ultrix-32 are trademarks of Digital Equipment Corporation.
</p>
<p>
The X Window System&trade; is a trademark of the Massachusetts Institute of
Technology.
<p>
UNIX is a trademark of AT&T Bell Laboratories.
</p>
</div>
</div>
