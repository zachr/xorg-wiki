# Call For Proposals **2018 X.Org Developers Conference (XDC 2018)** **26-28 September 2018** **A Coruña, Galicia, Spain**

The [2018 X.Org Developers Conference](http://www.x.org/wiki/Events/XDC2018) is the annual technical meeting for [X Window System](http://x.org) and [Free Desktop](http://freedesktop.org) developers. The attendees will gather to discuss outstanding technical issues related to the Open Source Graphics stack (X, Wayland, Mesa, DRI, ...) and its software ecosystem. The event is free of charge and open to the general public.

The XDC 2018 Technical Program Committee (TPC) is requesting proposals for papers and presentations at XDC 2018. While any serious proposal will be gratefully considered, topics of interest to X.org and [FreeDesktop.org](http://freedesktop.org) developers are encouraged. There are three particular types of proposal the TPC is seeking:

1. Technical talk abstracts: 250-1000 words describing a presentation to be made at XDC 2018. This can be anything: a work-in-progress talk, a proposal for change, analysis of trends, etc.
1. Informal white papers: 1000+ words on something of interest or relevance to X.org developers, FreeDesktop.org developers or the X community at large. These papers will appear in the online conference proceedings of XDC 2018, and are unrefereed (beyond basic checks for legitimacy and relevance). Papers can be refereed if requested in advance.
1. Technical research papers: 2500+ words in a format and style suitable for refereed technical publication. Papers that are judged acceptable by the TPC and its referees will be published in the online conference proceedings of XDC 2018.

XDC 2018 technical presenters will be chosen from the authors of any of these submissions (as well as other presenters invited by the TPC).

Presenters will be selected from those who submit before the deadline. There is usually time left for late talk proposals, but do not count on it. We also may be able to offer financial assistance for travel for presenters who could not otherwise afford to attend and who submit before the deadline.  Please do submit your proposal in a timely fashion.

Remember when you submit your proposal that the talks will be recorded and live-streamed.

**Proposals due:** Wednesday 25th July 2018 17:00 UTC.

**Accepted formats:** PDF and ASCII text.

**Notification of acceptance:** Wednesday 1st August 2018.

**E-mail:** board@foundation.x.org
