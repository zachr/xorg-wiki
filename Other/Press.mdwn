This page lists press releases issued by the [[X.Org Foundation|XorgFoundation]], regarding  X.Org.

The [[/News]] page lists other news about X.Org software releases & development events.

* 1st June, 2015: [[X.Org Developer's Conference: Call for Proposals 2015|Other/Press/CFP2015]]
* 2nd May, 2014: [[X.Org Developer's Conference: Call for Proposals 2014|Other/Press/CFP2014]]
* 9th August, 2012: [[X.Org Foundation joins Open Invention Network (OIN) patent non-aggression community|Other/Press/XorgOIN]] 
* 2nd August, 2012: [[X.Org Developer's Conference: Call for Proposals 2012|Other/Press/CFP2012]], [[supplement|Other/Press/CFP2012_supplemental]] 
* 20th July, 2012: [[X.Org Foundation achieves non-profit public charity status|Other/Press/501c3StatusDetermination]]
* 26th October, 2009: [[X11R7.5 released|Other/Press/X11R75Released]] 
* 15th February, 2007: [[X11R7.2 released|Other/Press/X11R72Released]] 
* 22nd May, 2006: [[X11R7.1 released|Other/Press/X11R71Released]] 
* 21st December, 2005: [[X11R6.9 & X11R7.0 Released|Other/Press/X11R6970Released]] 
* 18th April, 2005: [[X Window System Test Suite Released|Other/Press/XTSReleased]] 
* 9th February, 2005: [[X11R6.8.2 Released|Other/Press/X11R682Released]] 
