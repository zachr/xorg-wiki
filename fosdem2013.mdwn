# X.Org DevRoom at FOSDEM2013

* Time:  Saturday the **2nd** of **February 2013**. 
* Place: At **[[FOSDEM 2013|http://fosdem.org/]]** in Brussels, Belgium. 
The goal of X.Org DevRoom at FOSDEM is to have X.Org/Mesa/Wayland and related projects' developers meet, discuss and hack and have a very clear presence at [[one of the most highly regarded Free and Open Source community events|http://www.fosdem.org/]]. So FOSDEM is the perfect place to get exposed to both other developers and the (advanced) user community. 

Sadly the DevRoom is limited to only a single day this year, namely Saturday. This makes scheduling kind of tight, and the massive ARM GPU driver block was partly moved to a mainline talk to relieve this scheduling tightness. 


### DevRoom Talks Schedule

<a name="schedule"></a> 

[[https://fosdem.org/2013/schedule/track/xorg/|https://fosdem.org/2013/schedule/track/xorg/]] 


## Further FOSDEM information

For more information about the FOSDEM event, there's always the [[FOSDEM website|http://www.fosdem.org/]]. It includes city maps, information about transportation and a list of hotels. 

If you would like a complete overview of FOSDEM, then maybe [[last years site|http://www.fosdem.org/2012]] will be of interest. 


## Contact

<a name="contact"></a> Please mail libv at skynet dot be. 
