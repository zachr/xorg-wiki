This page has general information on X development, including how to get started. 

A common misconception is that developers need to understand all of X to get started. This is not true, and even of the "core" developers, only few actually know all the pieces that put X together. The best way to get started is to simply pick a problem and give it a try. 

There is also a new book in the Amazon Kindle format, [[Hands-on Projects for the Linux Graphics Subsystem|http://www.amazon.com/dp/B007QJKOGS/ref=rdr_ext_sb_ti_hist_1]]. This includes a number of original projects on the X.Org implementation of the Linux graphics subsystem. The book is aimed for instructors, students and any other OS internals enthusiasts. 


## Documentation

* [[X New Developer’s Guide|guide]]: High level introduction to X and the surrounding graphics stack, and how to get started in developing for them.
* [[Xplain: Explaining X11 for the rest of us|http://magcius.github.io/xplain/article/]]: Another introduction to X, with interactive examples - work in progress.
* [[Glossary|Development/Documentation/Glossary]]: Various terms used in X, and what they mean. 
* [[Building the X Window System|Building_the_X_Window_System]]: For builders and developers working on the X.Org source tree 
* [[Submitting patches|Development/Documentation/SubmittingPatches]]: How to submit a patch to an Xorg project. 
* [[Making a release|Development/Documentation/ReleaseHOWTO]]: How to make an individual module release. 
* [[Writing documentation|Development/Documentation/WritingDocumentation]]: How to document your work in X. 
* [[X server source layout|Development/Documentation/XserverSourceLayout]]: A map to the different directories in the X server source 
* [[The devPrivates system|Development/Documentation/DevPrivates]]: All about the private storage system in the server. 
* [[Wrapping functions|Development/Documentation/WrappingFunctions]]: How to add hooks to functions in the server. 
* [[Grab processing|Development/Documentation/GrabProcessing]]: How input grabs are dealt with in the server. 
* [[Cursor handling|Development/Documentation/CursorHandling]]: Introduction into DIX cursor handling. 
* [[Input event processing|Development/Documentation/InputEventProcessing]]: How input events are dealt with in the server. 
* [[Security|Development/Documentation/Security]]: How to make use of the security facilities within X. 
* [[Performance|Development/Documentation/Performance]]: A fairly detailed document explaining what our performance problems actually are, and things that aren't performance problems at all. 
* [[Xorg input driver HOWTO|Development/Documentation/XorgInputHOWTO]]: How to write an input driver for X.Org. 
* [[How video cards work|Development/Documentation/HowVideoCardsWork]]: A conceptual overview. 
* [[Xorg video driver HOWTO|Development/Documentation/XorgVideoHOWTO]]: How to write a basic video driver for X.Org. 
* [[Kdrive Drivers|Development/Documentation/KdriveDrivers]]: notes for making new kdrive video drivers 
* [[Dri Wiki|http://dri.freedesktop.org/wiki/]]: Information about using the Direct Rendering Infrastructure (DRI) with X. 
* [[Multiseat|Development/Documentation/Multiseat]]: Various informations about how to obtain and develop such model. 
* [[Deprecated and obsolete|Development/Documentation/Obsolescence]]: Notes on deprecated and obsolete extensions and server-side stuff. 
* [[More Xorg Documentation|XorgDeveloperDocumentation]]: Some pointers from deep in the Wiki. This page should possibly be moved into Development/Documentation. 
* [[Pointer acceleration|Development/Documentation/PointerAcceleration]]: information on the pointer accel mechanism. 

#### Tools

* [[Using git|Development/Documentation/git]]: Information on using git. 
* [[Server debugging|Development/Documentation/ServerDebugging]]: How to debug the X server with gdb. 
* [[Server profiling|Development/Documentation/ServerProfiling]]: How to profile the X server. 
* [[Using ctags to find functions|Development/Documentation/UsingCtags]]: Some tips to find a function in vim. 
* [[Using Eclipse|Development/Documentation/UsingEclipse]]: Some tips to use Eclipse to develop X.Org. 

#### Various Projects

* [[PciReworkHowto|PciReworkHowto]]: How to fix a video driver to use libpciaccess 
* [[VgaArbiter|VgaArbiter]]: Routing VGA instructions correctly. 
* [[Xv2, Render API|Development/Xv2]]: a long rambling discussion about giving Pictures GCs, and moving Xv to Render + RandR. 
* [[X12|Development/X12]]: a list of things we'd fix if we ever get around to a new version of the core X protocol 

## Helping out

* [[ContributionHOWTO|Development/ContributionHOWTO]]: How to get started contributing bug reports, patches, fixes, or even entire drivers. 
* [[ToDo|ToDo]]: Things that need doing. 
* [[Janitor|Development/Janitor]] Xorg Janitor project page 
