# The X.Org Foundation X11R6.8 Release Status


## Table of Contents

[[!toc startlevel=2]]


## Current status

The following describes the current status of this release.  Each row describes one platform.  See the legend at the bottom of this page for a description of each column and the color codings.

The current release candidate tag is: ``XORG-6_8_0``

The release criteria is that the release is ready to ship when there are no red cells in table.

**At this time, we need:**

1. **Volunteers to perform the tests described below**.  Please e-mail [[xorg@lists.freedesktop.org|mailto:xorg@lists.freedesktop.org]] if you can test a platform.
1. There are several platforms listed at the bottom of the table that are currently all in the _open questions_ state.  If you have information about these distributions, please send e-mail to [[xorg@lists.freedesktop.org|mailto:xorg@lists.freedesktop.org]] with the info.
1. If any of the data in the table is incorrect, please e-mail [[xorg@lists.freedesktop.org|mailto:xorg@lists.freedesktop.org]] with a description of the error.
1. Please report intermediate states (e.g. build tests done), so that people can see your progress.

[[!table header="no" class="mointable" data="""
||||||||
 |  |  |  |  |  |  |
**OS** | **Arch** | **Distro/Release** | **Name** | **Build** | **Install** | **Conformance** | **Run**
AIX | arch??? | release(s)??? | Dan McNichol |  |  |  |
Cygwin | IA-32 | Net-release | Alexander Gottwald |  |  |  |
FreeBSD | Alpha | 4.10 | Eric Anholt |  |  |  |
FreeBSD | Alpha | 5.2.1 | Eric Anholt |  |  |  |
FreeBSD | AMD64/EM64T | 5.2.1 | Eric Anholt |  |  |  |
FreeBSD | IA-32 | 4.10 | Eric Anholt |  |  |  |
FreeBSD | IA-32 | 5.2.1 | Eric Anholt |  |  |  |
FreeBSD | IA-64 | 5.2.1 | Eric Anholt |  |  |  |
FreeBSD | Sparc64 | 5.2.1 | Eric Anholt |  |  |  |
HP-UX | HP PA-RISC | release(s)??? | Paul Anderson |  |  |  |
Linux | Alpha | Debian version(s)??? |   |   |   |   |
Linux | Alpha | Gentoo | Donnie Berkholz |  |  |  |
Linux | Alpha | SUSE 8.1 |   |   |   |   |
Linux | AMD64/EM64T | Debian version(s)??? |   |   |   |   |
Linux | AMD64/EM64T | Gentoo | Andrew Bevitt | RC2 | RC2 | RC2 | RC2
Linux | AMD64/EM64T | Red Hat FC2 |   |   |   |   |
Linux | AMD64/EM64T | Red Hat RHEL3 |   |   |   |   |
Linux | AMD64/EM64T | SUSE 9.1 | Egbert Eich |  |  |  |
Linux | AMD64/EM64T | SUSE SLES9 | Egbert Eich |  |  |  |
Linux | ARM | Debian version(s)??? |   |   |   |   |
Linux | ARM | Gentoo | Donnie Berkholz |  |  |  |
Linux | IA-32 | Debian unstable | Several | RC2 |  | RC2 |
Linux | IA-32 | Debian 3.0 |   |   |   |   |
Linux | IA-32 | Gentoo | Andrew Bevitt | RC2 | RC2 | RC2 | RC2
Linux | IA-32 | LTSP 4.1 | Jim McQuillan |  |  |  |
Linux | IA-32 | Red Hat 7.2 | Kevin Martin | RC2 | RC2 | RC2 | RC2
Linux | IA-32 | Red Hat 9 | Kevin Martin | RC2 | RC2 | RC2 | RC2
Linux | IA-32 | Red Hat FC1 | Mike Harris |  |  |  |
Linux | IA-32 | Red Hat FC2 | Mike Harris |  |  |  |
Linux | IA-32 | Red Hat RHEL3 | Mike Harris |  |  |  |
Linux | IA-32 | SUSE 9.1 | Several | RC2 |  | FC2 |
Linux | IA-32 | SUSE SLES9 | Egbert Eich |  |  |  |
Linux | IA-64 | Debian unstable |   |   |   |   |
Linux | IA-64 | Debian 3.0 |   |   |   |   |
Linux | IA-64 | Gentoo | Donnie Berkholz |  |  |  |
Linux | IA-64 | Red Hat RHEL3 |   |   |   |   |
Linux | IA-64 | SUSE SLES9 | Egbert Eich |  |  |  |
Linux | M68k | Debian version(s)??? |   |   |   |   |
Linux | MIPS | Debian version(s)??? |   |   |   |   |
Linux | MIPS | Gentoo | Donnie Berkholz |  |  |  |
Linux | PPC | Debian unstable | Nathan Hand | RC2 | RC2 | RC2 | RC2
Linux | PPC | Gentoo | Donnie Berkholz |  |  |  |
Linux | PPC | SUSE SLES9 | Egbert Eich |  |  |  |
Linux | PPC64 | Gentoo | Donnie Berkholz |  |  |  |
Linux | HP PA-RISC | Debian version(s)??? |   |   |   |   |
Linux | HP PA-RISC | Gentoo | Donnie Berkholz |  |  |  |
Linux | S/390 | Debian version(s)??? |   |   |   |   |
Linux | S/390 | SUSE SLES9 | Egbert Eich |  |  |  |
Linux | Sparc | Debian/SID | Chris Debenham | RC2 |  |  |
Linux | Sparc | Gentoo | Donnie Berkholz |  |  |  |
MacOS | PPC | 10.1 | Torrey Lyons |  |  |  |
MacOS | PPC | 10.2 | Torrey Lyons |  |  |  |
MacOS | PPC | 10.3 | Torrey Lyons |  |  |  |
MacOS/Darwin | IA-32 | 7.2.1 | Torrey Lyons |  |  |  |
MacOS/Darwin | PPC | 7.2.1 | Torrey Lyons |  |  |  |
OpenBSD | AMD64/EM64T | 3.6 | Matthieu Herrb | RC2 | RC2 | RC2 | RC2
OpenBSD | IA-32 | 3.6 | Matthieu Herrb | RC2 | RC2 | RC2 | RC2
Solaris | IA-32 | Solaris 9 | Alan Coopersmith |  |  |  |
Solaris | IA-32 | Solaris 10 Beta | Alan Coopersmith |  |  |  |
Solaris | Sparc | Solaris 9 | Alan Coopersmith |  |  |  |
Solaris | Sparc | Solaris 10 Beta | Alan Coopersmith |  |  |  |
IRIX | ??? |   |   |   |   |   |
LynxOS | ??? |   |   |   |   |   |
NetBSD | ??? |   |   |   |   |   |
OS/2 | ??? |   |   |   |   |   |
SCO | ??? |   |   |   |   |   |
"""]]


## Legend


### Columns

1. **OS** -- Operating system
1. **Arch** -- Architecture
1. **Distro/Release** -- Distribution with release number or simply release number if distribution is not applicable
1. **Name** -- Name of the person responsible for testing this platform
1. **Build** -- Release has been built properly
1. **Install** -- Release has been installed properly
1. **Conformance** -- Release passes VSW4 test suite
1. **Run** -- Release runs on platform

### Colors

1. **Blue** -- All column headings
1. **Green** -- **Build**, **Install**, **Conformance** or **Run** tests passed and are ready for release
1. **Red** -- **Build**, **Install**, **Conformance** or **Run** tests have **not** passed and are **not** ready for release
1. **Purple** -- Open questions (e.g., what distribution release is to be tested or what architecture is to be tested)

## Detailed test instructions

This section outlines the test procedure for each of the test columns above.  When a test run has been completed, please e-mail the [[xorg@lists.freedesktop.org|mailto:xorg@lists.freedesktop.org]] mailing list with the following information so that the release status matrix (above) can be updated. Note that we are interested in progress: please do not wait to complete all phases of testing to send in reports.

1. Your Name
1. The date tested
1. The platform you tested:
   * The operating system tested (e.g., AIX, Cygwin, !FreeBSD, HP-UX, Linux, etc.)
   * The architecture tested (e.g., Alpha, AMD64, EM64T, IA-32, IA-64, Sparc, etc.)
   * The distribution and release tested (e.g., Red Hat FC2, SUSE 9.1, Debian unstable, Solaris 9, etc.)
1. The snapshot or release candidate tag tested (e.g., XORG-6_7_99_1, etc.)
1. Build test status: passed or failed or untested
1. Install test status: passed or failed or untested
1. Conformance test status: passed or failed or untested
1. Run test status: passed or failed or untested
   * List the tests run

For any test(s) that failed, please include in your report the test(s) that failed, and file a bugzilla report if no one has already filed one against the failure(s) you found.


### Build tests

Each of the following build tests can be performed by copying the _sample_ host.def file (or the _alternate_) to the xc/config/cf directory and the running `make World >& World.LOG` (or other such command as appropriate for your platform), and then checking the !World.LOG file for any failures.

1. Build with empty host.def file ([[sample|http://www.freedesktop.org/~kem/build-tests/1/host.def]] [[alternate|http://www.freedesktop.org/~kem/build-tests/1a/host.def]])
1. Build with BuildServersOnly defined as YES ([[sample|http://www.freedesktop.org/~kem/build-tests/2/host.def]] [[alternate|http://www.freedesktop.org/~kem/build-tests/2a/host.def]])
1. Build with DoLoadableServer defined as NO ([[sample|http://www.freedesktop.org/~kem/build-tests/3/host.def]] [[alternate|http://www.freedesktop.org/~kem/build-tests/3a/host.def]])

Note that some systems do not have a compatible version of Freetype2 installed on their system, so in addition to each build requirement above, defining HasFreetype2 as NO is permitted.  Each _alternate_ host.def file above have this define included.


### Install tests

Each of the following install tests can be performed by building the release (as described above using the _sample_ or _alternate_ host.def file provided), running `make Install >& Install.LOG` (or other such command as appropriate for your platform), and checking the !Install.LOG output for any failures.

1. Build and install with no host.def file ([[sample|http://www.freedesktop.org/~kem/install-tests/1/host.def]] [[alternate|http://www.freedesktop.org/~kem/install-tests/1a/host.def]])
1. Build and install with: ProjectRoot defined to be something other than the default, and NothingOutsideProjectRoot defined as YES ([[sample|http://www.freedesktop.org/~kem/install-tests/2/host.def]] [[alternate|http://www.freedesktop.org/~kem/install-tests/2a/host.def]])

Note that some systems do not have a compatible version of Freetype2 installed on their system, so in addition to each install requirement above, defining HasFreetype2 as NO is permitted.  Each _alternate_ host.def file above have this define included.


### Conformance tests

After installing the full release, the conformance tests can be run using the X test suite, which can be downloaded [[here|http://www.freedesktop.org/~kem/testing/xtest.tar.gz]].  A helper script (called ``xreg``) is used to run the X test suite, which can be downloaded [[here|http://www.freedesktop.org/~kem/testing/xreg]].  See the next two sections below for more information on how to setup and use these tools.

_**UPDATE**_ The `xreg` script and the `xtest.tar.gz` tarball have been updated to fix some xtest errors.  Please update to these latest versions (27 Aug 04).


#### Setting up the X test suite

Here are some brief instructions on how to download and set up everything that you will need to run the X test suite:

1. `mkdir /tmp/testing`
1. `cd /tmp/testing`
1. `wget http://www.freedesktop.org/~kem/testing/xtest.tar.gz`
1. `wget http://www.freedesktop.org/~kem/testing/xreg`
1. `tar xvzf xtest.tar.gz`
1. `cd xtest/xsuite`
1. `sh BUILD.xf86`
1. `cd ../..`
Now you should be ready to begin testing.


#### Examples of how to use the xreg script

Here are some examples of how to use xreg to run the X test suite:

1. `xreg -xtest -xvfb`
   * This runs xtest at all default depths using the Xvfb server.
   * The default depths are 8, 15, 16, and 24+32.
   * The "24+32" depth is one that uses a depth of 24 with a frame buffer bits per pixel of 32 (i.e., -depth 24 -fbbpp 32).
1. `xreg -xtest -xorg -d 16`
   * This runs xtest at depth 16 using the Xorg server.
1. `xreg -xtest -xvfb -d 15 -test !XCopyArea`
   * This runs xtest at depth 15 using the Xvfb server, but it only runs the !XCopyArea test.
   * Selecting individual tests is very useful to track down test failures.
1. `xreg -xtest -xvfb -d 16 -xvfbwidth 1280 -xvfbheight 1024 -test !XFillRectangles -n 3-5`
   * This runs xtest at depth 16 using the Xvfb server running at 1280x1024, but only runs the third through the fifth tests of the !XFillRectangles test.

Notes on using xreg:

* The output from these test runs are stored in `pwd`/results by default.  You can change the default output dir using the -O command line option.
* The material below assumes that you have done a full install of the system to /usr/X11R6.  However, if you are using a different ProjectRoot, you can use the following command line option to the xreg script to run from that alternate location: `-projroot` _path-to-your-project-root_
* The files that are generated from an xreg run of xtest are:
   1. `X-setup..output` -- this file contains the output of the X server during the setup phase
   1. `xtest.DEPTH.DATE.TIME.errors` -- this file contains the list of errors found during the test run at depth _DEPTH_ made on date _DATE_ at time _TIME_.
   1. `xtest.DEPTH.DATE.TIME.report` -- this file contains the report of all tests run at depth _DEPTH_ made on date _DATE_ at time _TIME_.
   1. `xtest.DEPTH.DATE.TIME.summary` -- this file contains a summary of the errors found during the test run at depth _DEPTH_ made on date _DATE_ at time _TIME_.  The summary file is only useful during full test runs (e.g., not when running individual tests).
   1. `xtest.DEPTH.DATE.TIME.results` -- this directory contains the journal from the tests run at depth _DEPTH_ made on date _DATE_ at time _TIME_ as well as any error images generated.
* After running xtest, you can check to see if everything passed by looking at the summary/errors/report file(s) to see if there are any failures.
* There are some known failures that the summary file attempts to take into account.  The first part of the summary file is the list of failures, and at the end of the summary file is a diff between the known failures (e.g., !XDrawArcs) and what the failures were for this run.
* The xreg script has only been tested on Linux systems.  If there are problems with these scripts, please post patches to the [[xorg@lists.freedesktop.org|mailto:xorg@lists.freedesktop.org]] mailing list.
* There are many other options to xreg (and it can be used to run other tests such as x11perf).  Run `xreg -help` to see the usage message.

#### Actually running the conformance tests

For this section, one of the following should be used for testing:

* For platforms based on a XFree86-style DDX, the ``dummy`` driver should be used.
   * For example: `xreg -xtest -xorg`
   * This will run xtest at all the default depths using the Xorg server.
   * Check the output of each report or summary file to make sure that all tests that are expected to pass do actually pass.
* For all other platforms, ``Xvfb`` should be used.
   * For example: `xreg -xtest -xvfb -d "15 16 24+32"`
   * This will run xtest at depths 15, 16 and 24+32 using the Xvfb server.
   * Check the output of each report or summary file to make sure that all tests that are expected to pass do actually pass.
   * Note that Xvfb does not currently run at depth 8, so the example above limits the testing to depths 15, 16 and 24+32.  Update, this problem has been fixed in CVS now and Xvfb at depth 8 works again.

Additional notes:

* The ``Xvfb`` server is special X server that uses a virtual framebuffer.  It is normally built and installed with the full release.  See the `Xvfb(1)` for more information about this server.
* The ``dummy`` driver is a special driver available with the XFree86 DDX.  To use the dummy driver, simply substitue it for your normal card driver in the `Device` section of your `xorg.conf` configuration file.  For example, if you normally uses an ati driver, then you will have a `Device` section with `Driver "ati"` to let the X server know that you want it to load and use the ati driver; however, for these conformance tests, you would change that line to `Driver "dummy"` and remove any other ati specific options from the `Device` section.

### Run tests

After installing the full release, you can run the subset of tests listed below that applies to the platform being tested.  Please run these tests on at least two different driver families (where applicable).  For example, on an IA-32 system running Linux, you could run the tests using one card from the ATI driver family and another card from the NVIDIA driver family.

Tests for each driver family:

1. X test suite (listed above)
1. x11perf
1. rendertest (found in the xapps CVS repository on freedesktop.org)
1. Standard graphical environment
1. GL tests: glxgears, gloss, [[quake3|http://www.freedesktop.org/~jg/quake3.tar.gz]]
1. Switch to/from VTs (on Linux)

## Tinderboxes

Several tinderboxes have been set up to test the release.  See [[this page|http://freedesktop.org/cgi-bin/tinderbox3/showbuilds.pl?tree=XMonolithic]] for their status.

Ideally, the tinderboxes could be set up to run the build, install and conformance tests listed above.

-- Main.KevinEMartin - 17 Aug 2004
