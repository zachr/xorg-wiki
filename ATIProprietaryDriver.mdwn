## ATI/AMD Closed Source “fglrx” Drivers

Since the Radeon R200 chipset series ATI/AMD is providing its own set of closed source binary only drivers for their hardware. The drivers were originally designed for the FireGL professional graphics adapters but got extended soon for supporting any comparable mainstream board design from ATI Inc. and from third party board vendors. 

See also the [[VideoDrivers]] page for a list of other drivers for ATI hardware. 


### Driver Package

The rpm packaged drivers from ATI/AMD do provide those main components: 

* readme file or a set of documentation pages 
* DRI compatible driver module for 2D support 
* DRI compatible driver module for OpenGL support 
* kernel module (precompiled binaries plus source/lib for self building)
 
Other than that there are those minor components: 

* a set of install/uninstall scripts for usage by the rpm packaging 
* a customized x11 configuration tool 
* a binary control panel based upon QT toolkit and its sources 
* sample source for the gamma correction interface 
* sample application for OpenGL pbuffer rendering extension
 
The driver is hosted at <http://www.ati.com> - go to download, select your board and then the Linux operating system for getting to the matching page. If Linux is not listed for your specific board then that might just be a web update error - you should then try out the drivers for a similar board as there is really only one single driver for all boards. 


### Status

The driver is relatively mature and does perform quite well for professional OpenGL applications. Some gaming applications do work reasonably nice, some others might still expose some minor or major problems. 2D performance is below of what the open source DRI drivers do provide, but is still acceptable fast for everyday work. The set of drivers still is bound to XFree86 4.0.0 and X.Org 6.7.0, but might get updated to be compatible with the latest X.Org release. Further updates and inclusion of GLSL are expected for one of the very next releases. 

The current drivers are for Linux/i386 and Linux/x86_64 (AMD 64bit or Intel EM64T) platforms only, but there were some Linux/ia64 drivers available from the download page of some big OEM vendor. 

For more information and discussion on the current driver releases a visit of the <http://www.rage3d.com/> forums is recommended. 


### Support

Please note that Xorg developers are mostly unable to help with any issues with this driver. For discussions with other users, please use the #ati IRC channel on irc.freenode.net. 
