# sDVO ADD2 DVI Cards

The [[Intel Driver Documentation|http://intellinuxgraphics.org/documentation.html]] says:

   * You can purchase (for around $20) an 'sDVO' card which plugs into the PCI-E slot and offers DVI support in various flavors. This is just an adapter board which connects the onboard graphics chip with a DVI connector using the sDVO portion of the PCI-E standard; it's not an external graphics adapter.

There doesn't seem to be a lot of information about these cards, so I thought I would consolidate some of it here in the hopes that it helps other people looking for a solution.

At [[http://lwn.net/Articles/194821/|http://lwn.net/Articles/194821/]] Keith Packard from Intel said:

   * We've bought a huge pile of sDVO cards to work on the modesetting branch. One thing we discovered is that there are two flavors of sDVO cards, the ADD2-N and ADD2-R cards. The suffix indicates whether the card is set up in 'N'ormal or 'R'everse orientation; Normal cards use the first channels on the PCI-E connector while Reverse cards use the last channels.



   * All of the motherboards we have support only Normal cards, I guess the theory for Reverse cards is that you could plug in a PCI-E x4 card into the same PCI-E bus as one of the Reverse cards and have both things working at the same time.



   * And, all of the ADD2-N cards we've got work just fine with everything from 915 through 965.



   * I can see where the AGP-based ADD (not ADD2) cards would cause problems; they don't use a standard interface so the driver has to have support for each specific card. We're facing that with the modesetting branch and we're collecting information on as many of those as we can.



   * In summary - ADD2-N cards 'just work' everywhere we've tried them. ADD cards require card-specific driver changes.

According to [[one manufacturer|http://www.asisupport.com/newsletter_04_2005.htm#%22ADD2%22%20Cards%20-%20What%20exactly%20are%20they?]], ADD2-N cards are intended for ATX form factor boards while ADD2-R cards are intended for BTX form factor boards. That would be in line with Keith's comments above as I believe that all the Intel boards are ATX.


## Cards

Eric Anholt has a [[List of ADD2 Cards|http://web.archive.org/web/20071215081658/http://people.freedesktop.org/~anholt/intel-add-list.html]].  Please fill in any additional information about these cards, or their specifications, features, and compatibility here.


### ADD2 (SDVO) Cards

* ASUS DVI-ADD2 (DVI-D, Same as HP?)
      * [[Manufacturer Specs|http://support.asus.com/download/download.aspx?modelname=DVI-ADD2&SLanguage=en-us]]
* HP DY674A (DVI-D, Normal pinout)
      * [[Manufacturer Specs|http://h18000.www1.hp.com/products/quickspecs/12008_na/12008_na.HTML]]
      * [[Manufacturer Specs|http://h18000.www1.hp.com/products/quickspecs/11968_div/11968_div.HTML]] (shows pinout, search for model number)
* HP DY673A (DVI-D, Reverse pinout)
      * [[Manufacturer Specs|http://h18000.www1.hp.com/products/quickspecs/12008_na/12008_na.HTML]]
      * [[Manufacturer Specs|http://h18000.www1.hp.com/products/quickspecs/11968_div/11968_div.HTML]] (shows pinout, search for model number)
* MOLEX 79530-5004 (DVI, ADD2-R, 16x, UPC:8-41531-06000-8)
      * [[Manufacturer Specs|http://www.channel.molex.com/files/chnnl_usrgd_add2_Rev1.pdf]]
* MOLEX 79530-5005 (DVI, ADD2-N, 4x, UPC:8-41531-06001-5)
      * [[Manufacturer Specs|http://www.channel.molex.com/files/chnnl_usrgd_add2_Rev1.pdf]]
* MOLEX 79530-5006 (DVI, ADD2-R, 16x, UPC:8-41531-06002-2)
      * [[Manufacturer Specs|http://www.channel.molex.com/files/chnnl_usrgd_add2_Rev1.pdf]]
* MOLEX 79530-5007 (DVI, ADD2-N, 4x, UPC:8-41531-06003-9)
      * [[Manufacturer Specs|http://www.channel.molex.com/files/chnnl_usrgd_add2_Rev1.pdf]]
* Prolink ADD-7021-DVI-T (DVI, S-Video, ADD2-R/N?, CH7021 chipset)
      * [[Manufacturer Specs|http://www.prolink-usa.com/item_disp.php?upc=4711845391135]]
* Prolink ADD-7307-DVI-N (DVI, ADD2-N?, CH7307B chipset)
      * [[Manufacturer Specs|http://www.prolink-usa.com/item_disp.php?upc=4711845391173]]
* Prolink ADD-7307-DVI-R (DVI, ADD2-R?, CH7307B chipset)
      * [[Manufacturer Specs|http://www.prolink-usa.com/item_disp.php?upc=782515006004]]
* Prolink PV-CH7312 (DVI?, CH7312 chipset?)
      * [[Manufacturer Specs|http://www.prolink.com.tw/english/products/vga/PCX/ADD2%20Card.htm]]
* Prolink PV-CH7315 (DVI, HDMI?, CH7315 chipset?)
      * [[Manufacturer Specs|http://www.prolink.com.tw/english/products/vga/PCX/ADD2%20Card.htm]]
* DELL J4571 (DVI?, ADD2-N)
* Wintec 35111141 (DVI-I)
      * [[Manufacturer Specs|http://www.wintecind.com/pegasus.htm]]
* Wintec 35111142 (DVI-I, HDCP)
      * [[Manufacturer Specs|http://www.wintecind.com/pegasus.htm]]
* Wintec 35111140 (Dual DVI-I, standard cable)
      * [[Manufacturer Specs|http://www.wintecind.com/pegasus.htm]]
* Wintec 35111147 (DMS59, DVI, Y-cable)
      * [[Manufacturer Specs|http://www.wintecind.com/pegasus.htm]]
* Wintec 35111149 (HDMI)
      * [[Manufacturer Specs|http://www.wintecind.com/pegasus.htm]]
* Lenovo 73P2516 (DVI-I)
      * [[Manufacturer Specs|http://www5.pc.ibm.com/europe/products.nsf/$wwwPartNumLookup/_73P2516?OpenDocument]]
* Silicon Image Orion ADD2-N DUAL PAD x16 Card (Sil 1364 chipset, Same as ASUS?)
      * [[i965G (Asus P5B-V): Diagnosing ADD2/SDVO issues|http://lists.freedesktop.org/archives/xorg/2007-February/022184.html]]

### ADD (DVO) Cards

* Kontron ADD-LVDS 820935 (LVDS, CH7017 chipset)
      * [[Manufacturer Specs|http://www.kontron-emea.com/index.php?id=226&cat=188&productid=491]]
* Kontron ADD-DVI 720930 (DVI, TV, s-video/composite)
      * [[Manufacturer Specs|http://www.kontron-emea.com/index.php?id=226&cat=188&productid=490]]
* Kontron ADD-DVI-I/CRT 820942 (DVI-I, internal CRT, internal LVDS)
      * [[Manufacturer Specs|http://www.kontron-emea.com/index.php?id=226&cat=188&productid=1221]]
* HP DD505A (DVI-D, Sil164 chipset)
      * [[Manufacturer Specs|http://h18000.www1.hp.com/products/quickspecs/11443_na/11443_na.HTML]]
* AOpen  91.88V10.001 (DVI, TV, cvbs, s-video)
      * [[Manufacturer Specs|http://global.aopen.com.tw/products/mb/add.htm]]

## Other Mailing List Threads/Info

* [[Intel 965G - working&supported Add2 card with TV-out/S-video?|http://lists.freedesktop.org/archives/xorg/2007-February/021695.html]]
* [[Intel graphics driver -- thanks!|http://lists.freedesktop.org/archives/xorg/2007-February/021735.html]]
* [[Intel GMA graphics - any good for hi-def?|http://lists.freedesktop.org/archives/xorg/2007-February/022004.html]]
* [[Wikipedia sDVO page|http://en.wikipedia.org/wiki/SDVO]]
