# Organization of the X.Org security team


## How are security issues handled ?

Generally, security issues are reported to xorg-security@.  When CERT, iDefense or one of the other groups reports an issue, someone on the list takes the lead to co-ordinate it (this has typically been Matthieu or Alan).  The usual procedure follows: agree on an unembargo date, try to get it fixed, etc, etc. 

When we go public, announcements are made on [[the X.Org wiki Security page|Development/Security]], [[the xorg-announce mailing list|http://lists.x.org/mailman/listinfo/xorg-announce]], and [[the oss-security mailing list|http://oss-security.openwall.org/wiki/mailing-lists/oss-security]].

## Handling secrecy

xorg-security@ is a private list, and security related problems can be marked as private in gitlab (although this will restrict read access to X.Org team members on gitlab.freedesktop.org, not only to members of xorg-security@).


## How are the fixes tested and by who (before made public)?

As a number of vendors are members of the xorg-security list, we rely on tests done by them. Past experience has shown that they don't test things too much (they do read patches though). 
