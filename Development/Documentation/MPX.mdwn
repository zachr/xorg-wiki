This page lists the differences between X servers with MPX support and previous servers (in terms of input processing only). MPX is the in-server implementation to support multiple simultaneous input devices. The protocol-specific parts to MPX are included in the X Input Extension 2.0 (XI2).

[[!toc startlevel=2]]

## Device hierarchy

MPX features a two-layer device hierarchy. Any physical device is a "slave device", does not have a cursor and can only send XI events. Then there are "master devices", virtual devices that are represented by a cursor and a keyboard focus. MDs always come in pairs, one pointer, one keyboard. An SD can be attached to a MD. In this case any event by the SD is routed through the MD. To the client it appears that events come from the SD and from the MD (a client should never listen directly to SDs).

MDs generates core events and XI events. Creating a master device (_ChangeDeviceHierarchy_ request) creates a master pointer and a master keyboard. By default, two MDs exist: the virtual core pointer and the virtual core keyboard. All physical devices are attached to either of these to but this attachment can be changed.


## Input events

In earlier servers, all core events come from the virtual core pointer (VCP) or the virtual core keyboard (VCK). All device events from the device. It is not possible to XOpenDevice() the core pointer/core keyboard.

In MPX, SDs send XI events and MDs send core and XI events.


## ClientPointer

Each client can have one (master) pointer explicitly assigned. Each time pointer data is needed for request/reply handling, this _ClientPointer_ is chosen (e.g. XQueryPointer() will return the _ClientPointer_'s coordinates). The master keyboard paired with the _ClientPointer_ will be used for request/reply handling (e.g. XGetInputFocus()).

The _ClientPointer_ should only be used by the window manager to set the CP setting for traditional X applications. If you ever use XSetClientPointer() in your standard GUI application, you're most likely doing something wrong.

The general rule is that the _ClientPointer_ will work just as the traditional pointer in the given client. All other pointers interacting with the same client may experience difficulties (see _GrabOwnership_).

Inside the DIX, the _ClientPointer_ is realised with the _PickPointer_() and the _PickKeyboard_() functions (events.c). Both of which check the device list for available devices. If there is a physical device attached, then it is chosen as the _ClientPointer_ and returned. If no physical device is available, then the VCP is returned. A call to _PickPointer_() also _sets_ the _ClientPointer_, so subsequent calls to _PickPointer_() return the same pointer, unless changed by the client.

The _ClientPointer_ can be set with XSetClientPointer().


## Grabs

Each device can only be [[grabbed|GrabProcessing]] by one client at a time. If a client core-grabs the device, the _ClientPointer_ will be grabbed. Until the core grab is removed, no other client can grab this device (even XGrabDevice() will fail).

It works the other way around too. If a client uses XGrabDevice(), any XGrabPointer/Keyboard() on the same device will fail with _AlreadyGrabbed_.

While a grab is active, other clients may still get events from the other devices. The difference between core grabs and device grabs can be expressed as:

* A core grab guarantees a client that it doesn't get events from any other device.
* A device grab guarantees that no other client gets events from this device.

### Grab ownership

If a client core-grabs a device, no other device will send events to this client while the core grab is active. This is called grab ownership and was necessary to not confuse standard X apps.

This can be annoying, as clients usually grab the pointer for popup menus. Which in turn means that each time you get a popup, all other devices are deactivated until the popup is gone. Even worse, if you are not the _ClientPointer_, you won't even be able to use the popup and the _ClientPointer_ has to click on it. Use XSetClientPointer() to avoid this.

Grab ownership works different for passive grabs. A passive grab is stored on the window with the client's _ClientPointer_, but when the grab is activated (by pressing a button/key) the grab device is switched to the device that caused the grab to activate. The device now stays the same as long as the grab is active. If the client now starts an active grab, this active grab is put on the already grabbed device, not on the _ClientPointer_.

Grab ownership does not apply to device grabs.


### XGE

XI2 requires [[XGE]] for its events.

---
[[CategoryServerInternals]]
