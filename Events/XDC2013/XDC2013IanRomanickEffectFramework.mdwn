### Ian Romanick - An Effects Framework for OpenGL Testing

#### Links

* [[Slides|Effects-Framework-for-OpenGL-Testing.pdf]]
* Video: [[youtube|https://www.youtube.com/watch?v=Kw-lcoApz5s]], [[mp4|http://www.x.org/videos/XDC2013/ian_romanick-an_effects_framework_for_opengl_testing.mp4]]
