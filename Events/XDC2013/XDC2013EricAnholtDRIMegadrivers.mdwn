### Eric Anholt - DRI megadrivers

#### Links

* [[slides|xdc-2013-megadrivers.pdf]]
* videos: [[youtube|https://www.youtube.com/watch?v=0fJq-2haT3Y]], [[mp4|http://www.x.org/videos/XDC2013/eric_anholt-dri_megadrivers.mp4]]
