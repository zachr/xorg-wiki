# XDC2018 Program

* Welcome party
 * [[Tuesday|Program#tuesday]]

* Main Track
 * [[Wednesday|Program#wednesday]]
 * [[Thursday|Program#thursday]]
 * [[Friday|Program#friday]]

* Workshop Track
 * [[Wednesday|Program#workshops_wednesday]]
 * [[Thursday|Program#workshops_thursday]]
 * [[Friday|Program#workshops_friday]]

* Optional activity
 * [[Saturday|Program#saturday]] 

Remember to list your [[Lightning talks|Program#lightning]] and [[demo|Program#demo]] ideas.

<a name="tuesday"></a>
## Tuesday

[[!table header="no" data="""
19:30 - 23:00 | Welcome Party, sponsored by Igalia. Check-in / Badge pickup. [[More info|SocialEvents#tuesday_event]]
"""]]

<a name="main_track"></a>
## Main track

Main track talks are going to happen in the Auditorium, which is at the ground floor of the venue, in front of the main entrance.

Description of the talks can be found on the table or [[here|Talks]].

<a name="wednesday"></a>
### Wednesday

[[!table header="no" data="""
08:30 - 08:45 | Check In / Badge Pickup
08:45 - 09:15 | Opening session
09:15 - 10:00 | [[Jens Owens - Open Source Driver Development Funding Hooking up the Money Hose|Talks#jens]] ([[slides|https://xdc2018.x.org/slides/JensOwen-OSSDriverDevFunding.pdf]], [[video|https://www.youtube.com/watch?v=QF-xHTKY4H4]])
10:15 - 11:00 | <s>[[Keith Packard - Improving frame timing accuracy in Mesa, DRM and X|Talks#keith]]</s><b> (Cancelled) Lightning talks/demos.</b>
11:00 - 11:30 | Coffee Break
11:30 - 12:15 | [[Emil Velikov - A look towards a robust GPU device selection in GL|Talks#emil]] ([[slides|https://xdc2018.x.org/slides/device-selection-gl-emil.pdf]], [[video|https://www.youtube.com/watch?v=9WOgihsCKAA]])
12:30 - 13:15 | [[Jason Ekstrand - Vulkan Timeline Semaphores (break-out kick-off)|Talks#jason]] ([[slides pt1|https://xdc2018.x.org/slides/vulkan_timeline_semaphores_pt1.pdf]], [[slides pt2|https://xdc2018.x.org/slides/vulkan_timeline_semaphores_pt2.pdf]], [[video|https://www.youtube.com/watch?v=SpE--Rf516Y]])
13:15 - 14:45 | Lunch, sponsored by X.Org Foundation @ UDC
14:45 - 15:15 | [[Rodrigo Siqueira/Haneen Mohamed - VKMS|Talks#rodrigo_haneen]] ([[slides|https://xdc2018.x.org/slides/vkms_xdc.pdf]], [[video|https://www.youtube.com/watch?v=ber_9vkj_-U]])
15:30 - 16:00 | [[Nayan Deshmukh - Improve the Linux/DRM GPU scheduler to have dynamic scheduling|Talks#nayan]] ([[slides|https://xdc2018.x.org/slides/Nayan_XDC.pdf]], [[video|https://www.youtube.com/watch?v=XlKE4WMmn-o]])
16:00 - 16:30 | Break
16:30 - 17:30 | [[Demo Track|Program#demo]]
18:00 - 20:00 | A Coruña Old Town Guided Walking tour, sponsored by A Coruña's Tourism Office and Tranvías de La Coruña. [[More info|SocialEvents#wednesday_tour]]
20:00 - 22:00 | Stand-up dinner, sponsored by Igalia. [[More info|SocialEvents#wednesday_dinner]]
"""]]

<a name="thursday"></a>
### Thursday

[[!table header="no" data="""
08:45 - 09:00 | Opening Session
09:00 - 09:45 | [[Daniel Stone & Keith Packard - freedesktop.org update|Talks#daniel_keith]] ([[slides|https://xdc2018.x.org/slides/2018-09-26-xdc-fdo-update-daniels.pdf]], [[video|https://www.youtube.com/watch?v=s22B3E7rUTs]])
10:00 - 10:45 | [[Emil Velikov & Juan A. Suarez - Releasing and testing free opensource graphics drivers: the case of Mesa3D|Talks#emil_juan]] ([[slides|https://xdc2018.x.org/slides/release_testing_xdc2018.pdf]], [[video|https://www.youtube.com/watch?v=jEIJEz7JCks]])
10:45 - 11:15 | Coffee break
11:15 - 12:00 | [[Mark Janes & Clayton Craft - Mesa Continuous Integration at Intel|Talks#mark_clayton]] ([[slides|https://xdc2018.x.org/slides/Mesa_Continuous_Integration_at_Intel.pdf]], [[video|https://www.youtube.com/watch?v=nm3U7jaNJQQ]])
12:15 - 13:00 | [[Lionel Landwerlin - Performance Monitoring & Queries on Intel GPUs|Talks#lionel]] ([[slides|https://xdc2018.x.org/slides/lionel_XDC_2018.pdf]], [[video|https://www.youtube.com/watch?v=H1xS3d0yc8M]])
13:00 - 14:30 | Lunch, sponsored by X.Org Foundation @ UDC
14:30 - 15:15 | [[Jerome Glisse - getting rid of get_user_page() in favor of HMM|Talks#jerome]] ([[slides|https://xdc2018.x.org/slides/hmm-gup-no-more.pdf]], [[video|https://www.youtube.com/watch?v=_Z8NR0E0tyU]])
15:30 - 16:15 | [[Matt Turner - An Introduction to i965 Assembly and Bit Twiddling Hacks|Talks#matt]] ([[slides|https://xdc2018.x.org/slides/mattst88-XDC2018.pdf]], [[video|https://www.youtube.com/watch?v=raRBi-33Vz4]])
16:15 - 16:45 | Break
16:45 - 17:30 | [[Kenneth Graunke - Optimizing i965 for the Future|Talks#kenneth]] ([[slides|https://xdc2018.x.org/slides/optimizing-i965-for-the-future.pdf]], [[video|https://www.youtube.com/watch?v=XUis_0lMUBI]])
17:45 - 18:45 | X.Org Foundation Board Meeting @ Discussion room 2.1a

"""]]

<a name="friday"></a>
### Friday
[[!table header="no" data="""
08:45 - 09:00 | Opening Session
09:00 - 09:45 | [[Karol Herbst & Rob Clark - OpenCL support inside mesa through SPIR-V and NIR|Talks#karol_rob]] ([[slides|https://xdc2018.x.org/slides/clover.pdf]], [[video|https://www.youtube.com/watch?v=o-cJ_rS-9tQ]])
10:00 - 10:45 | [[Alejandro Piñeiro - ARB_gl_spirv implementation on Mesa: status update|Talks#alejandro]] ([[slides|https://xdc2018.x.org/slides/alejandro_2018-xdc-talk.pdf]], [[video|https://www.youtube.com/watch?v=XB3TPPNUX28]])
10:45 - 11:15 | Coffee Break
11:15 - 12:00 | [[Mauro Rossi - android-x86 status update and graphics stack evolution|Talks#mauro]] ([[slides|https://xdc2018.x.org/slides/XDC2018_Android-x86_Tech_Talk.pdf]], [[video|https://www.youtube.com/watch?v=O1D_XYaxs-c]])
12:15 - 13:00 | [[Lyude Paul & Alyssa Rosenzweig - Introducing Panfrost|Talks#lyude_alyssa]] ([[slides|https://xdc2018.x.org/slides/Panfrost-XDC_2018.pdf]], [[video|https://www.youtube.com/watch?v=qtt2Y7XZS3k]])
13:00 - 14:30 | Lunch, sponsored by X.Org Foundation @ UDC
14:30 - 15:15 | [[Elie Tournier - What’s new in the virtual world?|Talks#elie]] ([[slides|https://xdc2018.x.org/slides/Virgl_Presentation.pdf]], [[video|https://www.youtube.com/watch?v=RyEw27j4338]])
15:30 - 16:00 | [[Daniel Vetter - State of the X.Org|Talks#daniel]] ([[slides|https://xdc2018.x.org/slides/xorg-report-2018.pdf]], [[video|https://www.youtube.com/watch?v=ZTn8obiZ-8c]])
16:00 - 16:30 | Break
16:30 - 18:00 | [[Lightning Talks|Program#lightning]]
18:00 - 18:15 | Closing Session ([[video|https://www.youtube.com/watch?v=WCBaJaIHZK4]])
"""]]

<a name="saturday"></a>
## Saturday

[[!table header="no" data="""
09:30 - 18:30 | Sightseeing activity in Santiago de Compostela. [[More info|SocialEvents#saturday_event]]
"""]]

<a name="workshop_track"></a>
## Workshop track

One of the goals of the conference is to facilitate the discussions among the community members. We are going to have a discussion room in the venue that can be used for workshops, hacking, discussions, etc. The room will have desks, power sockets and a beamer/projector.

Where: room 2.1a, second floor of the same building of the venue.

Description of the workshops can be found on the table or [[here|Workshops]].

<a name="workshops_wednesday"></a>
### Wednesday
[[!table header="no" data="""
15:45 - ... | GPU offloading [[More info|Workshops#gpu_offloading]]
"""]]

<a name="workshops_thursday"></a>
### Thursday
[[!table header="no" data="""
10:00 - ... | Vulkan Timeline Semaphores [[More info|Workshops#vulkan_kick_off]]
14:30 - ... | Graphics Testing [[More info|Workshops#graphics_testing]]
"""]]

<a name="workshops_friday"></a>
### Friday
[[!table header="no" data="""
10:00 - ... | VKMS [[More info|Workshops#vkms]]
11:30 - ... | Graphics testing [[More info|Workshops#graphics_testing]]
"""]]


<a name="lightning"></a>
# Lightning Talks

5 minutes, summaries from formal workshop track discussions have priority, otherwise first come first serve (please list your talks here with name) until time is up.

## Wednesday morning 

### Jakob Bornecrantz - OpenXR, OpenHMD and XR general in FLOSS.

[Slides](https://people.collabora.com/~jakob/XR_FOSS.pdf) ([[Alternative source|https://xdc2018.x.org/slides/XR_FOSS.pdf]]), [[Video|https://www.youtube.com/watch?v=Y9S5O92mczU]]

Talk about the work OpenXR inside of Collabora and the future of XR FLOSS. 

### Neil Roberts - VkRunner: a simple Vulkan shader script test utility

[[Slides|https://xdc2018.x.org/slides/neil_roberts_vkrunner.pdf]], [[Video|https://www.youtube.com/watch?v=VRGseMUCQXA]]

A short presentation of VkRunner which is a tool to help create tests using shaders on Vulkan with a simple script to verify the results.

### Brian Starkey - Graphics buffer allocation

[[Slides|https://xdc2018.x.org/slides/XDC_allocation.pdf]], [[Video|https://www.youtube.com/watch?v=8sLICcNHiRI]]

What's the plan? Do we need a generic GEM ioctl?

### Zach Reizner - Exosphere, Chromium and crosvm

[[Video|https://www.youtube.com/watch?v=OZJiHMtIQ2M]]

A demonstration of the Linux GUI applications running on Chromebooks. The guest OS is run inside of a virtual machine and a custom virtio device tunnels the Wayland protocol to a host side Wayland compositor with minimal copies using guest-mapped dmabufs.

## Friday afternoon

### Rodrigo Siquera - VKMS Workshop

[[Video|https://www.youtube.com/watch?v=5iM2vuyPET4]]

Ideas for future VKMS work

### Martin Peres - GFX Testing Workshop

[Slides](https://docs.google.com/presentation/d/1Lhd-nynpuyaj_vsMzhbn4zj8bQhEYF50Na2iGv-2Yp0/edit?usp=sharing) ([[Alternative source|https://xdc2018.x.org/slides/GFX_Testing_Workshop.pdf]]), [[Video|https://www.youtube.com/watch?v=otnHWzDAc7I]]

A quick update on the consensus we reached at the Gfx Testing Workshop. 



### Kyle Brenneman - GPU offloading Workshop

[[Slides|https://xdc2018.x.org/slides/gpu_offloading_lightning_talk_slides.pdf]], [[Video|https://www.youtube.com/watch?v=4UEQLpPkosY]]

GPU offloading notes.

### Jason Ekstrand - Vulkan Timeline Semaphores Workshop

[[Video|https://www.youtube.com/watch?v=MxbYmut5wl0]]

Vulkan Timeline Semaphores Workshop.

### Erik Faye-Lund - Zink: OpenGL on Vulkan

[Slides](https://people.collabora.com/~kusma/zink-lightning-slides.pdf) ([[Alternative source|https://xdc2018.x.org/slides/zink-lightning-slides.pdf]]), [[Video|https://www.youtube.com/watch?v=ukrB-Lbl_Jg]]

A brief introduction to what and why Zink is, and some of the challenges in implementing OpenGL on top of Vulkan.

### Arkadiusz Hiler - IGT Update

[[Slides|https://xdc2018.x.org/slides/IGT_Update.pdf]], [[Video|https://www.youtube.com/watch?v=hmb-Z-DqPKE]]

Short update on recent changes in the IGT GPU Tools - the rename, the new runner and switching over to meson.

### Felix Kuehling - AMD Radeon Open Compute Platform

[Slides](https://drive.google.com/open?id=1ePlNzxYryveh6iFL-cqJO7ycSsYJ4LbC) ([[Alternative source|https://xdc2018.x.org/slides/AMD_Radeon_Open_Compute_Platform_lightning.pdf]]), [[Video|https://www.youtube.com/watch?v=e2ZSu5MydC4]]

A quick overview over ROCm, its architecture and applications.

### Ian Romanick - Value Range Tracking NIR

[[Slides|https://xdc2018.x.org/slides/ian_lightning_value-range-tracking.pdf]], [[Video|https://www.youtube.com/watch?v=o3C16C5E40g]]

Past, WIP, and future optimization efforts in NIR based on tracking the possible ranges of values.

### Carlos Santa - A low latency GPU engine based reset mechanism for a more robust UI experience

[[Slides|https://xdc2018.x.org/slides/carlos-santa-intel-tdr-xdc2018-.pdf]], [[Video|https://www.youtube.com/watch?v=DBxtvW9cpRU]]

### Manasi Navare - Driving futuristic resolution with Display Stream Compression on Intel Graphics

[[Slides|https://xdc2018.x.org/slides/Manasi_XDC_2018_final.pdf]], [[Video|https://www.youtube.com/watch?v=4UAjBJqyZN4]]

Talk about Display Port 1.4 spec's new feature of display stream compression required to drive high resolutions at higher frame rates and how its enabled on Intel Linux Graphics driver.

### Chad Versace - Vulkan DRM Format Modifiers

[[Video|https://www.youtube.com/watch?v=VkFQURcC5mM]]

A quick status update on the state of DRM format modifiers in Vulkan. Possibly a lightning API overview if time allows.

[prebuilt spec and headers](http://kiwitree.net/~chadv/vulkan/#1.0-VK_EXT_image_drm_format_modifier)

### Mark Janes - FrameRetrace

[[Video|https://www.youtube.com/watch?v=uO7uMd30Yrg]]

New features and platform support since last year's XDC

### Roman Gilg - Present buffer flips in Xwayland

[[Slides|https://xdc2018.x.org/slides/presentation_gsoc_project.pdf]], [[Video|https://www.youtube.com/watch?v=Wjs_Yoz5n_c]]

Report and outlook on a GSOC 2017 project on improving the usage of the Present extension in Xwayland.

<a name="demo"></a>
# Demo Track Ideas

10 minutes, first come first serve (please list your demos here with name), until time is up.

## Heinrich Fink - Optimizing Motion-to-Photon Latency on DAQRI Smart Glasses

[[Slides|https://xdc2018.x.org/slides/Heinrick_Fink_daqri_optimizing_motion_to_photon_latency.pdf]], [[Video|https://www.youtube.com/watch?v=d_KGN-xXbzE]]

Presentation on optimisation techniques / graphics requirements to
reduce motion-to-photon latency. Demo on device throughout the
conference in the hall-way.

## Drew DeVault - Building Wayland desktop components with layer shell

[[Video|https://www.youtube.com/watch?v=VuRXHJu5Kmg]]

Demonstration of the wlroots layer shell, examples of where it's useful, and why we think it's a good idea.
