# XDC2013: Portland, Oregon, USA
[[!table header="no" class="mointable" data="""
[[<< XDC 2012|Events/XDC2012]]  | **XDC 2013**  | [[XDC 2014 >>|Events/XDC2014]]
"""]]

* [[Attendees|Events/XDC2013/Attendees]] 
* [[Program|Events/XDC2013/Program]] 
* [[Social Events|Events/XDC2013/Social]]
* [[Proceedings and Recorded Videos|Events/XDC2013/Proceedings]]

<div itemscope itemtype="http://schema.org/Event">

<span itemprop="name">The 2013 X.Org Developer's Conference (XDC2013)</span>
is to be held from
<span itemprop="startDate" content="2013-09-23">September 23rd</span> through
<span itemprop="endDate" content="2013-09-25">25th, 2013</span>
in Portland, Oregon, USA.


## Venue

<div itemprop="location" itemscope itemtype="http://schema.org/Place">

The conference will be held in the Willamette conference room at the
<a href="http://www.uplacehotel.com/plan-your-event/" itemprop="url"><span
 itemprop="name">University Place Conference Center</span></a>
adjacent to the Portland State University campus.

<a href="https://maps.google.com/maps?q=University+Place+Hotel+%26+Conference+Center&hl=en&ll=45.507821,-122.680614&spn=0.010993,0.024312&cid=15069655939619738318&gl=US&t=m&z=16&iwloc=A" itemprop="map">Google Map</a>

</div>
</div>

## Program

The schedule is now posted on the [[program page|Events/XDC2013/Program]].

There were two ways to submit a presentation for this event: 

1. There was a [[Call for Proposals|Events/XDC2013/CallForProposals]]. The deadline for submission was August, 1, 2013. 
1. For more informal presentations you could add your talk to the [[program page|Events/XDC2013/Program]]. The deadline for submission here was September 20, 2013. 

## Registration

If you want to attend XDC2013 please add your name to the [[attendees page|Events/XDC2013/Attendees]]. Additionally please subscribe to the [[X.org events mailing list|http://lists.x.org/mailman/listinfo/events]] where we will post regular updates. 


## Accommodation

There are a number of affordable hotels in Portland (especially since we painstakingly avoided any week with a major trade show), many close to the conference venue. 

We have arranged a special room rate at University Place, the conference venue.


## Travel

The Portland Airport is close to the city and can be reached by local transportation in about 40 minutes. There are flights from Amsterdam and Tokyo, as well as many cities within the US. 


## Local transportation

Portland has an reasonable public transportation system: Most major locations in Portland can easily be reached by public transportation. 

Tickets are available from vending machines at any rail stop or aboard any bus (see also our hints on using public transportation). The conference venue and hotel can be reached on the Max Yellow line. 


## Local Dining

The conference venue is at the southern edge of the downtown area. There are many restaurants and food carts close-by as well as north towards the city center. Feel free to ask for recommendations on site. 


## Recordings

* [[Videos|http://www.x.org/videos/XDC2013/]]

## Travel Sponsoring

If you are not on a corporate budget but have something to present, please contact the X.Org Foundation Board of Directors [[board@foundation.x.org]] for travel sponsorship. 


## Further Inquiries

If you have any questions regarding the event please feel free to contact me at [[keithp@keithp.com]]. 


## Thanks

The organizers want to thank:

Our Helpers 

* Bart Massey for organizing the venue 
* Ian Romanick for organizing the paper committee 

## Links

* [[X.Org Conference/Event Anti-Harassment Policy|XorgFoundation/Policies/Harassment]]
