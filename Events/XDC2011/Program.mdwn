

# XDC 2011 program


## Schedule

**If you would like to add a talk or change information regarding your talk, please edit the Ideas section below the schedule or contact [[MichaelLarabel|MichaelLarabel]], and it will be added to the schedule or if anything needs to be re-arranged.** 

**11 September (Sunday; ~14.00+):** For anyone arriving into Chicago early, on Sunday at around 2:00PM (and will likely be there for several hours thereafter) there will be several X.Org developers meeting up at [[Snickers|http://www.yelp.com/biz/snickers-chicago]] ([[Google Maps|http://maps.google.com/maps/place?cid=17973169954687798724&q=Snickers+Chicago&gl=us&ved=0CBEQ-gswAA&sa=X&ei=q-JnToWkMZugMuv_4LYE]], a fairly traditional and decent American "dive bar". The location is quite ideal (located at 448 N State St and one block away from a Red line CTA train), there's several other bars and restaurants within easy walking distance to go after meeting up, a good selection of alcohol with decent prices, friendly bartenders, and we should have nearly the entire bar to ourselves. Several developers may also end up on there on Saturday. If any questions or more info contact [[MichaelLarabel|MichaelLarabel]]. 
[[!table header="no" class="mointable" data="""
 **12 September (Monday)** |||
 9:00   |  Welcome coffee - registration  |  [[MichaelLarabel|MichaelLarabel]] 
 10:00  |  Codebase of the future  |  [[JameySharp|JameySharp]] 
 11:00  |  Bringing In New Contributors  |  [[MatthiasHopf|MatthiasHopf]] 
 12:00  |  Lunch break ||
 14:00  |  XKB 1.1  |  [[DanielStone|DanielStone]] 
 15:00  |  Multi-Touch BoF / Wacom BoF  |  [[DanielStone|DanielStone]], [[PeterHutterer|PeterHutterer]], [[ChaseDouglas|ChaseDouglas]] 
 16:00  |  Driver Support BoF  |  Daniel Stone 
"""]]
[[!table header="no" class="mointable" data="""
 **13 September (Tuesday)** |||
 9:00   |  Welcome coffee  |  ```Coffee Table``` 
 9:10   |  Compiler Stacks  |  [[TomStellard|TomStellard]] 
 10:10   |  Nouveau  |  [[MartinPeres|MartinPeres]] 
 10:55  |  [[TimeGraph|TimeGraph]] / GPU Scheduling  |  [[ShinpeiKato|ShinpeiKato]] 
 11:30  |  LLVM In Mesa  |  Ian Romanick 
 12:30  |  Lunch break ||
 14:00  |  _Phoronix-Sponsored Beer Afternoon_ (~2:30PM to 5PM)  |   
 14:10  |  Graphics Memory Management  |  [[AlexDeucher|AlexDeucher]] 
 16:00  |  [[Contributing To X.Org & Open-Source Panel Discussion|http://www.phoronix.com/scan.php?page=news_item&px=OTg3NQ]]  |  [[MichaelLarabel|MichaelLarabel]] Panel: [[PeterHutterer|PeterHutterer]], [[MartinPeres|MartinPeres]], [[CorbinSimpsom|CorbinSimpsom]], [[KeithPackard|KeithPackard]], [[MattDew|MattDew]], [[KenGraunke|KenGraunke]] 
"""]]
[[!table header="no" class="mointable" data="""
 **14 September (Wednesday)** |||
 9:00   |  Welcome coffee  |  ```Coffee Table``` 
 9:10   |  DRM overlay plane  |  [[JesseBarnes|JesseBarnes]] 
 10:00  |  OpenGL 3.0  |  [[IanRomanick|IanRomanick]] 
 11:00  |  GPGPU  |  [[MartinPeres|MartinPeres]] 
 11:45  |  Lunch break ||
 14:00  |  Auto Regression Analysis  |  [[ShuangHe|ShuangHe]] 
 15:00  |  GPU kernel and userspace border  |  Jerome Glisse 
 16:00  |  Release Schedules  |  [[JeremyHuddleston|JeremyHuddleston]] 
 16:45  |   X.Org Foundation session, Conclusion (XDS2012 Ideas?)  |   
"""]]


## Ideas

* Multitouch BoF ([[DanielStone|DanielStone]], [[PeterHutterer|PeterHutterer]], [[ChaseDouglas|ChaseDouglas]]) 
   * Status, plans, etc 
* XKB 1.1 ([[DanielStone|DanielStone]]) 
   * Not XKB 2. 
* Auto regression Analysis ([[ShuangHe|ShuangHe]]) 
   * Introduction, plans 
* Nouveau ([[MartinPeres|MartinPeres]],[[ShinpeiKato|ShinpeiKato]]) 
   * Community, past, present and future work (30 minutes) ([[MartinPeres|MartinPeres]]) 
* [[Towards DRM-compliant GPU scheduling at the device driver level|http://rtml.ece.cmu.edu/projects/timegraph]] ([[ShinpeiKato|ShinpeiKato]]) 
   * To be scheduled after the Nouveau talk, if possible 
   * 30-45 minutes, to be scheduled on the 13th or the 14th 
* DRM overlay plane API ([[JesseBarnes|JesseBarnes]]) 
   * review of new ioctls, platform considerations 
* GPGPU (Informal talk? Proposed by [[MartinPeres|MartinPeres]]) 
   * What we can already do, what do we want and what needs to be done 
   * Meant as a follow-up of the OpenCL GSoC and the pscnv work 
   * A compute abstraction layer (driver-side): [[ShinpeiKato|ShinpeiKato]] 
      * 30 minutes, to be scheduled on the 13th or the 14th 
* Automated Per-Commit Testing Of Mesa Drivers ([[MichaelLarabel|MichaelLarabel]]) 
   * Introduction, Demonstration of Nouveau/Radeon/Intel 
   * Seek feedback, ideas 
* Driver support BoF 
* BoD BoF (X.Org [[BoardOfDirectors|BoardOfDirectors]]) 
* Release Schedules (Volunteer: [[JeremyHuddleston|JeremyHuddleston]], Voluntold: [[KeithPackard|KeithPackard]], [[AlanCoopersmith|AlanCoopersmith]]) 
      * Rolling drivers into main xserver? 
* Wacom BoF ([[PeterHutterer|PeterHutterer]]) 
      * Status, where to go from here? (probably not of interest to those not working on wacom) 
* LLVM as a requirement for Mesa ([[IanRomanick|IanRomanick]], discussion) 
   * It's useful for a lot more than generating code for shaders... 
* Compiler Stacks ([[TomStellard|TomStellard]]) 
   * Supporting OpenCL 
   * Using LLVM as an IR 
   * Better code generation for backends 
   * Discussion about the current status of various MESA IRs? 
   * Remaining refactors leading to using GLSL IR to represent ARB_vertex_program / ARB_fragment_program assembly shaders ([[IanRomanick|IanRomanick]]) 
* Codebase of the future! (discussion, [[JameySharp|JameySharp]]) 
   * X needs to do more in less code. What are our biggest cleanup targets? 
   * To kick things off: in xserver, I suggest unifying DDX and DIX as the big goal. 
* Contributing To X.Org & Open-Source [Panel Discussion, [[MichaelLarabel|MichaelLarabel]]] 
* The long, hard road to OpenGL 3.0 ([[IanRomanick|IanRomanick]]) 
* Methods of Attraction - How to gain more contributors ([[MatthiasHopf|MatthiasHopf]]) 