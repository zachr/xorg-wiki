A collection of older events that X.Org has held, presented at, or had significant presence at.  Move things here from the [[current events page|Events]] when they get too old, and if possible, link to what we did there. 

### 2017
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[X.Org Developer's Conference: XDC2017|XDC2017]]</span>,
   <link itemprop="url" content="https://www.x.org/wiki/Events/XDC2017/" />
   <span itemprop="location">Mountain View, California, USA</span>,
   <span itemprop="startDate" content="2017-09-20">September 20th</span> through
   <span itemprop="endDate" content="2017-09-22">September 22nd, 2017</span>.
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://fosdem.org/2017">FOSDEM 2017</a></span>,
   <span itemprop="location">Brussels, Belgium</span>,
   <span itemprop="startDate" content="2017-02-04">February 4</span> through
   <span itemprop="endDate" content="2017-02-05">February 5, 2017</span>.
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://lca2017.linux.org.au/">linux.conf.au 2017</a></span>,
   <span itemprop="location">Hobart, Tasmania, Australia</span>,
   <span itemprop="startDate" content="2017-01-16">January 16 through </span>
   <span itemprop="endDate" content="2017-01-20">January 20, 2017</span>.
  </span>

### 2016
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[X.Org Developer's Conference: XDC2016|XDC2016]]</span>,
   <link itemprop="url" content="https://www.x.org/wiki/Events/XDC2016/" />
   <span itemprop="location">Helsinki, Finland</span>,
   <span itemprop="startDate" content="2016-09-16">September 21st</span> through
   <span itemprop="endDate" content="2016-09-18">September 23rd, 2016</span>.
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://lca2016.linux.org.au/">linux.conf.au 2016</a></span>,
   <span itemprop="location">Geelong, Victoria, Australia</span>,
   <span itemprop="startDate" content="2016-02-01">February 1 through </span>
   <span itemprop="endDate" content="2016-02-05">February 5, 2016</span>
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://fosdem.org/2016/">FOSDEM 2016</a></span>,
   <span itemprop="location">Brussels, Belgium</span>,
   <span itemprop="startDate" content="2016-01-30">January 30th</span> through
   <span itemprop="endDate" content="2016-01-31">January 31st, 2016</span>.
   * [FOSDEM’16: Graphics DevRoom](https://fosdem.org/2016/schedule/track/graphics/) -- [Videos](http://video.fosdem.org/2016/)
   * Main track talk: [Vulkan in Open-Source](https://fosdem.org/2016/schedule/event/vulkan_graphics/)

### 2015

* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[X.Org Developer's Conference: XDC2015|XDC2015]]</span>,
   <link itemprop="url" content="http://www.x.org/wiki/Events/XDC2015/" />
   <span itemprop="location">Toronto, Canada</span>,
   <span itemprop="startDate" content="2015-09-16">September 16th</span> through
   <span itemprop="endDate" content="2015-09-18">September 18th, 2015</span>.
  </span>
* <span itemscope itemtype="http://schema.org/Event">
<span itemprop="name">[[FOSDEM 2015|https://fosdem.org/2015/]]</span>,
   <link itemprop="url" content="https://fosdem.org/2015/" />
   <span itemprop="location">Brussels, Belgium</span>,
   <span itemprop="startDate" content="2015-01-31">January 31 through </span>
  <span itemprop="endDate" content="2015-02-01">February 2, 2015</span>
   * [FOSDEM’15: Graphics DevRoom](https://fosdem.org/2015/schedule/track/graphics/) -- [Videos](http://video.fosdem.org/2015/devroom-graphics/)
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://lca2015.linux.org.au/">linux.conf.au 2015</a></span>,
   <span itemprop="location">Auckland, New Zealand</span>,
   <span itemprop="startDate" content="2015-01-12">January 12</span> through
   <span itemprop="endDate" content="2015-01-16">16, 2015</span>
  </span>
   * [Slides and Videos of X, DRM, & Mesa talks from LCA2015](http://lists.x.org/archives/xorg-devel/2015-January/045391.html)


### 2014

* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[X.Org Developer's Conference: XDC2014|XDC2014]]</span>,
   <link itemprop="url" content="http://www.x.org/wiki/Events/XDC2014/" />
   <span itemprop="location">Bordeaux, France</span>,
   <span itemprop="startDate" content="2014-10-08">October 8th</span> through
   <span itemprop="endDate" content="2014-10-10">10th, 2014</span>
  </span>
* <span itemscope itemtype="http://schema.org/Event">
<span itemprop="name">[[FOSDEM 2014|https://fosdem.org/2014/]]</span>,
   <link itemprop="url" content="https://fosdem.org/2014/" />
   <span itemprop="location">Brussels, Belgium</span>,
   <span itemprop="startDate" content="2014-02-01">February 1 through </span>
  <span itemprop="endDate" content="2014-02-02">2, 2014</span>,
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://lca2014.linux.org.au/">linux.conf.au 2014</a></span>,
   <span itemprop="location">Perth, Australia</span>,
   <span itemprop="startDate" content="2014-01-06">January 6</span> through
   <span itemprop="endDate" content="2014-01-10">10, 2014</span>
  </span>

### 2013

* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[X.Org Developer's Conference: XDC2013|XDC2013]]</span>,
   <link itemprop="url" content="http://www.x.org/wiki/Events/XDC2013/" />
   <span itemprop="location">Portland, Oregon, USA</span>,
   <span itemprop="startDate" content="2013-09-23">September 23rd</span> through
   <span itemprop="endDate" content="2013-09-25">25th, 2013</span>
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a href="http://www.linuxplumbersconf.org/2013/" itemprop="url">Linux Plumber's Conference 2013</a></span>,
   <a href="http://wiki.linuxplumbersconf.org/2013:graphics_and_display">Graphics and Display microconference</a>,
   <span itemprop="location">New Orleans, Louisiana, USA</span>,
   <span itemprop="startDate" content="2013-09-18">September 18</span> through
   <span itemprop="endDate" content="2013-09-20">20th, 2013</span>
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name">[[FOSDEM 2013|fosdem2013]]</span>,
   <link itemprop="url" content="https://fosdem.org/2013/" />
   <span itemprop="location">Brussels, Belgium,
   <span itemprop="startDate" content="2013-02-02">February 2 through
   <span itemprop="endDate" content="2013-02-03">3, 2013</span>
  </span>
* <span itemscope itemtype="http://schema.org/Event">
   <span itemprop="name"><a itemprop="url" href="http://lca2013.linux.org.au/">linux.conf.au 2013</a></span>,
   <span itemprop="location">Canberra, Australia</span>,
   <span itemprop="startDate" content="2013-01-28">January 28</span> through
   <span itemprop="endDate" content="2013-02-02">February 2, 2013</span>
  </span>

### 2012

* [[X.Org Developer's Conference: XDC2012|Events/XDC2012]], Nuremberg, Germany, September 19 through 21, 2012 
* [[Book Sprint 2012|Events/BookSprint2012]], Nuremberg, Germany, September 17 and 18, 2012 
* [[GUADEC 2012|http://www.guadec.org/]], A Coruña, Spain, July 26 through August 1, 2012 
* [[FOSDEM 2012|fosdem2012]], Brussels, Belgium, February 4 through 5, 2012 
   * [[X/Graphics Dev Room Call for Participation|http://lists.x.org/archives/xorg-devel/2011-September/025000.html]], [[Followup|http://lists.x.org/archives/xorg-devel/2011-October/025953.html]] 
   * [[X.Org/Wayland/OpenICC Dev Room schedule|https://archive.fosdem.org/2012/schedule/track/xorgopenicc_devroom.html]] 
* [[linux.conf.au 2012|http://lca2012.linux.org.au/]], Ballarat, Australia, January 16 through 20, 2012 

### 2011

* [[X.Org Developers' Conference (XDC2011)|Events/XDC2011]]. Chicago, USA, September 12 through 14, 2011. 
* [[Linux Plumbers Conf 2011|http://www.linuxplumbersconf.org/2011/]], Santa Rosa, California, USA, September 7 through 9, 2011 
* [[GNOME/KDE Desktop Summit|http://www.desktopsummit.org/]], Berlin, Germany, August 6 through 12, 2011 
* [[linux.conf.au 2011|http://lca2011.linux.org.au/]], Brisbane, Australia, January 24 through 29, 2011 

### 2010

* [[Linux Plumber's Conference|http://events.linuxfoundation.org/events]], Cambridge, Massachusetts, USA, November 3 through 5, 2010 
* [[X.Org Developers' Summit (XDS2010)|Events/XDS2010]]. Toulouse, France, September 16 through 18, 2010. 
* [[GUADEC 2010|http://www.guadec.org/index.php/guadec/2010]], The Hague, The Netherlands, July 24 through 30, 2010 
* [[Akademy 2010|http://akademy.kde.org/]], Tampere, Finland, July 3 through 10, 2010 
* [[FOSDEM 2010|fosdem2010]], Brussels, Belgium, February 6 and 7, 2010 
* [[linux.conf.au 2010|http://www.lca2010.org.nz/]], Wellington, New Zealand, January 18 through 23, 2010 

### 2009

* [[VideoHackfest|http://gstreamer.freedesktop.org/wiki/VideoHackfest]], Barcelona, Spain, November 19th through 22nd, 2009 
* [[X.Org Developers' Conference 2009|Events/XDC2009]], Portland, Oregon, USA, September 28 through 30 
* [[Linux Plumber's Conference|http://linuxplumbersconf.org/2009/]]: [[X Window System track|http://linuxplumbersconf.org/ocw/events/2009/tracks/3]], Portland, Oregon, USA, September 23 through 25 
* [[Gran Canaria Desktop Summit: GUADEC & Akademy 2009|http://www.grancanariadesktopsummit.org/]], Las Palmas, Gran Canaria, July 3 through 11, 2009 
* [[FOSDEM 2009|fosdem2009]], Brussels, Belgium, February 5 through 8, 2009 
* [[linux.conf.au|http://linux.conf.au/]], Hobart, Tasmania, January 19 through 24, 2009 

### 2008

* [[Gnome Boston Summit|http://live.gnome.org/Boston2008]], Cambridge, Massachusetts, October 10 through 13. 
* [[X.Org Developers' Summit 2008|Events/XDS2008]], Edinburgh, Scotland, September 3 through 5. 
* [[GUADEC 2008|http://2008.guadec.org/]], Istanbul, Turkey, July 7th through 12 
* [[X.Org Developers' Conference 2008|Events/XDC2008]], Mountain View, California, April 16 through 18. 
* [[xorg@fosdem 2008|fosdem2008]], February 23 - 24. Brussels, Belgium. 
* [[linux.conf.au|http://linux.conf.au/]], Melbourne, Australia, Jan 28 - Feb 2 

### 2007

* [[Gnome Summit|http://live.gnome.org/BostonSummit]], Cambridge, Massachusetts, October 6 through 8. 
* [[X.Org Developers' Summit 2007|Events/XDS2007]], Cambridge, England, September 10 through 12. 
* [[GUADEC 2007|http://2007.guadec.org/]], Birmingham, England, July 15 through 21. 
* [[xorg@fosdem 2007|fosdem2007]], February 24-25. Brussels, Belgium. 
* [[Xorg Developer's Conference 2007|Events/XDC2007]] was held on February 7, 8, and 9 in the San Francisco Bay Area. See [[XDC2007 Notes|Events/XDC2007/Notes]] for the talk transcript.

## 2006

* An X.Org BOF was held at [[SIGGRAPH 2006|http://www.siggraph.org/s2006/index.php]] (August 1 through 3, 2006) in Boston. 
* The [[Desktop Developers' Conference|http://lanyrd.com/2006/desktop-developers-conference/]] was held July 17 and 18, 2006, in Ottawa prior to OLS 2006. 
* X.Org presented at [[LinuxTag|LinuxTag]] 2006 in Wiesbaden, Germany May 3 - 6 2006. 
* X.Org had an [[X Developers HotHouse and X DevRoom|fosdem2006]] on February 24-26 at FOSDEM 2006 in Brussels, Belgium. 
* [[X Developers' Conference 2006|Events/XDC2006]] was held on February 8-10 2006 at the Sun Microsystems campus in Santa Clara, CA. 

## 2005

* [[Desktop Developers' Conference|http://lanyrd.com/2005/desktop-developers-conference/]], July 18 and 19, 2005, Ottawa, prior to OLS 2005. 
* X.Org had its first European Developers meeting in Karlsruhe, Germany from June, 19 to June, 20, right before [[LinuxTag 2005|http://www.linuxtag.org]]. (Read more at [[LinuxTagMeeting2005|LinuxTagMeeting2005]]) 
* [[X Developers' Conference 2005|Events/XDC2005]], February 12 through 14, in Cambridge, Massachusetts. 

## 2004

* [[Desktop Developers' Conference|http://lanyrd.com/2004/desktop-developers-conference/]], July 19 and 20, 2004, Ottawa, prior to OLS 2004. 
* The first [[X Developers' Conference|Events/XDC2004]] (well, first for the X.Org Foundation) was April 28 through 30, 2004 in Cambridge, Massachusetts. 
