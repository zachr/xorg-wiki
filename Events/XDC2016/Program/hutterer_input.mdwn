# Peter Hutterer - (lib)?input update

[[Slides|xdc2016_input.html]], [[Video|https://youtu.be/4PflCyiULO4?t=2h06m20s]]

The usual update on what's going on in the input stack, primarily libinput.
