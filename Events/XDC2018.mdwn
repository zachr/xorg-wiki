# XDC2018: A Coruña, Galicia, Spain.
[[!table header="no" class="mointable" data="""
[[<< XDC 2017|/Events/XDC2017]]  | **XDC 2018**  | [[XDC 2019 >>|/Events/XDC2019]]
"""]]

<div style="float: right; background: #ffffdd; border: 1px solid grey; border-radius: 5px; padding: 5px; margin: 5px; font-family: sans-serif; line-height: 120%" align="center">
<div style="padding-bottom: 5px;"><i>Get XDC updates on:</i></div>
<a href="https://plus.google.com/102057777074369211973" rel="publisher"><img src="/wiki/google+.png" style="border:0;width:34px;height:34px;" alt="Google+" title="Google+"/></a>
<a href="https://twitter.com/xdc2018" rel="publisher"><img src="/wiki/twitter.png" style="border:0;width:34px;height:34px;" alt="Twitter" title="Twitter"/></a>
<a href="https://www.youtube.com/channel/UCXlH5v1PkEhjzLFTUTm_U7g" rel="publisher"><img src="/wiki/youtube.png" style="border:0;width:34px;height:34px;" alt="YouTube" title="YouTube"/></a>
</div>

* <a href="https://xdc2018.x.org">Website</a>
* [[Call For Proposals|/Other/Press/CFP2018/]]
* [[Attendees|/Events/XDC2018/Attendees]]
* [[Program|/Events/XDC2018/Program]]
* [[Social Events|/Events/XDC2018/SocialEvents]], can be checked out from the [[program|/Events/XDC2018/Program]] too.

<div itemscope itemtype="http://schema.org/Event">

<span itemprop="name">The 2018 X.Org Developer's Conference (XDC2018)</span>
is being hosted by GPUL (Galician Linux User Group) in A Coruña, Spain, from the 26th through the 28th of September, 2018. The event will be held at Computer Science Faculty of University of A Coruña, Campus de Elviña, 15071, A Coruña, Galicia (Spain). See [[map|https://www.openstreetmap.org/?mlat=43.33287&mlon=-8.41088#map=18/43.33287/-8.41088]].
</div>

## Livestream & recordings

The livestream of the recording is provided by University of A Coruña. [[Watch it here|https://tv.udc.gal/live/5b99033e11cad1f7358b458a]].

The recordings will be uploaded after the conference to Youtube.

## Registration
The conference is entirely free of charge. If you want to attend XDC2018 please add your name to the [[attendees page|Events/XDC2018/Attendees]]. Additionally please subscribe to the [[X.Org events mailing list|http://lists.x.org/mailman/listinfo/events]] where we may post updates.

### VISA invitation letter

If you are attending to XDC2018 at A Coruña, Spain and you need a Visa Invitation please send us all the needed data before Monday, 23rd July to [[xdc2018@gpul.org]] with the subject "Visa Invitation Request".

Once we got it, the process will be:

* The XDC2018 team will process it and prepare the letters
* The University of a Coruña will sign the letters
* We will send you a scan of the signed letter via mail and if needed the original letter via post. 

We will sent by email the first batch of letters at the end of July. Requests received after that date are expected to be send at the beginning of September because in August the University is on holidays so we don't have guarantee that the Dean is available for signing them. So if you need the letter better being in the first batch.

The data we need is:

- full name as stated in passport: 
- date and place of birth: 
- nationality / country of origin: 
- passport number: 
- registered address:
- if an actual letter via snail mail is needed, an address to which
the letter can be sent:
- arrival and departure dates for XDC: 
- email address for potential questions: 
- affiliation: 
- your interest in XDC to motivate the invitation (member of the community, attending as speaker, etc).
- any particular requirement of the local Spain embassy: 

## Accomodation

There are plenty of [[hotels in the city|https://www.google.com/maps/d/viewer?ll=43.365794275165776%2C-8.396322510055711&spn=0.088628%2C0.09922&msa=0&z=15&mid=1HIxsUvvQ6C0tGyswFwxF-wSXNVI]] center for all budgets, we recommend to stay in a hotel in the city center as there are many options for dinner and for enjoying the night life of the city. There is public transport to go to the venue from the city center ([[UDC line|http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=udc]], see its [[itinerary in Google|https://www.google.com/maps/dir/Pr.+Pontevedra,+A+Coru%C3%B1a/Facultad+de+Inform%C3%A1tica+-+UDC,+Campus+de+Elvi%C3%B1a,+15071+A+Coru%C3%B1a,+La+Coru%C3%B1a/@43.3502143,-8.4255692,14z/data=!3m1!4b1!4m16!4m15!1m5!1m1!1s0xd2e7c62b69b0a77:0xf4b600525364275a!2m2!1d-8.4077923!2d43.3677294!1m5!1m1!1s0xd2e7cb3d82865e7:0x426ea83b7a5a451f!2m2!1d-8.410889!2d43.332845!2m1!5e0!3e3?hl=en]]).

## Code of conduct
XDC is meant to be a technical conference where everyone can enjoy themselves. To this end, we will ask you to be kind to each-others and check out our [[Anti-Harassment Policy|XorgFoundation/Policies/Harassment]].

Breaches of the code of conduct should be reported in one of the following ways:

* Send an email to any member of the commission:

 * Manasi Navare [[manasi.d.navare@intel.com|mailto:manasi.d.navare@intel.com]].
 * Juan A. Suarez [[jasuarez@igalia.com|mailto:jasuarez@igalia.com]].
 * Daniel Vetter [[daniel.vetter@ffwll.ch|mailto:daniel.vetter@ffwll.ch]].


* Call to this phone number: +34 658 185 656.

## Meals

X.Org Foundation is sponsoring the lunch during the days of the conference to the registered attendees.

The canteen is 300 meters far away from the venue, see [[itinerary|https://www.google.com/maps/dir/Facultade+de+Inform%C3%A1tica+-+UDC,+Campus+de+Elvi%C3%B1a,+15071+A+Coru%C3%B1a,+La+Coru%C3%B1a/Facultade+de+Econom%C3%ADa+e+Empresa+-+UDC,+Campus+de+Elvi%C3%B1a,+15071+A+Coru%C3%B1a/@43.3320649,-8.4140789,17z/data=!4m14!4m13!1m5!1m1!1s0xd2e7cb3d82865e7:0x426ea83b7a5a451f!2m2!1d-8.410889!2d43.332845!1m5!1m1!1s0xd2e7cb6cc1c8e5f:0x8ccd860452c5850a!2m2!1d-8.4130001!2d43.3310393!3e2]].

There will be options for vegetarians and vegans. However, if you have any kind of food allergy, dietary restriction or similar, please contact the organization as soon as possible by email ([[xdc2018@gpul.org]]) or at the registration desk of the conference.

## Travel

The nearest airport is A Coruña airport (LCG), which is connected to Madrid, Barcelona, Lisbon, London/Heathrow. To get to the venue using public transport from A Coruña Airport, use the Airport-City Center Bus line ([[Line 4051 Puerta Real - Aeropuerto|http://autoscalpita.com/wp-content/uploads/2018/04/ASC-Horarios-Abril-2018.pdf]]), stopping at Bus Station (Estación de Autobuses), from there you can get a taxi or taking a bus nearby to the venue ([[UDC bus line|http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=udc]]).

Another near airport is Santiago de Compostela airport (SCQ), which is connected to Madrid, Barcelona, Brussels, Dublin, Frankfurt, Istanbul, Geneva, Basel/Mulhouse, Milan/Bergamo, Paris/CDG. It is about 50km from Coruña. It's a good option if you have a direct flight, but requires taking the bus from the airport to the Santiago train station and then take a train to Coruña.

There are also trains to A Coruña from Madrid and Barcelona. See [[renfe.com|http://www.renfe.com/EN/viajeros/index.html]] website for more details about train tickets and schedules.

There are buses going to A Coruña from different cities of Spain, see [[Alsa.com|https://www.alsa.com/en/web/bus/home]] for more details.

If you want more info about traveling to A Coruña and its public transportation, check this [[page|https://xdc2018.x.org/#venue]].

### Travel Sponsoring
If you are not on a corporate budget but have something to present, please contact the X.Org Foundation Board of Directors [[board@foundation.x.org]] for travel sponsorship.  (See [[/Events]] for details.)

## Sponsors
### Platinum sponsor
<img src=https://xdc2018.x.org/img/logos/logo-igalia.png title="Igalia" alt="Igalia Logo" width="320">

### Gold sponsors
<img src=https://xdc2018.x.org/img/logos/logo-amd.png title="AMD" alt="AMD Logo" width="180">

<img src=https://xdc2018.x.org/img/logos/arm-logo.png title="ARM" alt="ARM Logo" width="180">

<img src=https://xdc2018.x.org/img/logos/logo-collabora.png title="Collabora" alt="Collabora Logo" height="80">

<img src=https://xdc2018.x.org/img/logos/logo-google.png title="Google" alt="Google Logo" width="180">

<img src=https://xdc2018.x.org/img/logos/logo-intel.png alt="Intel Logo" height="80">

<img src=https://xdc2018.x.org/img/logos/logo-nvidia.png alt="NVIDIA Logo" height="80">

<img src=https://xdc2018.x.org/img/logos/logo-valve.png alt="Valve Logo" width="180">

### Host sponsoring

<img src=https://xdc2018.x.org/img/logos/logo-gpul.png alt="GPUL Logo" height="80">

<img src=https://xdc2018.x.org/img/logos/logo-udc.png alt="UDC Logo" width="180">

#### Collaborators

<img src=https://xdc2018.x.org/img/logos/ayuntamiento_de_a_coruna.jpg alt="Corunha's Town Hall Logo" width="100">

<img src=https://xdc2018.x.org/img/logos/turismocorunha_logo.png alt="Corunha's Tourism Office Logo" width="100">

<img src=https://xdc2018.x.org/img/logos/tranvias_corunha_logo.png alt="Tranvías Corunha Logo" width="80">

<img src=https://xdc2018.x.org/img/logos/logo_xunta.png alt="Xunta Galicia Logo" width="100">

<img src=https://xdc2018.x.org/img/logos/logo_amtega.jpg alt="AMTEGA Logo" width="100">

## Contact the organization

Please send an email to [[xdc2018@gpul.org]]. There is also a [[Twitter account|https://twitter.com/xdc2018]] for real-time communications.
