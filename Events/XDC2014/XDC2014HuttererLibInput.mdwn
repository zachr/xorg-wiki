# Peter Hutterer - Consolidating the input stacks with libinput

In the X world we have the server providing us with much of the higher-level concepts of the input stack and a number of input modules for some device-specific functionality (wheel emulation, tapping, etc.). Communication between those parts is difficult and almost nonexistent.

Wayland requires the input stack to be re-written for every compositor. To avoid that, we have created libinput, a library that provides almost the whole stack of input processing, across all devices. The compositor's input stack is a thin wrapper around libinput, limited to the actual semantic handling of the events.

This talk outlines libinput, talks about what we can already do, what we can't do yet and why not, and what we may be able to do once someone tells us what to do in the first place.

[[Slides|xdc-2014.html]]
[[Video|http://www.x.org/videos/XDC2014/HuttererLibInput.webm]]

<iframe width="420" height="315" src="//www.youtube.com/embed/vxhdba4RS8s" frameborder="0" allowfullscreen></iframe>
