# Matt Turner - GLSL compiler: Where we've been and where we're going

The generated code quality of the GLSL compiler (i965 backend included) has improved significantly in the last year without significant infrastructure additions. How do we measure our progress and how do we tell what we still need to improve?

Can we continue making significant improvements without adding significantly to our compiler infrastructure? (Probably not)

We know that we want an SSA-based compiler, but we have a number of options to get us there. How do we properly evaluate our options and continue making significant improvements to our generated code?

[[Slides|glsl_compiler.pdf]]
[[Video|http://www.x.org/videos/XDC2014/TurnerGLSL.webm]]

<iframe width="560" height="315" src="//www.youtube.com/embed/3C96NJeYAy4?list=PLe6I3NKr-I4KG8LUga3KamQlCFb6Cc843" frameborder="0" allowfullscreen></iframe>
