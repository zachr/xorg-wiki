

### Development State and Features of the Joystick Input Module


### Sascha Hlusiak

The joystick input module was part of the old XFree86 distribution, but the driver was in a bad state until 2007, when development was pushed forward. It first supported the Linux joystick kernel device only, later on support for evdev devices was added, which added support for the driver to be hotplugged by hal enabled server. A FreeBSD kernel backend is available as well, but like Solaris, this is not a prominent system on workstations. Today the joystick driver is well hotpluggable using udev and supports many features and configuration possibilities, like generating mouse button events, pointer movement or key strokes. Enabling the driver to act as both, a pointer and a keyboard device, was particularly challenging. Having a second keyboard device with a different keyboard layout was a problem too, but situation improved with recent servers and XI2/MPX. Support for properties enable 3rd party programs to be written that change the configuration of the device on demand. Prominent target platforms are Linux on the PS3 or the XBox. 

Slides: [[xf86-input-joystick.pdf|xf86-input-joystick.pdf]] 

Presentation: [[http://www.youtu.be/Ul2em93dpX4|http://www.youtu.be/Ul2em93dpX4]] 
