

### How Neuro-Scientists Use the Open Source Graphics Stack: some Examples


### Mario Kleiner

This talk gives some examples  on how neuro-scientists / brain-researchers use computer-graphics for conducting experiments on human perception and cognition, and what kind of sometimes slightly special requirements this imposes on the graphics stack in terms of features, robustness, accuracy and control and what the pain points and wishes of the developers and users of such research software are. Basically we will talk about how one can make live better (or worse) for developers/users of such research software. 
