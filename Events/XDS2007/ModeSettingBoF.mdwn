This page is to get a preliminary idea of what people want to discuss at the [[ModeSetting]] BOF at XDS.

The BOF will mostly be concerned with the push to add modesetting to kernel space on Linux platforms, which is useful for suspend/resume and also for smooth GUI bootup.

* [[DavidAirlie|DavidAirlie]]:
  1. Bash out the correct API level, at the moment modesetting-101 possibly does too much in-kernel.
  2. Add a control drm device node that is persistent.
  3. Userspace daemon vs none.

---

* [[JakobBornecrantz|JakobBornecrantz]]:
  1. Persistent- vs dynamic memory allocation for scanout buffers.

---

* [[SylvainBertrand|SylvainBertrand]]:
  1. suspend/resume/power management kernel interfaces to implement are specific to each driver model. Creating a DRM interface which would suit all driver models may not be reasonable. Another approach would be to have driver model specific code (with all the suspend/resume/power management kernel interfaces implementations) and a perfectly orthogonal DRM interface (As far as I understood current DRM, it provides almost a full blown driver model abstraction layer, I may be wrong though). The mode setting interface would be part of the "perfectly orthogonal DRM interface".
  2. What's up with ACPI model (see ACPIspec30a.pdf pages 591&592)?

---

* [[JesseBarnes|JesseBarnes]]:
  * my main concern is the current fb layer.  The current code & design duplicates a lot of fb functionality (getting mode lists, setting modes, exporting interfaces to userland).  How much duplication is acceptable?  Should we extend the fb APIs or accept the duplication?  Should fb be obsoleted entirely by this new code?  Should we share code?  If the answer to the last question is no, I think we can expect a lot of pushback from the fb and broader kernel community...

---

* [[JeromeGlisse|JeromeGlisse]]:
  1. Do we want to emulate framebuffer in kernel ? The scanout buffer format might be a little bit complicated to handle in kernel (stride, tiles, ...) and we may want to avoid to put such knowledge in the kernel. Then how kernel can tell you when anythings when it panics (asking for the modesetting to provide a failsafe and easy to use mode might help but also might not work in all case). This was already discussed a bit on IRC and mail.

 2. IOCTL versioning versus tag based arguments. Maybe it's good to think now to possible interface change rather than find out in couple of month, years that changing the interface is painfull (as it is now).

 3. For power management i would like to see the kernel exporting each hw capacity through somethings like HAL so we can then use some project like ohm which i believe is aimed to centralize all this power things. But i don't think this really fit modesetting topic and likely we could already work on this without drastic change in drm.

---
