# XInput Hotplug
[[!table header="no" class="mointable" data="""
 &#x26A0; The following description is quite out of date and left for historical reference only - the DBus API described below was later rev'ed to version 2, then deprecated in favor of [[HAL|XorgHAL]], which itself was deprecated in favor of udev on Linux.
"""]]


## Table of Contents

[[!toc startlevel=2]]


## Overview

The X.org server supports hotplugging input devices since [[November 2006|http://lists.freedesktop.org/archives/xorg/2006-October/019007.html]] ([[X11R7.2|Releases/7.2]] will NOT have hotplug support yet).


## Design

The X.server uses DBus to get information about devices that are to be added or removed. From [[http://cgit.freedesktop.org/xorg/xserver/commit/config/dbus-api?id=ec35e7198debf938f1115f584e675ce5995743e3]]


[[!format txt """
D-BUS Configuration API v0.1
----------------------------

The X server will register the bus name org.x.config.displayN, and the
object /org/x/config/N, where N is the display number.


Currently only hotplugging of input devices is supported.

org.x.config.input:
    org.x.config.input.add:
        Takes an argument of key/value option pairs in arrays, e.g.:
         [ss][ss][ss][ss]
        is the signature for four options.  These options will be passed
        to the input driver as with any others.
        Option names beginning with _ are not allowed; they are reserved
        for internal use.

        Returns one int32, which is an X Status, as defined in X.h.  If
        everything is successful, Success will be returned.  BadMatch will
        be returned if the options given do not match any device.  BadValue
        is returned for a malformed message.

        Notably, BadAlloc is never returned: the server internally signals
        to D-BUS that the attempt failed for lack of memory.

        The return does not notify the client of which devices were created
        or modified as a result of this request: clients are encouraged to
        listen for the XInput DevicePresenceNotify event to monitor changes
        in the device list.

    org.x.config.input.remove:
        Takes one int32 argument, which is the device ID to remove, i.e.:
         i
        is the signature.
        Same return values as org.x.config.input.add.

"""]]
The main idea behind keeping the discovery mechanism out of the server was that different systems can use different mechanisms.  On GNOME or KDE desktops it would make good sense to use [[HAL|http://freedesktop.org/wiki/Software/hal]] for detection (which is a cross-platform device detection and enummeration system), but an embedded system may well want to use something more lean.

Also, by moving the mechanism to an X client daemon, you can implement desktop specific policy in the daemon. For example, in the GNOME environment you could have a daemon that reads per user settings for input devices from GConf.


## XInput Protocol

A event was added _devicePresenceNotify_ to notify a client about new devices.  XI has been bumped to version 1.5


## Code

 * Simple, experimental device manager in C: git://anongit.freedesktop.org/users/daniels/respeclaration

## Other Stuff

 * The design of XOrg XINPUT drivers is undocumented, and inconsistent. The main driver Control() functions are not being used correctly, with PreInit() doing much of the work for DEVICE_INIT and DEVICE_ON.
      * Main.[[JoeKrahn|JoeKrahn]] suggests: combining the various other driver export functions into additional deviceControl() operations to create as a single driver access point, rather than Probe(), PreInit(). This function could also be used to replace the numerous standard driver-name symbols like _DriverName_Options. This would require a change to the module ABI, but would be, IMHO, much simpler and cleaner, and allow for things like returning an altered option list, based on the hardware.

## Issues

 * Security: allowing any client to ask the server to open a file as an input device is problematic.  E.g. adding a mouse and specifying /etc/passwd as the device file.
 * Backwards compatibility: GTK+ doesn't handle disappearing XInput devices well.  One option is to keep a shadow list that doesn't change, and export that to clients that haven't asked for a recent enough version of XInput.
 * Remote devices: how well does this integrate with remote input devices?
 * DBus' behaviour to abort the process (using assert()) if an error in the library occurs. This behaviour was considered inappropriate by Daniel Stone, with announcements that it may cause DBus to be removed again [[http://lists.freedesktop.org/archives/dbus/2006-November/006390.html]].

# See Also

 * [[XInputSpec]] - Related updates for the XInput extension.
 * [[XOrgInputDriverSpec]] - Input Driver module design.
 * [[XHotplugProposal]] - Initial proposal for the hotplug design.

-- Created by Main.[[KristianHøgsberg]] - 02 Jul 2004
