

# r600_demo - 3D bringup tool for AMD GPG r6xx/r7xx Chipsets

r600_demo is a bringup tool (read: only something for developers, and to show off, but not for real day use) for the newest graphics chips from AMD. It's based on register documentation and close work with the engineers at AMD. 

Main authors have been Alex Deucher from AMD and Matthias Hopf from Novell. Initial DRM implementation has been done by Dave Airlie from Redhat, most of the IP review is done (r600_demo, register specs) and pending (part of docs) by John Bridgman from AMD. 

The r600_demo git repo can be cloned from [[git://anongit.freedesktop.org/git/mesa/r600_demo|http://cgit.freedesktop.org/mesa/r600_demo/]] - instructions to compile and run are in the accompanying [[README|http://cgit.freedesktop.org/mesa/r600_demo/tree/README]]. You also need the [['''r6xx-r7xx-support'''-branch of mesa/drm|http://cgit.freedesktop.org/mesa/drm/log/?h=r6xx-r7xx-support]]. Note that due to the IP review process we cannot publish the original git repository at the moment, but a list of commits will be published at a later point of time. 

Announcements: 

* [[On xorg-devel, radeonhd, xorg-driver-ati by Alex Deucher on Dec 29, 2008|http://lists.freedesktop.org/archives/xorg/2008-December/041951.html]] 
* [[On mesa-devel & dri-devel by Matthias Hopf on Dec 30, 2008|http://www.mail-archive.com/dri-devel%40lists.sourceforge.net/msg37356.html]] 
Developer documentation: 

* [[r6xx Register descriptions|http://www.x.org/docs/AMD/R6xx_3D_Registers.pdf]] 
* [[r6xx Instruction descriptions|http://www.x.org/docs/AMD/r600isa.pdf]] - note changes r6xx vs. r7xx and (few) wrong ids, check r600_demo/r600_shader.h 
* [[Blog entry of Matthias Hopf on Dec 30, 2008|http://emmes.livejournal.com/3435.html]] - description of r600_demo layout 
* [[Slides on r600_demo talk from Matthias Hopf in X.org DevRoom on Fosdem 2009|http://www.vis.uni-stuttgart.de/~hopf/pub/Fosdem_2009_r600demo_Slides.pdf]] - Explanation of the r[67]xx architecture and of pitfalls you can fall into 
* [[Errata sheet|radeonhd:r6xxErrata]] - wiki page with additional information, erratas, and explanation of difficult areas 
Reviews / validation: 

* [[Phoronix on Dec 31, 2008|http://www.phoronix.com/scan.php?page=article&item=amd_rv770_triangles&num=1]] 