

# radeonhd packages

Links to packages of [[radeonhd|radeonhd]] for different systems. 


## deb-based systems

   * [[Debian|http://www.debian.org/]]: 2008-04-22: Version 1.2.1-1 in unstable. [[Debian radeonhd package page|http://packages.qa.debian.org/x/xserver-xorg-video-radeonhd.html]] 
   * [[Ubuntu|http://www.ubuntu.com/]]: 2008-01-03: Version 1.1.0-1 in Hardy (universe). [[Ubuntu radeonhd package page|https://launchpad.net/ubuntu/+source/xserver-xorg-video-radeonhd]]. Unofficial packages at [[https://wiki.ubuntu.com/XorgOnTheEdge|https://wiki.ubuntu.com/XorgOnTheEdge]] 

## ports-based systems

   * [[FreeBSD|http://www.freebsd.org/]]: [[ports/x11-drivers/xf86-video-radeonhd/ in cvsweb|http://www.freebsd.org/cgi/cvsweb.cgi/ports/x11-drivers/xf86-video-radeonhd/]] 
   * [[Gentoo|http://www.gentoo.org/]]: x11-drivers/xf86-video-radeonhd (to install, run **emerge -av xf86-video-radeonhd**) 
   * [[NetBSD|http://www.netbsd.org/]]: [[The NetBSD Packages Collection: x11/xf86-video-radeonhd|ftp://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/x11/xf86-video-radeonhd/README.html]] 
   * [[OpenBSD|http://www.openbsd.org/]]: ??? 

## rpm-based systems

   * [[Fedora|http://fedoraproject.org]]: 
            * Easy command for F-10, F-11: `yum [--enablerepo=updates-testing] update xorg-x11-drv-radeonhd` 
            * The [[koji xorg-x11-drv-radeonhd page|http://koji.fedoraproject.org/koji/packageinfo?packageID=5156]] has the newest packages for F-10, F-11, and rawhide. Those will eventually filter down into your normal updates* yum repositories. 
            * Cf. [[viewcvs|http://cvs.fedoraproject.org/viewcvs/rpms/xorg-x11-drv-radeonhd/]], [[koji page|http://koji.fedoraproject.org/koji/packageinfo?packageID=5156]], [[update status|https://admin.fedoraproject.org/updates/xorg-x11-drv-radeonhd]], [[pkgdb bugs|https://admin.fedoraproject.org/pkgdb/packages/bugs/xorg-x11-drv-radeonhd]], [[fedora bz bugs|https://bugzilla.redhat.com/buglist.cgi?product=Fedora&amp;component=xorg-x11-drv-radeonhd]], [[fdo bz bugs|https://bugs.freedesktop.org/buglist.cgi?quicksearch=radeonhd]]. 
   * [[openSUSE|http://www.opensuse.org/]]: [[radeonhd openSUSE search results|http://benjiweber.co.uk:8080/webpin/index.jsp?searchTerm=radeonhd]] 