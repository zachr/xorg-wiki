# X11R7.0 Release

<i>This is no longer the current release.  For information about the current release, please see [[Releases/latest|Releases/latest]].</i>

[[!toc startlevel=2]]


## About the X11R7.0 Release

The first major version release of the X Window System in more than a decade, X11R7.0 is the first release of the complete modularized and autotooled source code base for the X Window System. [[X11R6.9|Releases/6.9]], its companion release, with identical features, updates X11R6.8 from an identical code base but for its traditional imake build system features.

Xorg 7.0 represents a major change in the development process for X.  Developers can work on individual components with less interference from unrelated changes, and users can acquire new features and bug fixes asynchronously from the main release stream.  This makes it easier than ever for developers and users to participate in the growth and success of the X Window System.

See [[Changes Since 6.8|ChangesSince68]] for an overview of the new features in 7.0, or view the [[complete changelog|http://cvs.freedesktop.org/xorg/xc/ChangeLog?r1=1.365&r2=1.1587]] for list of fixes and enhancements.

*Important!*  A security advisory involving local privilege escalation and DoS has been issued.  Please see the [[security page|SecurityPage]] for more details.


## Documentation for X11R7.0

 * [[Complete documentation|Documentation]]
 * [[User Documentation|http://ftp.x.org/pub/X11R7.0/doc/html/]]
 * [[Xlib programming documentation (pdf)|http://xorg.freedesktop.org/X11R7.0/doc/PDF/xlib.pdf]] [[(txt)|http://xorg.freedesktop.org/X11R7.0/doc/xlib.txt]] [[(ps)|http://xorg.freedesktop.org/X11R7.0/doc/PostScript/xlib.ps]]
 * [[README|http://ftp.x.org/pub/X11R7.0/doc/html/README.html]]
 * [[Release Notes|http://ftp.x.org/pub/X11R7.0/doc/html/RELNOTES.html]]
 * [[Licenses|http://ftp.x.org/pub/X11R7.0/doc/html/LICENSE.html]]
 * [[Build Directions|http://ftp.x.org/pub/X11R7.0/doc/html/BUILD.html]]
 * Manual pages: [[Commands|http://ftp.x.org/pub/X11R7.0/doc/html/manindex1.html]], [[Library Functions|http://ftp.x.org/pub/X11R7.0/doc/html/manindex3.html]], [[Drivers|http://ftp.x.org/pub/X11R7.0/doc/html/manindex4.html]], [[Files and file formats|http://ftp.x.org/pub/X11R7.0/doc/html/manindex5.html]], [[Miscellaneous|http://ftp.x.org/pub/X11R7.0/doc/html/manindex7.html]]
