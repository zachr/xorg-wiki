Release manager: [[KeithPackard|KeithPackard]]

Stable branch maintainer: [[MattDew|MattDew]]


## Schedule

See the [[X.Org Calendar|http://www.google.com/calendar/embed?src=nl1n1fmvu091eqh35ldqspar80%40group.calendar.google.com&ctz=Australia/Brisbane]].


## Merge Windows

The stable release cycle will typically be 6 weeks between dot releases.  This is subject to change as needed if big issues arise forcing earlier releases or regressions arise pushing back a release.


#### 3 weeks before RC1

During the first three weeks of the release cycle, the branch is open for general nominations.  As a rule of thumb, if a change does not alter ABI, change dependencies, or introduce new features, it can probably be considered for the stable branch.  The focus of patches merged into the stable branch during this period should be

 * Crash fixes
 * General bug fixes
 * New architecture support

#### 2 weeks before RC2

During the next two weeks of the cycle, the branch gets slightly tighter.  The branch is still open for general nominations, but the criteria for acceptance is a bit stricter.  Larger change sets which do not fix crashes or regressions will likely be deferred to the next release cycle (and picked up prior to RC1).  The focus of patches merged into stable during this period should be

 * Crash fixes
 * Regressions (from any previous release)
 * Fixes for bugs listed in the [[1.13 tracker|https://bugs.freedesktop.org/show_bug.cgi?id=xserver-1.13]]

#### 1 week before final release

During the last week before the release, the branch gets much tighter.  Any other changes should be held for nomination in the next cycle.  The focus of patches merged into stable during this period should be

 * Crash fixes with minimal code churn
 * Regressions from the previous stable release

## Proposing patches for 1.13.x

Server 1.13.0 has been [[released|http://lists.x.org/archives/xorg-announce/2012-September/002068.html]].  Patches may be nominated for future 1.13.x releases using the process outlined below.

 * **Do not push to server-1.13-branch.**
 * Patches for 1.13 must be in master first. Please follow the guidelines outlined on the [[XServer|XServer]] page to get a patch into master. Exceptions are patches where the master code has changed in a way that git master is not affected by a particular bug.
 * For single patches: Once a patch is in master, let the stable branch maintainer know that this patch is to be cherry-picked to 1.13.  Reply to the original patch email, placing the branch maintainer in TO and CC-ing the list.  Make sure that the subject line contains `[PATCH 1.13]`.
 * For multiple patches: Create a local branch and cherry-pick the patches required (Always use cherry-pick -x). Push this branch to your people.freedesktop.org repository and send a pull request to the stable branch maintainer, CC-ing the list.  Make sure the subject line contains `[PULL 1.13]`.
 * Only fixes for regressions introduced since the previous stable release will be accepted between the final rc and the release.

### Critical Bugs / Halting Release

If you have found a bug that you believe should delay the release of the next stable release, make sure you do each of the following to get attention:

 * Create a bug, and make sure the branch maintainer is CC'd.  Include a link to the commit that caused the issue if you were able to determine that.
 * Email the branch maintainer and xorg-devel about the issue.  Make sure the subject line contains either `[1.13 REGRESSION]` or `[1.13 DNR]` or similar.

Make sure you get confirmation, and follow up.  If the branch maintainer agrees, the bug will be added to the [[1.13 tracker|https://bugs.freedesktop.org/show_bug.cgi?id=xserver-1.13]].
