
# vesa

Driver for VESA based video chips. License: MIT 


## Documentation and Support

* Please check the [[manual page|http://www.x.org/archive/X11R7.0/doc/html/vesa.4.html]] for the current release for configuration options. 

## Known Issues


### Release 6.7.0
